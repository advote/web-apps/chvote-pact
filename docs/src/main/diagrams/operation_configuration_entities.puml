@startuml

class PrivilegedAction {
    id: Long
    type: String
    operationName: String
    operationDate: DateTime
    rejectionReason: String
    creationDate: DateTime
    requester: User
    validator: User
    statusDate: DateTime
}

enum PrivilegedActionStatus {
    PENDING
    APPROVED
    REJECTED
}

class ConfigurationDeployment {
}

class ConfigurationModification {
}

class VotingMaterialsCreation {
}

class VotingMaterialsInvalidation {
}

class VotingPeriodInitialization {
}

PrivilegedAction *--> "1" PrivilegedActionStatus
PrivilegedAction <|-- ConfigurationDeployment
PrivilegedAction <|-- ConfigurationModification
PrivilegedAction <|-- VotingMaterialsCreation
PrivilegedAction <|-- VotingMaterialsInvalidation
PrivilegedAction <|-- VotingPeriodInitialization

' -----------------------------------------------

class Attachment {
    id: Long
    name: String
    externalIdentifier: Long
    importDate: DateTime
    fileHash: String
    file: byte[]
}

enum AttachmentType {
    OPERATION_REPOSITORY_VOTATION
    OPERATION_REPOSITORY_ELECTION
    DOMAIN_OF_INFLUENCE
    OPERATION_FAQ
    OPERATION_TRANSLATION_FILE
    OPERATION_TERMS_OF_USAGE
    OPERATION_CERTIFICATE_VERIFICATION_FILE
    OPERATION_REGISTER
    ELECTION_OFFICER_KEY
    BALLOT_DOCUMENTATION
    HIGHLIGHTED_QUESTION
}

class OperationReferenceFile {
    messageId: String
    generationDate: DateTime
    sourceApplicationManufacturer: String
    sourceApplicationProduct: String
    sourceApplicationProductVersion: String
    signatureAuthor: String
    signatureExpiryDate: DateTime
}

enum SignatureStatus {
    OK
    EXPIRED_CERTIFICATE
    REVOKED_CERTIFICATE
    INVALID_SIGNATURE
    INVALID_CERTIFICATE
}

class InformationAttachment {
    language: String
}

class BallotDocumentation {
    ballot: String
    label: String
}

class HighlightedQuestion {
    question: String
}

class PublicKeyFile {

}

class VotersRegisterFile {
    nbrOfVoters: Integer
}

Attachment *--> "1" AttachmentType
Attachment <|-- OperationReferenceFile
Attachment <|-- InformationAttachment
Attachment <|-- PublicKeyFile
InformationAttachment<|-- BallotDocumentation
InformationAttachment<|-- HighlightedQuestion
OperationReferenceFile *--> "1" SignatureStatus
OperationReferenceFile <|-- VotersRegisterFile

' -----------------------------------------------

class PrinterConfiguration {
    id: Long
    businessIdentifier: String
    printerName: String
    publicKey: byte[]
    swissAbroadWithoutMunicipality: boolean
}

class Municipality {
    id: Long
    ofsId: Integer
    name: String
    virtual: boolean
}

PrinterConfiguration *--> "1..*" Municipality

' -----------------------------------------------

class ProtocolInstance {
    id: Long
    nodeRepartitionKey: String
    protocolId: String
    statusDate: DateTime
    publicParameters: String
    printerArchiveCreationDate: DateTime
    tallyArchivePath: String
}

enum ProtocolEnvironment {
    TEST
    PRODUCTION
}

enum ProtocolStage {
    SEND_PUBLIC_PARAMETERS
    PUBLISH_PUBLIC_KEY_PARTS
    STORE_PUBLIC_KEY_PARTS
    SEND_ELECTION_SET
    PUBLISH_PRIMES
    SEND_VOTERS
    BUILD_PUBLIC_CREDENTIALS
    REQUEST_PRIVATE_CREDENTIALS
    SEND_ELECTION_OFFICE_KEY
    INITIALIZE_CONTROL_COMPONENTS
    PUBLISH_SHUFFLE
    PUBLISH_PARTIAL_DECRYPTION
}

enum ProtocolInstanceStatus {
    INITIALIZING
    PUBLIC_PARAMETERS_INITIALIZED
    GENERATING_KEYS
    KEYS_GENERATED
    GENERATING_CREDENTIALS
    CREDENTIALS_GENERATED
    CREDENTIALS_GENERATION_FAILURE
    ELECTION_OFFICER_KEY_INITIALIZING
    ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE
    ELECTION_OFFICER_KEY_INITIALIZED
    READY_TO_RECEIVE_VOTES
    SHUFFLING
    DECRYPTING
    GENERATING_RESULTS
    TALLY_ARCHIVE_GENERATION_FAILURE
    NOT_ENOUGH_VOTES_TO_INITIATE_TALLY
    RESULTS_AVAILABLE
}

class ProtocolStageProgress {
    id: Long
    ccIndex: Integer
    done: Long
    total: Long
    startDate: DateTime
    lastModification: DateTime
}

class VotersCreationStats {
    id: Long
    printerAuthorityName: String
    countingCircleId: Integer
    numberOfVoters: Long
    countingCircleName: String
    countingCircleBusinessId: String
}

ProtocolInstance *--> "1" ProtocolEnvironment
ProtocolInstance *--> "1" ProtocolInstanceStatus
ProtocolInstance *--> "*" ProtocolStageProgress
ProtocolInstance *--> "*" VotersCreationStats
ProtocolStageProgress *--> "1" ProtocolStage

' -----------------------------------------------

class Operation {
    id: Long
    clientId: String
    creationDate: DateTime
}

class OperationConfiguration {
    id: Long
    operationName: String
    operationLabel: String
    operationDate: DateTime
    lastChangeUser: String
    siteTextHash: String
    siteOpeningDate: DateTime
    siteClosingDate: DateTime
    gracePeriod: Integer
    canton: String
}

enum ValidationStatus {
    IN_VALIDATION
    VALIDATED
    INVALIDATED
}

enum Target {
    REAL
    SIMULATION
}

class VotingMaterialsConfiguration {
    id: Long
    author: String
    submissionDate: DateTime
    votingCardLabel: String
    simulationName: String
    validator: String
    validationDate: DateTime
}

class VotingPeriodConfiguration {
    id: Long
    author: String
    submissionDate: DateTime
    simulationName: String
    siteOpeningDate: DateTime
    siteClosingDate: DateTime
    gracePeriod: Integer
}

Operation *--> "1" OperationConfiguration : Configuration in test
Operation *--> "0..1" OperationConfiguration : Configuration in production
Operation *--> "0..1" VotingMaterialsConfiguration
Operation *--> "0..1" VotingPeriodConfiguration

ProtocolInstance o--> "1" Operation

OperationConfiguration *--> "1" ValidationStatus
OperationConfiguration *--> "0..1" ConfigurationDeployment
OperationConfiguration *--> "0..1" ConfigurationModification
OperationConfiguration *--> "1" PrinterConfiguration
OperationConfiguration *--> "*" OperationReferenceFile
OperationConfiguration *--> "*" InformationAttachment
OperationConfiguration *--> "*" BallotDocumentation
OperationConfiguration *--> "*" HighlightedQuestion
OperationConfiguration *--o "*" ProtocolInstance

VotingMaterialsConfiguration *--> "1" ValidationStatus
VotingMaterialsConfiguration *--> "1" Target
VotingMaterialsConfiguration *--> "1" VotingMaterialsCreation
VotingMaterialsConfiguration *--> "1" VotingMaterialsInvalidation
VotingMaterialsConfiguration *--> "*" VotersRegisterFile
VotingMaterialsConfiguration *--> "*" PrinterConfiguration
VotingMaterialsConfiguration *--> "1" PrinterConfiguration : Swiss Abroad Printer

VotingPeriodConfiguration *--> "1" Target
VotingPeriodConfiguration *--> "1" VotingPeriodInitialization
VotingPeriodConfiguration *--> "1" PublicKeyFile

' -----------------------------------------------

class DeadLetterEvent {
    id: Long
    protocolId: String
    originalChannel: String
    originalRoutingKey: String
    type: String
    exceptionMessage: String
    exceptionStacktrace: String
    creationDate: DateTime
    pickupDate: DateTime
    content: String
    contentType: String
}

' -----------------------------------------------

class Voter {
    id: Long
    protocolVoterIndex: Integer
    registerPersonId: String
    printingAuthorityName: String
}

class VoterCountingCircle {
    protocolCountingCircleIndex: Integer
    businessId: String
    name: String
}

class DateOfBirth {
    year: Integer
    montOfYear: Integer
    dayOfMonth: Integer
}

Voter *--> "1" VoterCountingCircle
Voter *--> "1" DateOfBirth
ProtocolInstance *--> "1..*" Voter

' -----------------------------------------------

@enduml