/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { VoterCreationStats } from '../model/voting-materials-invalidation';
import { PrinterStatsComponent } from './printer-stats.component';

describe('PrinterStatsComponent', () => {
  let component: PrinterStatsComponent;

  const vcs: VoterCreationStats = {
    printer: 'prn1',
    controllerTestingCards: 1,
    nonPrintableTestingCards: 1,
    printableTestingCards: 1,
    printerTestingCards: 1,
    statByCountingCircle: []
  };

  component = new PrinterStatsComponent();

  component.stats = vcs;
  component.ngOnInit();

  it('should not have real cards', () => {
    expect(component.hasRealCards).toBeFalsy();
  });

  it('should have testing cards', () => {
    expect(component.hasTestingCards).toBeTruthy();
  });
})
;
