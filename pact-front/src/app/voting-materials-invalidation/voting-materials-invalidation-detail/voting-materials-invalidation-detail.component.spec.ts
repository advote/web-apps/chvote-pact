/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ActivatedRoute, convertToParamMap, Params, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { ActionConverterService } from '../../action-detail/action-converter.service';
import { LoadingSpinnerService } from '../../core/loading-spinner.service';
import { VotingMaterialsInvalidationDetailComponent } from './voting-materials-invalidation-detail.component';
import { VoterCreationStats, VotingMaterialsInvalidation } from '../model/voting-materials-invalidation';
import { VotingMaterialsInvalidationService } from '../voting-materials-invalidation.service';
import Spy = jasmine.Spy;

describe('VotingMaterialsInvalidationDetailComponent', () => {
  let component: VotingMaterialsInvalidationDetailComponent;

  const vcs: VoterCreationStats = {
    printer: 'prn1',
    controllerTestingCards: 1,
    nonPrintableTestingCards: 1,
    printableTestingCards: 1,
    printerTestingCards: 1,
    statByCountingCircle: []
  };

  const action1: VotingMaterialsInvalidation = {
    id: 1,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: null,
    statusDate: null,
    operationName: '201709VP',
    status: 'NOT_CREATED',
    type: 'VOTING_MATERIALS_INVALIDATION',
    votingMaterialsConfigurationId: 1,
    votersCreationStats: [vcs]

  };

  const action2: VotingMaterialsInvalidation = {
    id: 1,
    ownerName: 'user1',
    operationDate: new Date(Date.now()),
    creationDate: new Date(Date.now()),
    statusDate: new Date(Date.now()),
    operationName: '201709VP',
    status: 'PENDING',
    type: 'VOTING_MATERIALS_INVALIDATION',
    votingMaterialsConfigurationId: 2,
    votersCreationStats: [vcs]
  };

  const route: ActivatedRoute = <any> {
    paramMap: of(convertToParamMap(<Params> {id: '1'}))
  };

  const snackBar: MatSnackBar = <any> {
    openFromComponent: jasmine.createSpy('openFromComponent')
  };

  const votingMaterialsInvalidationService: VotingMaterialsInvalidationService = <any> {
    getById: jasmine.createSpy('getById').and.returnValue(of(action2)),
    getByBusinessId: jasmine.createSpy('getByBusinessId').and
      .returnValue(of(action1)),
    createAction: jasmine.createSpy('createAction').and.returnValue(of(true)),
    approve: jasmine.createSpy('approve').and.returnValue(of(true)),
    reject: jasmine.createSpy('reject').and.returnValue(of(true))
  };

  beforeEach(() => {
    (votingMaterialsInvalidationService.getById as Spy).calls.reset();
    (votingMaterialsInvalidationService.getByBusinessId as Spy).calls.reset();
    (votingMaterialsInvalidationService.createAction as Spy).calls.reset();
    (votingMaterialsInvalidationService.approve as Spy).calls.reset();
    (votingMaterialsInvalidationService.reject as Spy).calls.reset();
  });

  it('should retrieve and approve a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new VotingMaterialsInvalidationDetailComponent(router, route, snackBar,
      votingMaterialsInvalidationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingMaterialsInvalidationService.getById).toHaveBeenCalledTimes(1);
    expect(votingMaterialsInvalidationService.getById).toHaveBeenCalledWith(1);
    expect(votingMaterialsInvalidationService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');
    expect(component.action.type).toBe('VOTING_MATERIALS_INVALIDATION');
    component.onApproveAction();

    expect(votingMaterialsInvalidationService.approve).toHaveBeenCalledWith(1);
    expect(votingMaterialsInvalidationService.approve).toHaveBeenCalledTimes(1);
    expect(votingMaterialsInvalidationService.reject).not.toHaveBeenCalled();
    expect(votingMaterialsInvalidationService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-materials-invalidation']);
  });

  it('should retrieve and reject a pending action', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/pending-actions/1'
    };

    component = new VotingMaterialsInvalidationDetailComponent(router, route, snackBar,
      votingMaterialsInvalidationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingMaterialsInvalidationService.getById).toHaveBeenCalledTimes(1);
    expect(votingMaterialsInvalidationService.getById).toHaveBeenCalledWith(1);
    expect(votingMaterialsInvalidationService.getByBusinessId).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('PENDING');
    expect(component.action.type).toBe('VOTING_MATERIALS_INVALIDATION');

    component.onRejectAction('reason');

    expect(votingMaterialsInvalidationService.reject).toHaveBeenCalledWith(1, 'reason');
    expect(votingMaterialsInvalidationService.reject).toHaveBeenCalledTimes(1);
    expect(votingMaterialsInvalidationService.approve).not.toHaveBeenCalled();
    expect(votingMaterialsInvalidationService.createAction).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-materials-invalidation']);
  });

  it('should retrieve and create an voting material configuration', () => {
    const router: Router = <any> {
      navigate: jasmine.createSpy('navigate'),
      url: '/voting-materials-invalidation/1'
    };

    component = new VotingMaterialsInvalidationDetailComponent(router, route, snackBar,
      votingMaterialsInvalidationService, new LoadingSpinnerService(), new ActionConverterService());

    component.ngOnInit();

    expect(votingMaterialsInvalidationService.getByBusinessId).toHaveBeenCalledTimes(1);
    expect(votingMaterialsInvalidationService.getByBusinessId).toHaveBeenCalledWith(1);
    expect(votingMaterialsInvalidationService.getById).not.toHaveBeenCalled();
    expect(component.action.id).toBe(1);
    expect(component.action.status).toBe('NOT_CREATED');
    expect(component.action.type).toBe('VOTING_MATERIALS_INVALIDATION');

    component.onCreateAction();

    expect(votingMaterialsInvalidationService.createAction).toHaveBeenCalledWith(1);
    expect(votingMaterialsInvalidationService.createAction).toHaveBeenCalledTimes(1);
    expect(votingMaterialsInvalidationService.reject).not.toHaveBeenCalled();
    expect(votingMaterialsInvalidationService.approve).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/voting-materials-invalidation']);
  });
})
;
