/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { PrivilegedActionsModule } from './privileged-actions/privileged-actions.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { CoreModule } from './core/core.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { UserModule } from './user/user.module';
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from './layout/layout.module';
import { ConfigurationDeploymentModule } from './configuration-deployment/configuration-deployment.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { DateInterceptor } from './core/date-interceptor/DateInterceptor';
import { VotingMaterialsCreationModule } from './voting-materials-creation/voting-materials-creation.module';
import { VotingPeriodInitializationModule } from './voting-period-initialization/voting-period-initialization.module';
import { VotingMaterialsInvalidationModule } from './voting-materials-invalidation/voting-materials-invalidation.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    MaterialModule,
    UserModule,
    AuthenticationModule,
    PrivilegedActionsModule,
    ConfigurationDeploymentModule,
    VotingMaterialsCreationModule,
    VotingMaterialsInvalidationModule,
    VotingPeriodInitializationModule,
    AppRoutingModule,
    LayoutModule
  ],
  declarations: [AppComponent],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: DateInterceptor, multi: true}
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}
