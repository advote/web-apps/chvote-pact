/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { LoginComponent } from './login.component';
import { AuthenticationService } from '../authentication.service';

describe('LoginComponent', () => {
  let loginComponent: LoginComponent;
  const authenticationServiceMock: AuthenticationService = <any> {
    login: jasmine.createSpy('login')
  };

  beforeEach(() => {
    loginComponent = new LoginComponent(authenticationServiceMock);
  });

  it('should call authentication service on login', () => {
    loginComponent.login({username: 'user', password: 'passw0rd'});
    expect(authenticationServiceMock.login).toHaveBeenCalledWith('user', 'passw0rd');
  });
});
