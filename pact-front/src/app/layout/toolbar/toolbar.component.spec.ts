/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { ToolbarComponent } from './toolbar.component';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { NEVER, of } from 'rxjs';
import { UserService } from '../../user/user.service';
import { Router } from '@angular/router';
import { UserInfo } from '../../user/user-info';

describe('ToolbarComponent', () => {
  let toolbarComponent: ToolbarComponent;
  const availableLangs = ['fr', 'de'];
  const defaultLang = 'fr';
  let currentLang = defaultLang;
  const translateServiceMock: TranslateService = <any> {
    use: jasmine.createSpy('use').and.callFake((lang: string) => {
      currentLang = lang;
    }),
    get currentLang(): string {
      return currentLang;
    },
    getLangs: () => availableLangs,
    getDefaultLang: () => defaultLang,
    getBrowserLang: () => availableLangs[0]
  };
  const authenticationServiceMock: AuthenticationService = <any> {
    loginEvents: of(),
    logoutEvents: of()
  };
  const userServiceMock: UserService = <any> {
    currentUser: of(<UserInfo> {
      username: 'Somebody'
    })
  };
  const routerMock: Router = <any> {
    navigate: jasmine.createSpy('navigate'),
  };

  beforeEach(function () {
    toolbarComponent = new ToolbarComponent(
      authenticationServiceMock,
      userServiceMock,
      translateServiceMock,
      routerMock);
  });

  it('should have a title', () => {
    expect(toolbarComponent.title).toBeTruthy();
  });

  it('should have FR and DE translations', () => {
    expect(toolbarComponent.availableLangs()).toContain('fr');
    expect(toolbarComponent.availableLangs()).toContain('de');
  });

  it('should set the correct language on translate service when changing language', () => {
    // when
    toolbarComponent.currentLang = 'de';
    // then
    expect(translateServiceMock.use).toHaveBeenCalledWith('de');
    expect(toolbarComponent.currentLang).toBe('de');
  });

  it('should set user information on login', () => {
    const authServiceMock = <any> {
      logoutEvents: NEVER,
      loginEvents: of(<any> {})
    };
    toolbarComponent = new ToolbarComponent(
      authServiceMock,
      userServiceMock,
      translateServiceMock,
      routerMock);

    toolbarComponent.ngOnInit();
    expect(toolbarComponent.currentUser).toBeDefined();
    expect(toolbarComponent.currentUser.username).toBe('Somebody');
  });

  it('should clean up user information on logout', () => {
    const authServiceMock = <any> {
      logoutEvents: of(<any> {}),
      loginEvents: NEVER
    };

    toolbarComponent = new ToolbarComponent(
      authServiceMock,
      userServiceMock,
      translateServiceMock,
      routerMock);

    toolbarComponent.ngOnInit();
    expect(toolbarComponent.currentUser).toBeUndefined();
  });

  it('should call logout on authentication service', () => {
    const authServiceMock = <any> {
      logoutEvents: NEVER,
      loginEvents: NEVER,
      logout: jasmine.createSpy('logout')
    };

    toolbarComponent = new ToolbarComponent(
      authServiceMock,
      userServiceMock,
      translateServiceMock,
      routerMock);

    toolbarComponent.ngOnInit();
    expect(toolbarComponent.currentUser).toBeDefined();
    expect(toolbarComponent.currentUser.username).toBe('Somebody');

    toolbarComponent.logout();
    expect(authServiceMock.logout).toHaveBeenCalled();
  });
});
