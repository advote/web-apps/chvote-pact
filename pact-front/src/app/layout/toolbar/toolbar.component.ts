/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { Component, Input, OnInit } from '@angular/core';
import { SidenavComponent } from '../sidenav/sidenav.component';
import { TranslateService } from '@ngx-translate/core';
import { UserInfo } from '../../user/user-info';
import { UserService } from '../../user/user.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'pact-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input('sidenav') sidenav: SidenavComponent;
  private _user: UserInfo;

  constructor(private authenticationService: AuthenticationService,
              private userService: UserService,
              private translate: TranslateService,
              private router: Router) {
  }

  private _title = 'CHVote PACT';

  get title(): string {
    return this._title;
  }

  get currentLang(): string {
    return this.translate.currentLang;
  }

  set currentLang(value: string) {
    this.translate.use(value);
  }

  get currentUser(): UserInfo | undefined {
    return this._user;
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe(user => this._user = user);
    this.authenticationService.loginEvents.subscribe(_ => this.onLogin());
    this.authenticationService.logoutEvents.subscribe(_ => this.onLogout());
  }

  toggleSidenav() {
    this.sidenav.toggle();
  }

  logout(): void {
    this.authenticationService.logout();
  }

  availableLangs(): Array<string> {
    return this.translate.getLangs();
  }

  private onLogout() {
    this._user = undefined;
    this.router.navigate(['/login']);
  }

  private onLogin() {
    this.userService.currentUser.subscribe(user => this._user = user);
    this.router.navigate(['/']);
  }
}
