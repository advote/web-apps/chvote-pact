/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/


import { interval as observableInterval, Observable, Subscriber } from 'rxjs';

import { takeWhile } from 'rxjs/operators';
import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PollingService {
  private _observers: { [key: string]: Subscriber<any> } = {};

  constructor(private zone: NgZone,
              private httpClient: HttpClient) {

  }

  poll<T>(url: string, interval?: number): Observable<T> {
    return Observable.create((observer) => {
      let stop = false;
      this._observers[url] = observer;

      this.zone.runOutsideAngular(() => {
        observableInterval(interval ? interval : 15000).pipe(takeWhile(() => !stop)).subscribe(() => {
          this.zone.run(() => {
            this.httpClient.get<T>(url).subscribe(next => {
              observer.next(next);
            });
          });
        });
      });

      this.httpClient.get<T>(url).subscribe(next => {
        observer.next(next);
      });

      return () => {
        delete this._observers[url];
        stop = true;
      };
    });
  }

  broadcast() {
    Object.keys(this._observers).forEach(url => {
      this.zone.run(() => {
        this.httpClient.get(url).subscribe(next => {
          this._observers[url].next(next);
        });
      });
    });
  }
}
