/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { PACTController } from '../controller/pact.po';
import { LoginController } from '../controller/login.po';
import { PrivilegedActionsController } from '../controller/privileged-actions.po';
import { ActionDetailController } from '../controller/action-detail.po';
import { ActionTableController } from '../controller/action-table.po';
import { SidenavController } from '../controller/sidenav.po';
import { ConfigurationDeploymentsController } from '../controller/configuration-deployments.po';
import { VotingMaterialsConfigurationsController } from '../controller/voting-materials-configurations.po';
import { ConfigurationDeploymentDetailController } from '../controller/configuration-deployment-detail.po';
import { VotingMaterialsCreationDetailController } from '../controller/voting-materials-creation-detail.po';
import { LoginTestHelper } from '../helper/LoginTestHelper';
import { DateHelper } from '../helper/DateHelper';
import { browser } from 'protractor';
import { createOperations, resetDB } from '../helper/mock-server';
import { VotingPeriodInitializationsController } from '../controller/voting-period-initializations.po';

describe('Pact E2E', () => {

  describe('Navigation', () => {
    beforeAll((done) => {
      browser.controlFlow().execute(
        () => resetDB()
          .then(() => createOperations())
          .then(() => PACTController.open())
          .then(done)
      );
    });

    it('should display login form', () => {
      expect(LoginController.isLoginPage).toBeTruthy();
    });

    it('should fail to login with wrong creds', () => {
      LoginController.login('toto', 'not a password');
      expect(LoginController.isLoginPage).toBeTruthy();
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should display the number of pending actions', () => {
      expect(SidenavController.pendingActionCount).toBe(2);
    });


    it('should navigate to configuration deployment grid page', (done) => {
      ConfigurationDeploymentsController.open().then(() => {
        expect(ConfigurationDeploymentsController.isConfigurationDeploymentsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(5);
        done();
      });
    });

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(4);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should toggle between the splits of the privileged action grid page', (done) => {
      PrivilegedActionsController.splitPrivilegedActions(1).then(() => {
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        expect(PrivilegedActionsController.numberOfColumns).toBe(5);
        done();
      });
      PrivilegedActionsController.splitPrivilegedActions(2).then(() => {
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201802VP - Demande de déploiement');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user2');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('03.02.2018 09:21');
        expect(ConfigurationDeploymentDetailController.nbrOfAttachments).toBe(0);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should navigate to voting materials configuration grid page', (done) => {
      VotingMaterialsConfigurationsController.open().then(() => {
        expect(VotingMaterialsConfigurationsController.isVotingMaterialsConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(2);
        done();
      });
    });

    it('should navigate to voting period configuration grid page', (done) => {
      VotingPeriodInitializationsController.open().then(() => {
        expect(VotingPeriodInitializationsController.isVotingPeriodConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(2);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Configuration deployment action approbation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(4);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201802VP - Demande de déploiement');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user2');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('03.02.2018 09:21');
        expect(ConfigurationDeploymentDetailController.nbrOfAttachments).toBe(0);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should approve operation configuration', (done) => {
      ActionDetailController.approveAction().then(() => {
        expect(ConfigurationDeploymentsController.isConfigurationDeploymentsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(4);
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Configuration deployment action rejection', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201805VP - Demande de déploiement');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user2');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('11.03.2018 15:12');
        expect(ConfigurationDeploymentDetailController.nbrOfAttachments).toBe(1);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should reject operation configuration', (done) => {
      ActionDetailController.rejectAction().then(() => {
        expect(ConfigurationDeploymentsController.isConfigurationDeploymentsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(3);
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Configuration deployment action creation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should navigate to configuration deployment grid page', (done) => {
      ConfigurationDeploymentsController.open().then(() => {
        expect(ConfigurationDeploymentsController.isConfigurationDeploymentsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(3);
        done();
      });
    });

    it('should navigate to configuration deployment detail page', (done) => {
      ActionTableController.openActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201710VP - Demande de déploiement');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('TEST');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('20.08.2017 09:00');
        expect(ConfigurationDeploymentDetailController.nbrOfAttachments).toBe(2);
        expect(ActionDetailController.hasCreateButton).toBeTruthy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should create operation configuration', (done) => {
      ActionDetailController.createAction().then(() => {
        expect(ConfigurationDeploymentsController.isConfigurationDeploymentsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(3);
        done();
      });
    });

    it('should not display approve or reject button for the same user', (done) => {
      ActionTableController.openActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201710VP - Demande de déploiement');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toContain(DateHelper.dateAsString);
        expect(ConfigurationDeploymentDetailController.nbrOfAttachments).toBe(2);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Voting materials creation action approbation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user2', 'password');

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201812VP - Demande de création de matériel de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('12.01.2017 17:36');
        expect(ActionDetailController.getValueByFieldName('operationDate')).toBe('12.12.2018');
        expect(VotingMaterialsCreationDetailController.nbrOfPrinters).toBe(4);
        expect(VotingMaterialsCreationDetailController.nbrOfRegisters).toBe(2);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should approve voting materials configuration', (done) => {
      ActionDetailController.approveAction().then(() => {
        expect(VotingMaterialsConfigurationsController.isVotingMaterialsConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(1);
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Voting materials creation action creation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should navigate to voting materials creation grid page', (done) => {
      VotingMaterialsConfigurationsController.open().then(() => {
        expect(VotingMaterialsConfigurationsController.isVotingMaterialsConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(1);
        done();
      });
    });

    it('should navigate to voting materials creation detail page', (done) => {
      ActionTableController.openActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201901VP - Demande de création de matériel de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('Betty');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('30.11.2017 16:26');
        expect(ActionDetailController.getValueByFieldName('operationDate')).toBe('25.01.2019');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(VotingMaterialsCreationDetailController.nbrOfPrinters).toBe(0);
        expect(VotingMaterialsCreationDetailController.nbrOfRegisters).toBe(0);
        expect(ActionDetailController.hasCreateButton).toBeTruthy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should create voting materials creation', (done) => {
      ActionDetailController.createAction().then(() => {
        expect(VotingMaterialsConfigurationsController.isVotingMaterialsConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(1);
        done();
      });
    });

    it('should not display approve or reject button for the same user', (done) => {
      ActionTableController.openActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201901VP - Demande de création de matériel de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toContain(DateHelper.dateAsString);
        expect(ActionDetailController.getValueByFieldName('operationDate')).toBe('25.01.2019');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(VotingMaterialsCreationDetailController.nbrOfPrinters).toBe(0);
        expect(VotingMaterialsCreationDetailController.nbrOfRegisters).toBe(0);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Voting materials creation action rejection', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user2', 'password');

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(2).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201901VP - Demande de création de matériel de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toContain(DateHelper.dateAsString);
        expect(ActionDetailController.getValueByFieldName('operationDate')).toBe('25.01.2019');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(VotingMaterialsCreationDetailController.nbrOfPrinters).toBe(0);
        expect(VotingMaterialsCreationDetailController.nbrOfRegisters).toBe(0);
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should reject voting materials configuration', (done) => {
      ActionDetailController.rejectAction().then(() => {
        expect(VotingMaterialsConfigurationsController.isVotingMaterialsConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(0);
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });


  /**********************************************************
   ***************** VOTING PERIOD SECTION  *****************
   **********************************************************/

  describe('Voting period initialization action creation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should navigate to voting period creation grid page', (done) => {
      VotingPeriodInitializationsController.open().then(() => {
        expect(VotingPeriodInitializationsController.isVotingPeriodConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(2);
        done();
      });
    });

    it('should navigate to voting period creation detail page', (done) => {
      ActionTableController.openActionByPosition(1).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201908VP - Demande d\'initialisation de la période de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('16.05.2018 13:30');
        expect(ActionDetailController.getValueByFieldName('siteOpeningDate')).toBe('16.05.2040 13:30');
        expect(ActionDetailController.getValueByFieldName('siteClosingDate')).toBe('16.05.2040 17:30');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(ActionDetailController.getValueByFieldName('simulationName')).toBe('Simulation 1');
        expect(ActionDetailController.getElementByFieldName('action-detail-title-row'))
          .toBe('Clé de chiffrement de l\'autorité électorale');
        expect(ActionDetailController.getValueByFieldName('electoralAuthorityKey'))
          .toBe('A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191');
        expect(ActionDetailController.hasCreateButton).toBeTruthy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should create voting period initialization', (done) => {
      ActionDetailController.createAction().then(() => {
        expect(VotingPeriodInitializationsController.isVotingPeriodConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(2);
        done();
      });
    });

    it('should not display approve or reject button for the same user', (done) => {
      ActionTableController.openActionByPosition(1).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201908VP - Demande d\'initialisation de la période de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toContain(DateHelper.dateAsString);
        expect(ActionDetailController.getValueByFieldName('siteOpeningDate')).toBe('16.05.2040 13:30');
        expect(ActionDetailController.getValueByFieldName('siteClosingDate')).toBe('16.05.2040 17:30');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(ActionDetailController.getValueByFieldName('simulationName')).toBe('Simulation 1');
        expect(ActionDetailController.getElementByFieldName('action-detail-title-row'))
          .toBe('Clé de chiffrement de l\'autorité électorale');
        expect(ActionDetailController.getValueByFieldName('electoralAuthorityKey'))
          .toBe('A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191');
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });

  describe('Voting period creation action approbation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user2', 'password');

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(3);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201907VP - Demande d\'initialisation de la période de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toBe('12.01.2017 17:36');
        expect(ActionDetailController.getValueByFieldName('siteOpeningDate')).toBe('16.05.2040 13:30');
        expect(ActionDetailController.getValueByFieldName('siteClosingDate')).toBe('16.05.2040 17:30');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(ActionDetailController.getValueByFieldName('simulationName')).toBe('Simulation 2');
        expect(ActionDetailController.getElementByFieldName('action-detail-title-row'))
          .toBe('Clé de chiffrement de l\'autorité électorale');
        expect(ActionDetailController.getValueByFieldName('electoralAuthorityKey'))
          .toBe('A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191');
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should approve voting period configuration', (done) => {
      ActionDetailController.approveAction().then(() => {
        expect(VotingPeriodInitializationsController.isVotingPeriodConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(1);
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });


  describe('Voting period creation action creation', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user1', 'password');

    it('should navigate to voting period creation grid page', (done) => {
      VotingPeriodInitializationsController.open().then(() => {
        expect(VotingPeriodInitializationsController.isVotingPeriodConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(1);
        done();
      });
    });

    it('should navigate to voting period creation detail page', (done) => {
      ActionTableController.openActionByPosition(0).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201908VP - Demande d\'initialisation de la période de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toContain(DateHelper.dateAsString);
        expect(ActionDetailController.getValueByFieldName('siteOpeningDate')).toBe('16.05.2040 13:30');
        expect(ActionDetailController.getValueByFieldName('siteClosingDate')).toBe('16.05.2040 17:30');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(ActionDetailController.getValueByFieldName('simulationName')).toBe('Simulation 1');
        expect(ActionDetailController.getElementByFieldName('action-detail-title-row'))
          .toBe('Clé de chiffrement de l\'autorité électorale');
        expect(ActionDetailController.getValueByFieldName('electoralAuthorityKey'))
          .toBe('A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191');
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeFalsy();
        expect(ActionDetailController.hasRejectButton).toBeFalsy();
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });


  describe('Voting period creation action rejection', () => {
    beforeAll((done) => {
      PACTController.open().then(done);
    });

    LoginTestHelper.testValidLogin('user2', 'password');

    it('should navigate to privileged action grid page', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(2);
        expect(PrivilegedActionsController.numberOfColumns).toBe(6);
        done();
      });
    });

    it('should navigate to privileged action detail page', (done) => {
      PrivilegedActionsController.openPrivilegedActionByPosition(1).then(() => {
        expect(ActionDetailController.cardTitle).toBe('201908VP - Demande d\'initialisation de la période de vote');
        expect(ActionDetailController.getValueByFieldName('ownerName')).toBe('user1');
        expect(ActionDetailController.getValueByFieldName('statusDate')).toContain(DateHelper.dateAsString);
        expect(ActionDetailController.getValueByFieldName('siteOpeningDate')).toBe('16.05.2040 13:30');
        expect(ActionDetailController.getValueByFieldName('siteClosingDate')).toBe('16.05.2040 17:30');
        expect(ActionDetailController.getValueByFieldName('target')).toBe('Simulation');
        expect(ActionDetailController.getValueByFieldName('simulationName')).toBe('Simulation 1');
        expect(ActionDetailController.getElementByFieldName('action-detail-title-row'))
          .toBe('Clé de chiffrement de l\'autorité électorale');
        expect(ActionDetailController.getValueByFieldName('electoralAuthorityKey'))
          .toBe('A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191');
        expect(ActionDetailController.hasCreateButton).toBeFalsy();
        expect(ActionDetailController.hasApproveButton).toBeTruthy();
        expect(ActionDetailController.hasRejectButton).toBeTruthy();
        done();
      });
    });

    it('should reject voting period configuration', (done) => {
      ActionDetailController.rejectAction().then(() => {
        expect(VotingPeriodInitializationsController.isVotingPeriodConfigurationsPage).toBeTruthy();
        expect(ActionTableController.numberOfRows).toBe(0);
        done();
      });
    });

    it('should update privileged action grid', (done) => {
      PrivilegedActionsController.open().then(() => {
        expect(PrivilegedActionsController.isPrivilegedActionsPage).toBeTruthy();
        expect(PrivilegedActionsController.numberOfRows).toBe(1);
        done();
      });
    });

    LoginTestHelper.testLogout();
  });


});
