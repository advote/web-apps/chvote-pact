/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { PACTController } from '../controller/pact.po';
import { LoginController } from '../controller/login.po';
import { SidenavController } from '../controller/sidenav.po';
import { ConfigurationDeploymentsController } from '../controller/configuration-deployments.po';
import { LoginTestHelper } from '../helper/LoginTestHelper';
import { resetDB } from '../helper/mock-server';
import { browser } from 'protractor';

describe('Pact E2E - 1024px width', () => {
  beforeAll((done) => {
    browser.controlFlow().execute(
      () => resetDB()
        .then(() => PACTController.open())
        .then(done)
    );
  });

  it('should display login form', () => {
    expect(LoginController.isLoginPage).toBeTruthy();
  });

  LoginTestHelper.testValidLogin('user1', 'password');

  it('should show a toggle menu icon', () => {
    expect(SidenavController.isMenuToggleBtnDisplayed).toBeTruthy();
  });

  it('should initially not show the side nav', () => {
    expect(SidenavController.isSidenavOpened).toBeFalsy();
  });

  it('should open the side nav upon clicking on the toggle button', (done) => {
    SidenavController.openSideNav().then(() => {
      expect(SidenavController.isSidenavOpened).toBeTruthy();
      done();
    });
  });

  it('should close the side nav upon clicking on a nav item', (done) => {
    ConfigurationDeploymentsController.open().then(() => {
      expect(ConfigurationDeploymentsController.isConfigurationDeploymentsPage).toBeTruthy();
      done();
    });
  });

  LoginTestHelper.testLogout();
});
