/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import { browser, by, element, ExpectedConditions } from 'protractor';

export class VotingMaterialsInvalidationDetailController {

  static get nbrOfPrinter() {
    return element.all(by.css('.vmi-printer')).count();
  }

  static expansionPanel(idx: number) {
    return element(by.css(`.mat-expansion-panel:nth-child(${idx})`));
  }

  static openExpansionPanel(idx: number) {
    const expansionPanel = VotingMaterialsInvalidationDetailController.expansionPanel(idx);
    expansionPanel.click();
    const expansionBody = expansionPanel.element(by.css('.mat-expansion-panel-body'));
    browser.wait(ExpectedConditions.visibilityOf(expansionBody), 1000,
      'The expansion body has not been made visible on time');
  }

  static hasRealCardStatistics(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className("real-cards-stats")).isPresent();
  }


  static hasTestCardStatistics(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className("testing-cards-stats")).isPresent();
  }

  static controllerTestingCardCount(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className('action-cell-controllerTestingCards'))
      .getText()
      .then(value => parseInt(value));
  }

  static printableTestingCardCount(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className('action-cell-printableTestingCards'))
      .getText()
      .then(value => parseInt(value));

  }

  static nonPrintableTestingCardCount(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className('action-cell-nonPrintableTestingCards'))
      .getText()
      .then(value => parseInt(value));
  }

  static totalTestingCardCount(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className("panel-description-testingCardCount"))
      .getText()
      .then(value => parseInt(value));
  }

  static totalRealCardCount(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className("panel-description-realCardCount"))
      .getText()
      .then(value => parseInt(value));
  }

  static printerTestingCardCount(idx: number) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.className('action-cell-printerTestingCards'))
      .getText()
      .then(value => parseInt(value));
  }

  static getRealCardCount(idx: number, countingCircle: string) {
    return VotingMaterialsInvalidationDetailController.expansionPanel(idx)
      .element(by.id(`cc-count-${countingCircle}`))
      .getText()
      .then(value => parseInt(value));
  }

}

