/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.service.action;

import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.APPROVED;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.PENDING;
import static ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction.Status.REJECTED;

import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.VotingPeriodInitializationActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.period.VotingPeriodConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.action.vo.VotingPeriodInitializationVo;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyExistsException;
import ch.ge.ve.chvote.pactback.service.exception.PrivilegedActionAlreadyProcessedException;
import ch.ge.ve.chvote.pactback.service.operation.OperationService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A specialised {@link BaseActionService} for {@link VotingPeriodInitializationVo}s.
 */
@Service
public class VotingPeriodInitializationService extends ActionCreationService<VotingPeriodInitializationVo> {

  private final VotingPeriodConfigurationRepository        periodConfigurationRepository;
  private final VotingPeriodInitializationActionRepository actionRepository;
  private final OperationService                           operationService;


  private final PendingActionRetriever<VotingPeriodInitialization, VotingPeriodInitializationVo> pendingActionRetriever;

  public VotingPeriodInitializationService(UserRepository userRepository,
                                           VotingPeriodConfigurationRepository periodConfigurationRepository,
                                           VotingPeriodInitializationActionRepository actionRepository,
                                           OperationService operationService) {
    super(userRepository);
    this.actionRepository = actionRepository;
    this.periodConfigurationRepository = periodConfigurationRepository;
    this.operationService = operationService;
    this.pendingActionRetriever = new PendingActionRetriever<>(actionRepository, this::mapToPeriodInitializationVo);
  }


  private VotingPeriodInitializationVo mapToPeriodInitializationVo(VotingPeriodInitialization action) {
    Operation operation = operationService.getOperationByVotingPeriodConfigurationId(
        action.getVotingPeriodConfiguration().getId());
    return new VotingPeriodInitializationVo(operation, action);
  }

  @Override
  @Transactional
  public void approveAction(long actionId, String validatorName) {
    User validator = getUserByName(validatorName);

    VotingPeriodInitialization action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(this.updatePrivilegedAction(action, validator, APPROVED));

    Operation operation = operationService.getOperationByVotingPeriodConfigurationId(
        action.getVotingPeriodConfiguration().getId());
    operationService.publishVotingPeriodConfiguration(operation);
  }

  @Override
  @Transactional
  public void rejectAction(long actionId, String validatorName, String reason) {
    User validator = getUserByName(validatorName);

    VotingPeriodInitialization action = pendingActionRetriever.getPendingPrivilegedAction(actionId);
    actionRepository.save(this.updatePrivilegedAction(action, validator, REJECTED, reason));
  }

  @Override
  @Transactional(readOnly = true)
  public VotingPeriodInitializationVo getByBusinessId(Long businessId) {
    VotingPeriodConfiguration configuration = periodConfigurationRepository.getOne(businessId);
    configuration.getAction().ifPresent(action -> {
      if (!action.getStatus().canUpdate()) {
        throw new PrivilegedActionAlreadyProcessedException(action.getId());
      }
    });
    return mapToPeriodInitializationVo(configuration);
  }

  @Override
  @Transactional
  public void createAction(Long businessId, String requesterName) {
    if (actionRepository.existsByVotingPeriodConfiguration_Id(businessId)) {
      throw new PrivilegedActionAlreadyExistsException(
          "An action was already created for the VotingPeriodConfiguration object with ID " + businessId);
    }

    VotingPeriodConfiguration votingPeriodConfiguration =
        periodConfigurationRepository.findById(businessId)
                                     .orElseThrow(() -> new EntityNotFoundException(
                                         "No VotingPeriodConfiguration found for id: " + businessId));

    VotingPeriodInitialization action = new VotingPeriodInitialization();
    action.setVotingPeriodConfiguration(votingPeriodConfiguration);

    Operation operation = operationService.getOperationByVotingPeriodConfigurationId(
        votingPeriodConfiguration.getId());
    OperationConfiguration deployedConfiguration = operation.fetchDeployedConfiguration();

    actionRepository.save(populateNewAction(action, requesterName, deployedConfiguration.getOperationName(),
                                            deployedConfiguration.getOperationDate()));
  }

  @Override
  @Transactional(readOnly = true)
  public List<VotingPeriodInitializationVo> getNewOrPendingActions() {
    return periodConfigurationRepository
        .findAllByActionIsNullOrActionStatus(PENDING)
        .map(this::mapToPeriodInitializationVo)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public VotingPeriodInitializationVo getPendingPrivilegedAction(long id) {
    return pendingActionRetriever.getPendingPrivilegedActionVo(id);
  }

  private VotingPeriodInitializationVo mapToPeriodInitializationVo(VotingPeriodConfiguration configuration) {
    Operation operation = operationService.getOperationByVotingPeriodConfigurationId(configuration.getId());
    OperationConfiguration deployedConfiguration = operation.fetchDeployedConfiguration();
    return configuration.getAction()
                        .map(action -> new VotingPeriodInitializationVo(operation, action))
                        .orElse(new VotingPeriodInitializationVo(operation, deployedConfiguration.getOperationName(),
                                                                 deployedConfiguration.getOperationDate()));
  }
}
