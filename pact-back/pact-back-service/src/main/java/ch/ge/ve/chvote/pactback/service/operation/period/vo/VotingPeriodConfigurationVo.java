/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.service.operation.period.vo;

import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.service.operation.configuration.vo.OperationConfigurationVo;
import java.time.LocalDateTime;

/**
 * A voting period value object.
 */
public class VotingPeriodConfigurationVo {
  private final Long                     id;
  private final String                   author;
  private final LocalDateTime            submissionDate;
  private final String                   electoralAuthorityKey;
  private final String                   target;
  private final String                   simulationName;
  private final LocalDateTime            siteOpeningDate;
  private final LocalDateTime            siteClosingDate;
  private final Integer                  gracePeriod;
  private final OperationConfigurationVo configuration;

  /**
   * Create a new voting period configuration from the given operation.
   *
   * @param operation the operation.
   */
  public VotingPeriodConfigurationVo(Operation operation) {

    VotingPeriodConfiguration votingPeriodConfiguration =
        operation.getVotingPeriodConfiguration().orElseThrow(() -> new IllegalStateException(
            String.format("Missing voting period configuration in operation [%s]",
                          operation.getClientId())));


    this.id = votingPeriodConfiguration.getId();
    this.author = votingPeriodConfiguration.getAuthor();
    this.electoralAuthorityKey = votingPeriodConfiguration.getElectoralAuthorityKey().getFileHash();
    this.target = votingPeriodConfiguration.getTarget().name();
    this.simulationName = votingPeriodConfiguration.getSimulationName();
    this.submissionDate = votingPeriodConfiguration.getSubmissionDate();
    this.siteOpeningDate = votingPeriodConfiguration.getSiteOpeningDate();
    this.siteClosingDate = votingPeriodConfiguration.getSiteClosingDate();
    this.gracePeriod = votingPeriodConfiguration.getGracePeriod();
    this.configuration = new OperationConfigurationVo(operation.fetchDeployedConfiguration());
  }

  public Long getId() {
    return id;
  }

  public String getAuthor() {
    return author;
  }

  public LocalDateTime getSubmissionDate() {
    return submissionDate;
  }

  public String getElectoralAuthorityKey() {
    return electoralAuthorityKey;
  }

  public String getTarget() {
    return target;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public LocalDateTime getSiteOpeningDate() {
    return siteOpeningDate;
  }

  public LocalDateTime getSiteClosingDate() {
    return siteClosingDate;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public OperationConfigurationVo getConfiguration() {
    return configuration;
  }
}
