/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.configuration;

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import ch.ge.ve.chvote.pactback.contract.operation.BallotDocumentationVo;
import ch.ge.ve.chvote.pactback.contract.operation.HighlightedQuestionsVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.BallotDocumentation;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.HighlightedQuestion;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.exception.AttachmentParsingException;
import ch.ge.ve.chvote.pactback.service.operation.TimeService;
import ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.AttachmentService;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

/**
 * A service to interact and generate {@link Attachment}s for an {@link OperationConfiguration}.
 */
@Service
public class OperationConfigurationAttachmentService extends AttachmentService {

  /**
   * Create a new operation configuration attachment service.
   *
   * @param timeService                   the date-time service.
   * @param chVoteConfigurationProperties the global configuration properties of the application.
   */
  @Autowired
  public OperationConfigurationAttachmentService(TimeService timeService,
                                                 ChVoteConfigurationProperties chVoteConfigurationProperties) {
    super(timeService, chVoteConfigurationProperties);
  }

  /**
   * Populate the given {@link OperationConfiguration} entity with the attachments contained in the given zip files.
   *
   * @param operationConfiguration the {@link OperationConfiguration} entity to populate.
   * @param ballotDocumentations   a list containing metadata for all the ballot documentation files.
   * @param highlightedQuestions   a list containing metadata for all the highlighted question documentation files.
   * @param catalog                a list containing metadata of all the zip files.
   * @param attachmentsZip         the zip files to parse.
   *
   * @return the {@link OperationConfiguration} entity populated with attachments.
   */
  public OperationConfiguration populateAttachments(OperationConfiguration operationConfiguration,
                                                    Set<BallotDocumentationVo> ballotDocumentations,
                                                    Set<HighlightedQuestionsVo> highlightedQuestions,
                                                    List<AttachmentFileEntryVo> catalog,
                                                    List<MultipartFile> attachmentsZip) {
    attachmentsZip.forEach(zip -> {
      AttachmentFileEntryVo attachmentFileEntry =
          catalog.stream()
                 .filter(r -> r.getZipFileName().equals(zip.getOriginalFilename()))
                 .findFirst()
                 .orElseThrow(() -> new IllegalStateException(
                     String.format("Unexpected error: no file found in catalog with name [%s]",
                                   zip.getOriginalFilename())));

      this.populateAttachments(operationConfiguration,
                               ballotDocumentations,
                               highlightedQuestions,
                               attachmentFileEntry,
                               zip);
    });

    return operationConfiguration;
  }

  private OperationConfiguration populateAttachments(OperationConfiguration operationConfiguration,
                                                     Set<BallotDocumentationVo> ballotDocumentations,
                                                     Set<HighlightedQuestionsVo> highlightedQuestions,
                                                     AttachmentFileEntryVo attachmentFileEntry,
                                                     MultipartFile attachmentZip) {


    try (ZipInputStream zip = new ZipInputStream(attachmentZip.getInputStream())) {
      ZipEntry entry = zip.getNextEntry();
      parseEntryAndPopulate(attachmentFileEntry,
                            ballotDocumentations,
                            highlightedQuestions,
                            entry.getName(),
                            attachmentZip.getOriginalFilename(),
                            ByteStreams.toByteArray(zip),
                            operationConfiguration);

    } catch (IOException ex) {
      throw new AttachmentParsingException(
          String.format("An exception occurred while reading \"%s\".", attachmentFileEntry), ex);
    }

    return operationConfiguration;
  }

  private void parseEntryAndPopulate(AttachmentFileEntryVo attachmentFileEntry,
                                     Set<BallotDocumentationVo> ballotDocumentations,
                                     Set<HighlightedQuestionsVo> highlightedQuestions,
                                     String fileName,
                                     String zipFilename,
                                     byte[] file,
                                     OperationConfiguration operationConfiguration) throws IOException {

    AttachmentType type = AttachmentType.findTypeByShortLabel(attachmentFileEntry.getAttachmentType().name())
                                        .orElse(null);
    if (type != null && type.isOperationReferenceFile()) {
      attachOperation(attachmentFileEntry, fileName, file, operationConfiguration, type);
    } else if (type == AttachmentType.BALLOT_DOCUMENTATION) {
      attachBallotDocumentation(attachmentFileEntry, ballotDocumentations, fileName, zipFilename, file,
                                operationConfiguration);
    } else if (type == AttachmentType.HIGHLIGHTED_QUESTION) {
      attachHighlightedQuestion(attachmentFileEntry, highlightedQuestions, fileName, zipFilename, file,
                                operationConfiguration);
    } else {
      attachInformationAttachment(attachmentFileEntry, fileName, file, operationConfiguration, type);
    }
  }

  private void attachInformationAttachment(AttachmentFileEntryVo attachmentFileEntry, String fileName, byte[] file,
                                           OperationConfiguration operationConfiguration, AttachmentType type) {
    InformationAttachment attachment = new InformationAttachment();
    attachment.setLanguage(attachmentFileEntry.getLanguage());
    operationConfiguration.getInformationAttachments()
                          .add(populate(attachment, type, fileName, attachmentFileEntry.getImportDateTime(), file,
                                        attachmentFileEntry.getExternalIdentifier()));
  }

  private void attachHighlightedQuestion(AttachmentFileEntryVo attachmentFileEntry, Set<HighlightedQuestionsVo>
      highlightedQuestions, String fileName, String zipFilename, byte[] file, OperationConfiguration
                                             operationConfiguration) {
    HighlightedQuestionsVo highlightedQuestion = highlightedQuestions
        .stream()
        .filter(hq -> hq.getAnswerFile().getZipFileName().equals(zipFilename))
        .findFirst()
        .orElseThrow(() -> new IllegalStateException(
            String.format("Unexpected error: no such ballot documentation found with name [%s]", zipFilename)));
    HighlightedQuestion attachment = new HighlightedQuestion();
    attachment.setLanguage(attachmentFileEntry.getLanguage());
    attachment.setQuestion(highlightedQuestion.getQuestion());
    operationConfiguration.getHighlightedQuestions()
                          .add(populate(attachment, AttachmentType.HIGHLIGHTED_QUESTION, fileName,
                                        attachmentFileEntry.getImportDateTime(), file,
                                        attachmentFileEntry.getExternalIdentifier()));
  }

  private void attachBallotDocumentation(AttachmentFileEntryVo attachmentFileEntry, Set<BallotDocumentationVo>
      ballotDocumentations, String fileName, String zipFilename, byte[] file, OperationConfiguration
                                             operationConfiguration) {
    BallotDocumentationVo ballotDocumentation = ballotDocumentations
        .stream()
        .filter(bd -> bd.getDocumentation().getZipFileName().equals(zipFilename))
        .findFirst()
        .orElseThrow(() -> new IllegalStateException(
            String.format("Unexpected error: no such ballot documentation found with name [%s]", zipFilename)));
    BallotDocumentation attachment = new BallotDocumentation();
    attachment.setLanguage(attachmentFileEntry.getLanguage());
    attachment.setBallot(ballotDocumentation.getBallot());
    attachment.setLabel(ballotDocumentation.getLabel());
    operationConfiguration.getBallotDocumentations()
                          .add(populate(attachment, AttachmentType.BALLOT_DOCUMENTATION, fileName,
                                        attachmentFileEntry.getImportDateTime(), file,
                                        attachmentFileEntry.getExternalIdentifier()));
  }

  private void attachOperation(AttachmentFileEntryVo attachmentFileEntry, String fileName, byte[] file,
                               OperationConfiguration operationConfiguration, AttachmentType type)
      throws IOException {
    OperationReferenceFile attachment = new OperationReferenceFile();
    Document document = parse(fileName, file);
    attachment = populateInFileAttributes(attachment, attachmentFileEntry.getZipFileName(), document);
    operationConfiguration.getOperationReferenceFiles()
                          .add(populate(attachment, type, fileName, attachmentFileEntry.getImportDateTime(), file,
                                        attachmentFileEntry.getExternalIdentifier()));
  }

}
