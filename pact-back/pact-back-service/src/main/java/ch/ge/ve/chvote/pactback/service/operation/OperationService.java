/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.OperationConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.OperationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.PublicKeyFileRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.VotingMaterialsConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.period.VotingPeriodConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.CannotCreateOrUpdateVotingPeriodConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.CannotDeployOperationConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.CannotUpdateVotingMaterialConfigurationException;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.operation.configuration.OperationConfigurationService;
import ch.ge.ve.chvote.pactback.service.operation.materials.VotingMaterialsService;
import ch.ge.ve.chvote.pactback.service.operation.period.VotingPeriodService;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInitializationService;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService;
import ch.ge.ve.chvote.pactback.service.protocol.ProtocolVotingPeriodInitializationService;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * A service for retrieving and modifying {@link Operation} entities.
 */
@Service
public class OperationService {
  private final OperationRepository                       operationRepository;
  private final ProtocolInstanceService                   protocolInstanceService;
  private final ProtocolInitializationService             protocolInitializationService;
  private final ProtocolVotingPeriodInitializationService protocolVotingPeriodInitializationService;
  private final OperationConfigurationStatusVoMapper      operationConfigurationStatusVoMapper;
  private final OperationConfigurationService             configurationService;
  private final VotingMaterialsService                    votingMaterialsService;
  private final VotingPeriodService                       votingPeriodService;
  private final VotingMaterialsConfigurationRepository    votingMaterialsConfigurationRepository;
  private final VotingPeriodConfigurationRepository       votingPeriodConfigurationRepository;
  private final PublicKeyFileRepository                   publicKeyFileRepository;

  /**
   * Creates a new operation service.
   *
   * @param operationRepository                       the {@link Operation} repository.
   * @param protocolInstanceService                   the {@link ProtocolInstance} service.
   * @param protocolInitializationService             the {@link ProtocolInstance} initialization service.
   * @param protocolVotingPeriodInitializationService the {@link VotingPeriodConfiguration} initialization service.
   * @param operationConfigurationStatusVoMapper      the {@link OperationConfigurationStatusVo} mapper.
   * @param configurationService                      the {@link OperationConfiguration} service.
   * @param votingMaterialsService                    the {@link VotingMaterialsConfiguration} service.
   * @param votingPeriodService                       the {@link VotingPeriodConfiguration} service.
   * @param votingMaterialsConfigurationRepository    the {@link VotingMaterialsConfiguration} repository.
   * @param votingPeriodConfigurationRepository       the {@link VotingPeriodConfiguration} repository.
   * @param publicKeyFileRepository                   the {@link PublicKeyFile} repository.
   */
  @Autowired
  public OperationService(OperationRepository operationRepository,
                          ProtocolInstanceService protocolInstanceService,
                          ProtocolInitializationService protocolInitializationService,
                          ProtocolVotingPeriodInitializationService protocolVotingPeriodInitializationService,
                          OperationConfigurationStatusVoMapper operationConfigurationStatusVoMapper,
                          OperationConfigurationService configurationService,
                          VotingMaterialsService votingMaterialsService,
                          VotingPeriodService votingPeriodService,
                          VotingMaterialsConfigurationRepository votingMaterialsConfigurationRepository,
                          VotingPeriodConfigurationRepository votingPeriodConfigurationRepository,
                          PublicKeyFileRepository publicKeyFileRepository) {

    this.operationRepository = operationRepository;
    this.protocolInstanceService = protocolInstanceService;
    this.protocolInitializationService = protocolInitializationService;
    this.protocolVotingPeriodInitializationService = protocolVotingPeriodInitializationService;
    this.operationConfigurationStatusVoMapper = operationConfigurationStatusVoMapper;
    this.configurationService = configurationService;
    this.votingMaterialsService = votingMaterialsService;
    this.votingPeriodService = votingPeriodService;
    this.votingMaterialsConfigurationRepository = votingMaterialsConfigurationRepository;
    this.votingPeriodConfigurationRepository = votingPeriodConfigurationRepository;
    this.publicKeyFileRepository = publicKeyFileRepository;
  }

  /**
   * Get an {@link Operation} given an operation client identifier.
   */
  @Transactional(readOnly = true)
  public Operation getOperationByClientId(String clientId) {
    return operationRepository.findByClientId(clientId)
                              .orElseThrow(getEntityNotFoundExceptionSupplier(clientId));
  }

  /**
   * Gets the {@link Operation} whose voting materials configuration has the specified ID.
   */
  @Transactional(readOnly = true)
  public Operation getOperationByVotingMaterialsConfigurationId(Long vmcId) {
    return operationRepository.findByVotingMaterialsConfiguration_Id(vmcId)
                              .orElseThrow(getVotingMaterialsConfigurationExceptionSupplier(vmcId));
  }

  /**
   * Get an {@link OperationConfigurationStatusVo} given and operation client identifier.
   *
   * @param clientId the client id of the operation to retrieve.
   *
   * @return the current status of the operation.
   */
  @Transactional(readOnly = true)
  public OperationConfigurationStatusVo getOperationStatusByClientId(String clientId) {
    return operationRepository.findByClientId(clientId)
                              .map(operationConfigurationStatusVoMapper::map)
                              .orElseThrow(getEntityNotFoundExceptionSupplier(clientId));
  }

  private Operation getOrCreateOperation(String clientId) {
    return operationRepository.findByClientId(clientId).orElseGet(() -> {
      Operation operation = new Operation();
      operation.setClientId(clientId);
      operation.setCreationDate(LocalDateTime.now());
      return operation;
    });
  }

  private void stopTestInstances(Operation operation) {
    operation.getConfigurationInTest().ifPresent(
        configuration -> configuration.getProtocolInstance().ifPresent(protocolInstance -> {
          protocolInstanceService.deleteProtocolInstance(protocolInstance);
          configuration.setProtocolInstance(null);
        })
    );
  }

  /**
   * Creates or updates an {@link Operation} entity for the given {@link OperationConfigurationSubmissionVo}. If an
   * {@link Operation} exists for the given client id, older configurations and their associated action that are not
   * deployed to {@link ProtocolEnvironment#PRODUCTION} will be removed and their associated protocol instance will be
   * stopped.
   *
   * @param vo             the {@link OperationConfigurationSubmissionVo}.
   * @param zipAttachments the attachments file in a formatted ZIP.
   *
   * @return the new or updated operation
   */
  @Transactional
  public Operation createOrUpdateOperation(OperationConfigurationSubmissionVo vo, List<MultipartFile> zipAttachments) {
    Operation operation = getOrCreateOperation(vo.getClientId());

    stopTestInstances(operation);

    OperationConfiguration configuration = configurationService.createOperationConfiguration(vo, zipAttachments);
    operation.setConfigurationInTest(configuration);
    operation = operationRepository.save(operation);

    ProtocolInstance instance = protocolInstanceService.createProtocolInstance(operation,
                                                                               operation::fetchConfigurationInTest,
                                                                               ProtocolEnvironment.TEST);
    protocolInitializationService.startProtocol(instance);

    return operation;
  }

  /**
   * Invalidates the {@link OperationConfiguration} currently deployed in test for the {@link Operation} identified by
   * the given client identifier.
   *
   * @param clientId the client identifier of the operation to invalidate.
   * @param user     the username of the user performing the invalidation.
   */
  @Transactional
  public void invalidateConfigurationByClientId(String clientId, String user) {
    Operation operation = operationRepository.findByClientId(clientId)
                                             .orElseThrow(getEntityNotFoundExceptionSupplier(clientId));

    configurationService.invalidateLatestOperationConfiguration(operation, user);
  }


  /**
   * Validates the {@link OperationConfiguration} currently deployed in test for the {@link Operation} identified by the
   * given client identifier.
   *
   * @param clientId the client identifier of the operation to validate.
   * @param user     the username of the user performing the validation.
   */
  @Transactional
  public void validateConfigurationByClientId(String clientId, String user) {
    Operation operation = operationRepository.findByClientId(clientId)
                                             .orElseThrow(getEntityNotFoundExceptionSupplier(clientId));

    configurationService.validateLatestOperationConfiguration(operation, user);
  }

  /**
   * Set {@link OperationConfiguration} in test for the given {@link Operation} as the deployed configuration if there
   * is none already.
   *
   * @param operationConfigurationId ID of the in-test {@link OperationConfiguration} to deploy
   */
  @Transactional
  public void deployConfigurationInTest(Long operationConfigurationId) {
    Operation operation = operationRepository
        .findByConfigurationInTest_Id(operationConfigurationId)
        .orElseGet(() -> operationRepository.findByDeployedConfiguration_Id(operationConfigurationId)
                                            .orElseThrow(
                                                getOperationConfigurationExceptionSupplier(operationConfigurationId)));

    operation.getDeployedConfiguration().ifPresent(configuration -> {
      throw new CannotDeployOperationConfigurationException(
          String.format("An operation configuration with id [%s]] is already deployed " +
                        "for the operation with clientId [%s].", configuration.getId(), operation.getClientId()));
    });

    OperationConfiguration configuration =
        operation.getConfigurationInTest()
                 .orElseThrow(() -> new CannotDeployOperationConfigurationException(
                     "No OperationConfiguration found in test for clientId [" + operation.getClientId() + "]."));

    configuration.getProtocolInstance().ifPresent(protocolInstanceService::deleteProtocolInstance);
    configuration.setProtocolInstance(null);

    operation.setConfigurationInTest(null);
    operation.setDeployedConfiguration(configuration);

    operationRepository.save(operation);
  }

  /**
   * Creates a new Protocol instance into {@link ProtocolEnvironment#PRODUCTION} and publishes the deployed
   * configuration and its approved voting materials for the given {@link Operation}.
   *
   * @param operation a {@link Operation} with a deployed configuration.
   */
  @Transactional
  public void publishDeployedConfiguration(Operation operation) {
    ProtocolInstance instance = protocolInstanceService.createProtocolInstance(operation,
                                                                               operation::fetchDeployedConfiguration,
                                                                               ProtocolEnvironment.PRODUCTION);
    protocolInitializationService.startProtocol(instance);
  }

  /**
   * Creates a {@link VotingMaterialsConfiguration} for an existing {@link Operation}.
   *
   * @param clientId     ID of the operation in the client system
   * @param submissionVo data for the voting materials configuration
   *
   * @throws CannotUpdateVotingMaterialConfigurationException if the operation already has a voting materials
   *                                                          configuration that has not been rejected.
   */
  @Transactional
  public void addVotingMaterialsConfiguration(String clientId,
                                              VotingMaterialsConfigurationSubmissionVo submissionVo,
                                              List<MultipartFile> zipFiles) {
    Operation operation = getOperationByClientId(clientId);

    checkConditionsForCreatingVotingMaterialsConfiguration(operation);

    VotingMaterialsConfiguration votingMaterials =
        votingMaterialsService.createVotingMaterialsConfiguration(submissionVo, zipFiles);
    votingMaterialsConfigurationRepository.save(votingMaterials);

    operation.getVotingMaterialsConfiguration().ifPresent(votingMaterialsConfigurationRepository::delete);
    operation.setVotingMaterialsConfiguration(votingMaterials);
    operationRepository.save(operation);
  }

  /**
   * Publishes the voting period configuration for the given {@link Operation}.
   *
   * @param operation a {@link Operation} with a deployed configuration.
   */
  @Transactional
  public void publishVotingPeriodConfiguration(Operation operation) {
    protocolVotingPeriodInitializationService
        .initVotingPeriod(operation.fetchDeployedConfiguration().fetchProtocolInstance());
  }

  private void checkConditionsForCreatingVotingMaterialsConfiguration(Operation operation) {
    // if the previous generation has failed the voting material configuration can be updated regardless
    // of the action's state. No more checks are needed.
    if (!isProtocolStatusIn(operation, ProtocolInstanceStatus.CREDENTIALS_GENERATION_FAILURE)) {
      // the operation must have no voting materials configuration or one that has been rejected


      // The operation must have
      //     - no voting materials configuration
      //     - or no creation requested
      //     - or a rejected creation
      //     - or an invalidated voting material
      operation.getVotingMaterialsConfiguration().ifPresent(
          conf -> {
            if (conf.getInvalidationAction()
                    .map(action -> action.getStatus() == PrivilegedAction.Status.APPROVED)
                    .orElse(false)) {
              return;
            }

            if (conf.getCreationAction().map(action -> action.getStatus().equals(PrivilegedAction.Status.REJECTED))
                    .orElse(false)) {
              return;
            }

            if (conf.getCreationAction().isPresent()) {
              throw new CannotUpdateVotingMaterialConfigurationException(
                  String.format("Cannot update voting material configuration for Operation [%s] " +
                                "because it has not been rejected or invalidated",
                                operation.getClientId()));
            }
          }
      );
    }
  }

  /**
   * Delete the protocol instance of the deployed configuration. Updates operation configuration consequently.
   *
   * @param operation to manipulate
   */
  @Transactional
  public void deleteDeployedConfigurationInstance(Operation operation) {
    operation.getDeployedConfiguration()
             .ifPresent(operationConfiguration -> operationConfiguration.getProtocolInstance().ifPresent(
                 protocolInstance -> {
                   protocolInstanceService.deleteProtocolInstance(protocolInstance);
                   operationConfiguration.setProtocolInstance(null);
                 }));
  }


  private boolean isProtocolStatusIn(Operation operation, ProtocolInstanceStatus expectedStatus,
                                     ProtocolInstanceStatus... expectedStatuses) {
    EnumSet<ProtocolInstanceStatus> expectedStatusSet = EnumSet.of(expectedStatus, expectedStatuses);
    return operation.fetchDeployedConfiguration()
                    .getProtocolInstance()
                    .map(ProtocolInstance::getStatus)
                    .map(expectedStatusSet::contains)
                    .orElse(false);
  }

  private Supplier<EntityNotFoundException> getEntityNotFoundExceptionSupplier(String clientId) {
    return () ->
        new EntityNotFoundException("No OperationConfiguration found for clientId [" + clientId + "] ");
  }

  private Supplier<EntityNotFoundException> getVotingMaterialsConfigurationExceptionSupplier(Long vmcId) {
    return () ->
        new EntityNotFoundException(
            String.format("No Operation has a VotingMaterialConfiguration with ID [%d]", vmcId));
  }

  private Supplier<EntityNotFoundException> getOperationConfigurationExceptionSupplier(Long operationConfigurationId) {
    return () ->
        new EntityNotFoundException(String.format("No Operation has a either a test OperationConfiguration or a real "
                                                  + "OperationConfiguration with ID [%d]", operationConfigurationId));
  }

  @Transactional
  public void addVotingPeriodConfiguration(String clientId, VotingPeriodConfigurationSubmissionVo submissionVo,
                                           MultipartFile electionOfficerPublicKey) {
    Operation operation = getOperationByClientId(clientId);
    checkDateConsistency(operation, submissionVo);
    checkConditionsForCreatingVotingPeriodConfiguration(operation);
    operation.getVotingPeriodConfiguration().ifPresent(votingPeriodConfiguration -> {
      publicKeyFileRepository.delete(votingPeriodConfiguration.getElectoralAuthorityKey());
      votingPeriodConfigurationRepository.delete(votingPeriodConfiguration);
    });


    VotingPeriodConfiguration votingPeriodConfiguration =
        votingPeriodService.convertToVotingPeriodConfiguration(submissionVo, electionOfficerPublicKey);

    operation.setVotingPeriodConfiguration(votingPeriodConfiguration);
    operationRepository.save(operation);
  }

  private void checkDateConsistency(Operation operation, VotingPeriodConfigurationSubmissionVo submissionVo) {

    OperationConfiguration configuration = operation.getDeployedConfiguration().orElseThrow(
        () -> CannotCreateOrUpdateVotingPeriodConfigurationException.noConfigurationDeployed(operation.getClientId())
    );

    DeploymentTarget target = operation.getVotingMaterialsConfiguration().orElseThrow(
        () -> CannotCreateOrUpdateVotingPeriodConfigurationException.noVotingMaterial(operation.getClientId()))
                                       .getTarget();

    if (target == DeploymentTarget.REAL &&
        !(configuration.getSiteClosingDate().equals(submissionVo.getSiteClosingDate()) &&
          configuration.getSiteOpeningDate().equals(submissionVo.getSiteOpeningDate()))) {
      throw CannotCreateOrUpdateVotingPeriodConfigurationException.wrongDateForRealOperation(operation.getClientId());
    }
  }


  private void checkConditionsForCreatingVotingPeriodConfiguration(Operation operation) {

    boolean protocolStatusAllowCreation = isProtocolStatusIn(
        operation, ProtocolInstanceStatus.CREDENTIALS_GENERATED,
        ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE);

    if (!protocolStatusAllowCreation) {
      throw CannotCreateOrUpdateVotingPeriodConfigurationException.wrongProtocolState(
          operation.getClientId(),
          operation.fetchDeployedConfiguration().getProtocolInstance()
                   .map(ProtocolInstance::getStatus).orElse(null));
    }

    operation.getVotingMaterialsConfiguration().map(VotingMaterialsConfiguration::getValidationStatus)
             .ifPresent(validationStatus -> {
               if (validationStatus != ValidationStatus.VALIDATED) {
                 throw CannotCreateOrUpdateVotingPeriodConfigurationException
                     .votingMaterialNotValidated(operation.getClientId());
               }
             });


    boolean hasElectionOfficerKeyInitializationFailed =
        isProtocolStatusIn(operation, ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE);

    // if the previous initialization has failed the voting period configuration can be updated regardless
    // of the action's state. No more checks are needed.
    if (!hasElectionOfficerKeyInitializationFailed) {
      // the operation must have no voting period configuration or one that has been rejected
      operation.getVotingPeriodConfiguration().ifPresent(
          conf ->
              conf.getAction().ifPresent(action -> {
                if (!action.getStatus().equals(PrivilegedAction.Status.REJECTED)) {
                  throw CannotCreateOrUpdateVotingPeriodConfigurationException.notRejected(operation.getClientId());
                }
              })
      );
    }
  }


  /**
   * Gets the {@link Operation} whose voting period configuration has the specified ID.
   */
  @Transactional(readOnly = true)
  public Operation getOperationByVotingPeriodConfigurationId(Long vpcId) {
    return operationRepository.findByVotingPeriodConfiguration_Id(vpcId)
                              .orElseThrow(getVotingPeriodConfigurationExceptionSupplier(vpcId));
  }

  private Supplier<EntityNotFoundException> getVotingPeriodConfigurationExceptionSupplier(Long vpcId) {
    return () ->
        new EntityNotFoundException(
            String.format("No Operation has a VotingPeriodConfiguration with ID [%d]", vpcId));
  }


}
