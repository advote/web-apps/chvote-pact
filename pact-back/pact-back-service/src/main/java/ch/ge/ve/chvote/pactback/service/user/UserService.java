/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.user;

import ch.ge.ve.chvote.pactback.repository.user.UserRepository;
import ch.ge.ve.chvote.pactback.repository.user.entity.User;
import ch.ge.ve.chvote.pactback.service.exception.UserNotFoundException;
import ch.ge.ve.chvote.pactback.service.user.vo.UserVo;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A {@link User} service for retrieving and modifying {@link User} entities.
 */
@Service
@Transactional(readOnly = true)
public class UserService {

  private final UserRepository repository;

  /**
   * Creates a new {@link UserService} for the given repository.
   *
   * @param repository a {@link User} repository.
   */
  @Autowired
  public UserService(UserRepository repository) {
    this.repository = repository;
  }

  /**
   * Retrieve a {@link UserVo} by username.
   *
   * @param username the username to look for.
   *
   * @return The matching {@link UserVo}.
   */
  public UserVo findByUsername(String username) {
    Preconditions.checkNotNull(username);
    return repository.findByUsername(username)
                     .map(UserVo::new)
                     .orElseThrow(
                         () -> new UserNotFoundException(String.format("no user found for name %s", username)));
  }

}
