/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.exception.ProtocolVotingPeriodInitializationException;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolHandlerChain;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolState;
import ch.ge.ve.chvote.pactback.service.protocol.processor.ProtocolStateFactory;
import ch.ge.ve.chvote.pactback.service.protocol.processor.RealElectoralOfficerPublicKeyPublicationHandler;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * A service to start a voting period for a {@link ProtocolInstance}.
 */
@Service
public class ProtocolVotingPeriodInitializationService {
  private static final Logger log = LoggerFactory.getLogger(ProtocolVotingPeriodInitializationService.class);

  private final RealElectoralOfficerPublicKeyPublicationHandler sendElectoralOfficerPublicKeyHandler;
  private final ProtocolStateFactory                            protocolStateFactory;

  /**
   * Create a new {@link ProtocolVotingPeriodInitializationService}.
   *
   * @param sendElectoralOfficerPublicKeyHandler the {@link RealElectoralOfficerPublicKeyPublicationHandler}.
   * @param protocolStateFactory                 the {@link ProtocolStateFactory}.
   */
  @Autowired
  public ProtocolVotingPeriodInitializationService(
      RealElectoralOfficerPublicKeyPublicationHandler sendElectoralOfficerPublicKeyHandler,
      ProtocolStateFactory protocolStateFactory) {
    this.sendElectoralOfficerPublicKeyHandler = sendElectoralOfficerPublicKeyHandler;
    this.protocolStateFactory = protocolStateFactory;
  }

  private void execute(ProtocolHandlerChain chain, ProtocolState state) {
    try {
      chain.proceed(state);
    } catch (Exception e) {
      log.error("Failed to start voting period", e);
      sendElectoralOfficerPublicKeyHandler.indicateFailure(state);
      throw new ProtocolVotingPeriodInitializationException(
          String.format("Unable to start voting period for operation [%s]", state.getOperation().getClientId()), e);
    }
  }

  /**
   * Start the voting period for the given {@link ProtocolInstance} asynchronously. The configuration for
   * this {@link ProtocolInstance} will be retrieved from {@link ProtocolInstance#getConfiguration()}.
   *
   * @param instance the {@link ProtocolInstance} to start.
   */
  @Async
  public void initVotingPeriod(ProtocolInstance instance) {
    ProtocolState state = protocolStateFactory.initProtocolState(instance);
    state.getClient().setPublicParameters(state.getPublicParametersFactory().createPublicParameters());

    ProtocolHandlerChain chain = new ProtocolHandlerChain(
        Collections.singletonList(sendElectoralOfficerPublicKeyHandler),
        () -> state.updateProtocolStatus(ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES));

    execute(chain, state);
  }

}
