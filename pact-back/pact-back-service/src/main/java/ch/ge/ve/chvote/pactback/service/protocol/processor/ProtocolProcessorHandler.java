/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

/**
 * Represents a node in a {@link ProtocolHandlerChain}. Implementing classes must be stateless, the state
 * is contained in the {@link ProtocolState} message.
 */
public abstract class ProtocolProcessorHandler {

  /**
   * Process the given {@link ProtocolState} message and calls the next {@link ProtocolProcessorHandler} of the
   * given chain.
   *
   * @param state the {@link ProtocolState} to process.
   * @param chain the {@link ProtocolHandlerChain}.
   */
  public void process(ProtocolState state, ProtocolHandlerChain chain) {
    this.process(state);
    chain.proceed(state);
  }

  /**
   * Process the given {@link ProtocolState} and updates it if needed.
   *
   * @param state the {@link ProtocolState}.
   */
  protected abstract void process(ProtocolState state);

  /**
   * Check whether this step has already been passed by a {@code ProtocolState} (and its underlying {@code ProtocolInstance}).
   *
   * @param state the state wrapping the protocol instance to check
   * @return {@code true} if this handler has already (successfully) processed the given {@code ProtocolState}
   */
  public abstract boolean hasBeenProcessed(ProtocolState state);

}
