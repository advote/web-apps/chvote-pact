/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation;

import java.time.LocalDateTime;

/**
 * Public API provided by the "PACT-Scheduler".
 */
public interface PactSchedulerApi {

  /**
   * Creates a task to trigger the closure of an operation.
   * <p>
   *   A task will be scheduled to demand the PACT to generate the tally archive for this operation.
   * </p>
   *
   * @param clientId operation identifier in the client system
   * @param scheduledTime datetime when to trigger the tally-archive generation
   */
  void register(String clientId, LocalDateTime scheduledTime);

  /**
   * Removes a task configured for an operation.
   * <p>
   *   Can be used either to delete obsolete configuration (already triggered operation) or to cancel
   *   a scheduled task (not-yet-triggered operation).
   * </p>
   * <p>
   *   Note : If no task has been registered for this clientId, this method should return as no-op.
   * </p>
   *
   * @param clientId operation identifier in the client system
   */
  void unregister(String clientId);

}
