/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.OperationConfigurationRepository;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class to load the operation reference files from the database
 */
@Service
public class OperationReferenceFilesLoader {

  private final OperationConfigurationRepository repository;

  public OperationReferenceFilesLoader(OperationConfigurationRepository repository) {
    this.repository = repository;
  }

  /**
   * Loads the attachments in memory from the database.
   *
   * @param configurationId id of the {@link OperationConfiguration}
   * @param attachmentType  type of attachment to load
   *
   * @return an array of input streams for each file of type {@code attachmentType}
   */
  @Transactional(readOnly = true)
  public InputStream[] loadAttachments(Long configurationId, AttachmentType attachmentType) {
    OperationConfiguration configuration = repository.getOne(configurationId);

    return configuration.getOperationReferenceFiles()
                        .stream()
                        .filter(
                            file -> attachmentType == file.getType())
                        .map(OperationReferenceFile::getFile)
                        .map(ByteArrayInputStream::new)
                        .toArray(InputStream[]::new);
  }

  /**
   * Loads the operation repositories from the database, mapped by attachement name
   *
   * @param configurationId id of the {@link OperationConfiguration}
   *
   * @return a map keyed by attachement name of the operation repositories streams
   */
  @Transactional
  public Map<String, InputStream> loadRepositoriesByName(Long configurationId) {
    OperationConfiguration configuration = repository.getOne(configurationId);

    return configuration.getOperationReferenceFiles()
                        .stream()
                        .filter(
                            file -> AttachmentType.OPERATION_REPOSITORY_VOTATION == file.getType() ||
                                    AttachmentType.OPERATION_REPOSITORY_ELECTION == file.getType())
                        .collect(Collectors.toMap(Attachment::getName,
                                                  file -> new ByteArrayInputStream(file.getFile())));
  }
}
