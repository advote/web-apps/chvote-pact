/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;

import ch.ge.ve.chvote.pactback.service.protocol.vo.DeadLetterEventVo;
import ch.ge.ve.chvote.pactback.service.protocol.vo.DeadLetterEventVoBuilder;
import ch.ge.ve.event.validator.InvalidEventMessageRuntimeException;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.DlxEventHeaders;
import ch.ge.ve.service.EventHeaders;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A component that listens to the dead letter exchange and persists those messages.
 */
@Component
public class DeadLetterListener {

  private static final Logger logger = LoggerFactory.getLogger(DeadLetterListener.class);

  private final DeadLetterEventService deadLetterEventService;

  @Autowired
  public DeadLetterListener(DeadLetterEventService deadLetterEventService) {
    this.deadLetterEventService = deadLetterEventService;
  }

  /**
   * Persists the given event as a {@link ch.ge.ve.chvote.pactback.repository.protocol.entity.DeadLetterEvent}.
   *
   * @param message the event to persist.
   */
  @RabbitListener(
      containerFactory = "rabbitListenerContainerFactory",
      bindings = @QueueBinding(
          value = @Queue(value = Channels.DLX + "-pact-listener", durable = "true"),
          exchange = @Exchange(value = Channels.DLX, type = ExchangeTypes.FANOUT, durable = "true")
      )
  )
  public void processDeadLetterEvent(Message message) {
    logger.debug("Received an event from the DLX");
    final MessageHeaders headers = MessageHeaders.from(message);
    logHeaders(headers);
    final DeadLetterEventVoBuilder builder = DeadLetterEventVo.builder();
    populatePublicationData(builder, headers);
    populateErrorData(builder, headers);
    populateTimeData(builder, headers);
    MessageProperties messageProperties = message.getMessageProperties();
    Charset charset = Optional.ofNullable(messageProperties.getContentEncoding())
                              .map(Charset::forName).orElse(StandardCharsets.UTF_8);
    builder.setContent(new String(message.getBody(), charset))
           .setContentType(messageProperties.getContentType());
    deadLetterEventService.save(builder.build());
  }

  private static void logHeaders(MessageHeaders headers) {
    if (logger.isDebugEnabled()) {
      headers.forEach((k, v) -> logger.debug("[header] {} = {} ({})", k, v, v.getClass().getSimpleName()));
    }
  }

  private static void populatePublicationData(DeadLetterEventVoBuilder builder, MessageHeaders headers) {
    builder.setOriginalChannel(headers.getMandatory(DlxEventHeaders.ORIGINAL_CHANNEL));
    headers.getOptional(DlxEventHeaders.ORIGINAL_ROUTING_KEY).ifPresent(builder::setOriginalRoutingKey);
    builder.setProtocolId(headers.getMandatory(EventHeaders.PROTOCOL_ID));
    builder.setType(headers.getMandatory("__TypeId__"));
    logger.info("Read event {} from DLX, originally published in exchange {} with routing key {} for pid {}",
                builder.getType(), builder.getOriginalChannel(), builder.getOriginalRoutingKey(),
                builder.getProtocolId());
  }

  private static void populateErrorData(DeadLetterEventVoBuilder builder, MessageHeaders headers) {
    headers.getOptional(DlxEventHeaders.EXCEPTION_MESSAGE).ifPresent(builder::setExceptionMessage);
    headers.getOptional(DlxEventHeaders.EXCEPTION_STACK_TRACE).ifPresent(builder::setExceptionStacktrace);
    logger.info("\tException information: {} - {}", builder.getExceptionMessage(), builder.getExceptionStacktrace());
  }

  private static void populateTimeData(DeadLetterEventVoBuilder builder, MessageHeaders headers) {
    builder.setPickupDate(LocalDateTime.now());
    headers.getOptional(EventHeaders.CREATION_DATE)
           .map(date -> LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME))
           .ifPresent(builder::setCreationDate);
    logger.info("\tCreated on {} - Picked up on {}", builder.getCreationDate(), builder.getPickupDate());
  }

  private static final class MessageHeaders {
    static MessageHeaders from(Message m) {
      return new MessageHeaders(m.getMessageProperties().getHeaders());
    }

    private final Map<String, Object> headers;

    private MessageHeaders(Map<String, Object> headers) {
      this.headers = headers;
    }

    Optional<String> getOptional(String name) {
      return Optional.ofNullable(headers.get(name)).map(Object::toString);
    }

    String getMandatory(String name) {
      return getOptional(name)
          .orElseThrow(() -> new InvalidEventMessageRuntimeException("Missing mandatory header " + name));
    }

    void forEach(BiConsumer<? super String, ? super Object> consumer) {
      headers.forEach(consumer);
    }
  }
}
