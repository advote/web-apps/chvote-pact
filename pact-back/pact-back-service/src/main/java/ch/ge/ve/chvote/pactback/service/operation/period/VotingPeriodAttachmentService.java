/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.period;

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.exception.AttachmentParsingException;
import ch.ge.ve.chvote.pactback.service.operation.TimeService;
import ch.ge.ve.chvote.pactback.service.operation.configuration.attachment.AttachmentService;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * A service for managing the attachment (election officer's public key) to voting period.
 */
@Service
public class VotingPeriodAttachmentService extends AttachmentService {

  @Autowired
  public VotingPeriodAttachmentService(TimeService timeService,
                                       ChVoteConfigurationProperties chVoteConfigurationProperties) {
    super(timeService, chVoteConfigurationProperties);
  }

  /**
   * Creates the election officer's public key file.
   *
   * @param attachmentFileEntryVo    the {@link AttachmentFileEntryVo} metadata
   * @param electionOfficerPublicKey the file containing the election officer's public key
   */
  PublicKeyFile createPublicKeyFile(MultipartFile electionOfficerPublicKey,
                                    AttachmentFileEntryVo attachmentFileEntryVo) {
    PublicKeyFile publicKeyFile = new PublicKeyFile();
    try {
      byte[] fileContents = ByteStreams.toByteArray(electionOfficerPublicKey.getInputStream());
      populate(publicKeyFile, AttachmentType.ELECTION_OFFICER_KEY, electionOfficerPublicKey.getName(),
               attachmentFileEntryVo.getImportDateTime(), fileContents, attachmentFileEntryVo.getExternalIdentifier());
    } catch (IOException ex) {
      throw new AttachmentParsingException(
          String.format("An exception occurred while reading file [%s]",
                        electionOfficerPublicKey.getOriginalFilename()), ex);
    }
    return publicKeyFile;
  }

}
