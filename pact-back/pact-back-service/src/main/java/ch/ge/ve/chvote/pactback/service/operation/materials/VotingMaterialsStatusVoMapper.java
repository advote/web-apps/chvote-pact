/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.materials;

import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.AVAILABLE_FOR_CREATION;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_FAILED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_IN_PROGRESS;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_REJECTED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_REQUESTED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATION_REJECTED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATION_REQUESTED;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.VALIDATED;

import ch.ge.ve.chvote.pactback.contract.operation.status.ProtocolStageProgressVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsCreation;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties;
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException;
import ch.ge.ve.chvote.pactback.service.protocol.processor.OutputFilesManager;
import ch.ge.ve.chvote.pactback.service.protocol.processor.PrinterArchiveGenerator;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A utility class for mapping {@link Operation} to {@link VotingMaterialsStatusVo}.
 */
@Service
public class VotingMaterialsStatusVoMapper {

  private final OutputFilesManager      outputFilesManager;
  private final PrinterArchiveGenerator printerArchiveGenerator;
  private final Duration                votingMaterialsTimeout;
  private final String                  pactContextUrl;

  @Autowired
  public VotingMaterialsStatusVoMapper(OutputFilesManager outputFilesManager,
                                       PrinterArchiveGenerator printerArchiveGenerator,
                                       ChVoteConfigurationProperties configurationProperties) {
    this.outputFilesManager = outputFilesManager;
    this.printerArchiveGenerator = printerArchiveGenerator;

    this.votingMaterialsTimeout = Duration.of(
        configurationProperties.getProtocolCore().getVotingMaterialsTimeoutMinutes(),
        ChronoUnit.MINUTES
    );

    this.pactContextUrl = configurationProperties.getPactContextUrl();
  }

  /**
   * Creates a new {@link VotingMaterialsStatusVo} for the given {@link VotingMaterialsConfiguration}.
   *
   * @param operation the {@link Operation} to map to a voting materials status.
   *
   * @return the mapped {@link VotingMaterialsStatusVo}.
   */
  public VotingMaterialsStatusVo map(Operation operation) {

    return new VotingMaterialsStatusVo(
        userForLastChange(operation),
        dateForLastChange(operation),
        state(operation),
        rejectionReason(operation),
        deploymentPageUrl(operation),
        invalidationPageUrl(operation),
        getVotingCardLocations(operation),
        getProtocolStageProgress(operation)
    );

  }


  private List<ProtocolStageProgressVo<VotingMaterialCreationStage>> getProtocolStageProgress(Operation operation) {
    VotingMaterialsConfiguration configuration = getVotingMaterialsConfiguration(operation);
    if (isVotingMaterialConfigurationApprovedAndNotInvalidated(configuration)) {
      return ProtocolStageProgressVoMapper.mapProgress(getProtocolInstance(operation));
    } else {
      return Collections.emptyList();
    }
  }

  private boolean isVotingMaterialInvalidated(VotingMaterialsConfiguration configuration) {
    return configuration.getInvalidationAction()
                        .map(action -> action.getStatus() == PrivilegedAction.Status.APPROVED)
                        .orElse(false);
  }

  private Map<String, String> getVotingCardLocations(Operation operation) {
    VotingMaterialsConfiguration configuration = getVotingMaterialsConfiguration(operation);

    if (isVotingMaterialConfigurationApprovedAndNotInvalidated(configuration)) {
      ProtocolInstance instance = getProtocolInstance(operation);
      List<PrinterConfiguration> printerConfigurations = configuration.getPrinters();

      final Path outputPath = outputFilesManager.getOutputPath(instance);

      return printerConfigurations
          .stream()
          .map(PrinterConfiguration::getBusinessIdentifier)
          .distinct()
          .filter(printerId -> instance.getPrinterArchiveCreationDate() != null &&
                               printerArchiveGenerator.getArchivePath(
                                   instance.getConfiguration().getOperationLabel(),
                                   outputPath,
                                   printerId,
                                   instance.getPrinterArchiveCreationDate()
                               ).toFile().exists()
          ).collect(
              Collectors.toMap(Function.identity(),
                               printerId -> {
                                 Path archivePath = printerArchiveGenerator.getArchivePath(
                                     instance.getConfiguration().getOperationLabel(),
                                     outputPath,
                                     printerId,
                                     instance.getPrinterArchiveCreationDate()
                                 );
                                 return outputFilesManager.relativize(archivePath).toString().replaceAll("\\\\", "/");
                               })
          );
    } else {
      return Collections.emptyMap();
    }
  }

  private String userForLastChange(Operation operation) {
    VotingMaterialsConfiguration configuration = getVotingMaterialsConfiguration(operation);

    Optional<VotingMaterialsInvalidation>
        invalidationAction = operation.fetchVotingMaterialsConfiguration().getInvalidationAction();

    if (invalidationAction.isPresent()) {
      if (invalidationAction
          .map(action -> action.getStatus() == PrivilegedAction.Status.PENDING).orElse(false)) {
        return invalidationAction.get().getRequester().getUsername();
      }

      if (invalidationAction
          .map(action -> action.getStatus() == PrivilegedAction.Status.REJECTED).orElse(false)) {
        return invalidationAction.get().getValidator().getUsername();
      }
    }

    if (configuration.getValidationStatus() == ValidationStatus.VALIDATED ||
        configuration.getValidationStatus() == ValidationStatus.INVALIDATED) {
      return configuration.getValidator();
    }


    // voting materials creation process does not change the last change user
    return configuration
        .getCreationAction()
        .map(
            action -> Optional.ofNullable(action.getValidator())
                              .orElse(action.getRequester()).getUsername()
        )
        .orElse(configuration.getAuthor());
  }

  private boolean isVotingMaterialConfigurationApprovedAndNotInvalidated(VotingMaterialsConfiguration configuration) {
    if (isVotingMaterialInvalidated(configuration)) {
      return false;
    }
    Optional<VotingMaterialsCreation> creationAction = configuration.getCreationAction();
    return creationAction.isPresent() && creationAction.get().getStatus().equals(PrivilegedAction.Status.APPROVED);
  }

  private LocalDateTime dateForLastChange(Operation operation) {
    VotingMaterialsConfiguration configuration = getVotingMaterialsConfiguration(operation);

    Optional<VotingMaterialsInvalidation>
        invalidationAction = operation.fetchVotingMaterialsConfiguration().getInvalidationAction();
    if (invalidationAction.isPresent() && invalidationAction
        .map(action -> (action.getStatus() == PrivilegedAction.Status.PENDING ||
                        action.getStatus() == PrivilegedAction.Status.REJECTED))
        .orElse(false)) {
      return invalidationAction.get().getStatusDate();
    }


    if (configuration.getValidationStatus() == ValidationStatus.VALIDATED ||
        configuration.getValidationStatus() == ValidationStatus.INVALIDATED) {
      return configuration.getValidationDate();
    }

    if (isVotingMaterialConfigurationApprovedAndNotInvalidated(configuration)) {
      return getProtocolInstance(operation).getStatusDate();
    } else {
      return configuration
          .getCreationAction()
          .map(PrivilegedAction::getStatusDate)
          .orElse(configuration.getSubmissionDate());
    }
  }

  private VotingMaterialsStatusVo.State state(Operation operation) {
    VotingMaterialsConfiguration configuration = getVotingMaterialsConfiguration(operation);

    if (isVotingMaterialInvalidated(configuration)) {
      return INVALIDATED;
    }

    if (isVotingMaterialConfigurationApprovedAndNotInvalidated(configuration)) {
      return mapMaterialsCreationState(operation);
    }

    return configuration.getCreationAction()
                        .map(this::mapActionState)
                        .orElse(AVAILABLE_FOR_CREATION);

  }

  private String rejectionReason(Operation operation) {
    return getVotingMaterialsConfiguration(operation)
        .getCreationAction()
        .map(PrivilegedAction::getRejectionReason).orElse(null);
  }


  private VotingMaterialsConfiguration getVotingMaterialsConfiguration(Operation operation) {
    return operation.getVotingMaterialsConfiguration()
                    .orElseThrow(() -> new EntityNotFoundException(
                        String.format("No voting materials configuration found for clientId [%s]",
                                      operation.getClientId()))
                    );
  }

  private ProtocolInstance getProtocolInstance(Operation operation) {
    return operation.getDeployedConfiguration().orElseThrow(
        () -> new IllegalStateException(
            String.format("No deployed configuration found for clientId [%s]", operation.getClientId()))
    ).getProtocolInstance().orElseThrow(
        () -> new IllegalStateException(
            String.format("No protocol instance found for clientId [%s]", operation.getClientId()))
    );
  }

  private VotingMaterialsStatusVo.State mapActionState(VotingMaterialsCreation action) {
    switch (action.getStatus()) {
      case PENDING:
        return CREATION_REQUESTED;
      case APPROVED:
        return CREATION_IN_PROGRESS;
      case REJECTED:
        return CREATION_REJECTED;
      default:
        throw new IllegalStateException(
            "Unexpected materials creation action status [" + action.getStatus() + "]");
    }
  }

  private VotingMaterialsStatusVo.State mapMaterialsCreationState(Operation operation) {

    if (operation.fetchVotingMaterialsConfiguration().getInvalidationAction()
                 .map(action -> action.getStatus() == PrivilegedAction.Status.PENDING).orElse(false)) {
      return INVALIDATION_REQUESTED;
    }

    if (operation.fetchVotingMaterialsConfiguration().getInvalidationAction()
                 .map(action -> action.getStatus() == PrivilegedAction.Status.REJECTED).orElse(false)) {
      return INVALIDATION_REJECTED;
    }

    ValidationStatus status = getVotingMaterialsConfiguration(operation).getValidationStatus();
    if (status == ValidationStatus.VALIDATED) {
      return VALIDATED;
    }
    if (status == ValidationStatus.INVALIDATED) {
      return INVALIDATED;
    }


    ProtocolInstance protocolInstance = getProtocolInstance(operation);
    ProtocolInstanceStatus protocolInstanceStatus = protocolInstance.getStatus();

    switch (protocolInstanceStatus) {
      case INITIALIZING:
      case GENERATING_KEYS:
      case KEYS_GENERATED:
      case PUBLIC_PARAMETERS_INITIALIZED:
      case GENERATING_CREDENTIALS:
        // fallthrough intended to catch all the protocol instance statuses until the credentials are generated
        if (LocalDateTime.now().compareTo(protocolInstance.getStatusDate().plus(votingMaterialsTimeout)) > 0) {
          return CREATION_FAILED;
        } else {
          return CREATION_IN_PROGRESS;
        }
      case CREDENTIALS_GENERATION_FAILURE:
        return CREATION_FAILED;
      case ELECTION_OFFICER_KEY_INITIALIZING:
      case ELECTION_OFFICER_KEY_INITIALIZED:
      case ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE:
      case CREDENTIALS_GENERATED:
      case READY_TO_RECEIVE_VOTES:
      case GENERATING_RESULTS:
      case SHUFFLING:
      case DECRYPTING:
      case RESULTS_AVAILABLE:
      case TALLY_ARCHIVE_GENERATION_FAILURE:
      case NOT_ENOUGH_VOTES_TO_INITIATE_TALLY:
        return CREATED;
      default:
        // default to catch prospective new protocol instance states that would not be classified
        // in this mapping function
        throw new IllegalStateException(String.format(
            "Unexpected protocol instance status [%s] with voting material validation status [%s]",
            protocolInstanceStatus, status)
        );
    }
  }

  private String deploymentPageUrl(Operation operation) {
    Long id = getVotingMaterialsConfiguration(operation).getId();
    return String.format("%s/voting-materials-creation/%s", pactContextUrl, id);
  }

  private String invalidationPageUrl(Operation operation) {
    Long id = getVotingMaterialsConfiguration(operation).getId();
    return String.format("%s/voting-materials-invalidation/%s", pactContextUrl, id);
  }

}
