/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.action.vo;

import ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle;
import ch.ge.ve.chvote.pactback.repository.action.materials.entity.VotingMaterialsInvalidation;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link VotingMaterialsInvalidation} value object.
 */
public class VotingMaterialsInvalidationVo extends PrivilegedActionVo {

  private final Long votingMaterialsConfigurationId;

  private final List<PrinterStatsVO> votersCreationStats;


  /**
   * Creates a new voting materials invalidation value object based on the given {@link VotingMaterialsInvalidation}
   * entity.
   *
   * @param action              the {@link VotingMaterialsInvalidation} entity
   * @param votersCreationStats the list of {@link VotersCreationStats} generated during voting materials creation
   */
  public VotingMaterialsInvalidationVo(VotingMaterialsInvalidation action,
                                       List<VotersCreationStats> votersCreationStats) {
    super(action);
    votingMaterialsConfigurationId = action.getVotingMaterialsConfiguration().getId();
    this.votersCreationStats = mapStats(votersCreationStats);
  }

  private List<PrinterStatsVO> mapStats(List<VotersCreationStats> votersCreationStats) {
    return votersCreationStats
        .stream()
        .collect(Collectors.groupingBy(VotersCreationStats::getPrinterAuthorityName))
        .entrySet().stream()
        .map(statsByPrinter -> new PrinterStatsVO(statsByPrinter.getKey(), statsByPrinter.getValue()))
        .sorted(Comparator.comparing(PrinterStatsVO::getPrinter))
        .collect(Collectors.toList());
  }


  public VotingMaterialsInvalidationVo(Operation operation, String operationName, LocalDateTime operationDate,
                                       List<VotersCreationStats> votersCreationStats) {
    super(VotingMaterialsInvalidation.TYPE,
          operationName,
          operationDate,
          operation.fetchVotingMaterialsConfiguration().getAuthor(),
          operation.fetchVotingMaterialsConfiguration().getSubmissionDate());

    votingMaterialsConfigurationId = operation.getVotingMaterialsConfiguration()
                                              .map(VotingMaterialsConfiguration::getId)
                                              .orElse(null); // Actually not possible
    this.votersCreationStats = mapStats(votersCreationStats);
  }

  public Long getVotingMaterialsConfigurationId() {
    return votingMaterialsConfigurationId;
  }

  public List<PrinterStatsVO> getVotersCreationStats() {
    return votersCreationStats;
  }


  private static class PrinterStatsVO {

    private final String                      printer;
    private final List<CountingCircleStatsVO> statByCountingCircle;
    private final Long                        controllerTestingCards;
    private final Long                        printableTestingCards;
    private final Long                        nonPrintableTestingCards;
    private final Long                        printerTestingCards;


    PrinterStatsVO(String printer, Collection<VotersCreationStats> votersCreationStats) {
      this.printer = printer;
      statByCountingCircle = votersCreationStats
          .stream()
          .filter(stats -> !VirtualCountingCircle.byId(stats.getCountingCircleBusinessId()).isPresent())
          .map(CountingCircleStatsVO::new)
          .sorted(Comparator.comparing(o -> o.countingCircleName))
          .collect(Collectors.toList());
      controllerTestingCards = getVirtualCountingCircleStats(votersCreationStats,
                                                             VirtualCountingCircle.CONTROLLER_TESTING_CARDS);
      printableTestingCards = getVirtualCountingCircleStats(votersCreationStats,
                                                            VirtualCountingCircle.PRINTABLE_TESTING_CARDS);
      nonPrintableTestingCards = getVirtualCountingCircleStats(votersCreationStats,
                                                               VirtualCountingCircle.NOT_PRINTABLE_TESTING_CARDS);
      printerTestingCards = getVirtualCountingCircleStats(votersCreationStats,
                                                          VirtualCountingCircle.PRINTER_TESTING_CARDS);

    }

    private Long getVirtualCountingCircleStats(
        Collection<VotersCreationStats> votersCreationStats, VirtualCountingCircle type) {

      return votersCreationStats
          .stream()
          .filter(stats -> VirtualCountingCircle.byId(stats.getCountingCircleBusinessId())
                                                .map(virtualCountingCircle -> virtualCountingCircle == type)
                                                .orElse(false))
          .findFirst()
          .map(VotersCreationStats::getNumberOfVoters)
          .orElse(0L);
    }

    public String getPrinter() {
      return printer;
    }

    public List<CountingCircleStatsVO> getStatByCountingCircle() {
      return statByCountingCircle;
    }

    public Long getControllerTestingCards() {
      return controllerTestingCards;
    }

    public Long getPrintableTestingCards() {
      return printableTestingCards;
    }

    public Long getNonPrintableTestingCards() {
      return nonPrintableTestingCards;
    }

    public Long getPrinterTestingCards() {
      return printerTestingCards;
    }

  }

  private static class CountingCircleStatsVO {

    private final String countingCircleName;
    private final long   numberOfVoters;

    private CountingCircleStatsVO(VotersCreationStats votersCreationStats) {
      numberOfVoters = votersCreationStats.getNumberOfVoters();
      countingCircleName = votersCreationStats.getCountingCircleName();
    }


    public String getCountingCircleName() {
      return countingCircleName;
    }

    public long getNumberOfVoters() {
      return numberOfVoters;
    }
  }
}
