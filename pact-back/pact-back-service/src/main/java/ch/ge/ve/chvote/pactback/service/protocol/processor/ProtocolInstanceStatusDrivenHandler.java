/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.processor;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;

/**
 * A specialization of {@link ProtocolProcessorHandler} that manages {@link ProtocolInstanceStatus} change in case of
 * success and failure of the processing.
 */
public abstract class ProtocolInstanceStatusDrivenHandler extends ProtocolProcessorHandler {

  private final ProtocolInstanceStatus successStatus;
  private final ProtocolInstanceStatus failureStatus;

  /**
   * Instantiation with the {@link ProtocolInstanceStatus} values to set.
   *
   * @param successStatus status in case of success.
   * @param failureStatus status in case of failure.
   */
  protected ProtocolInstanceStatusDrivenHandler(ProtocolInstanceStatus successStatus,
                                                ProtocolInstanceStatus failureStatus) {
    super();
    this.successStatus = successStatus;
    this.failureStatus = failureStatus;
  }

  @Override
  protected final void process(ProtocolState state) {
    try {
      doProcess(state);
      state.updateProtocolStatus(successStatus);
    } catch (Exception e) {
      indicateFailure(state);
      throw new HandlerProcessingFailedException("Failed while processing " + getClass().getSimpleName(), e);
    }
  }

  /**
   * Process the given {@link ProtocolState} and updates it if needed.
   * <p>
   * This execution is surrounded by a try-catch bloc, and the ProtocolInstanceStatus is updated via the {@code state}
   * object at the end (success or failure status is set).
   * </p>
   *
   * @param state the {@link ProtocolState}
   *
   * @throws Exception any type of exception could be thrown here - will be translated to / encapsulated in {@link
   *                   HandlerProcessingFailedException} by the try-catch bloc
   */
  protected abstract void doProcess(ProtocolState state) throws Exception;

  /**
   * Updates the ProtocolInstanceStatus to its failure value.
   *
   * @param state the state wrapping the protocol instance to update
   */
  public void indicateFailure(ProtocolState state) {
    if (state.getInstance().getStatus() != failureStatus) {
      state.updateProtocolStatus(failureStatus);
    }
  }

  /**
   * The handler is considered processed if the ProtocolInstanceStatus has been set to its success value (or further).
   */
  @Override
  public boolean hasBeenProcessed(ProtocolState state) {
    return state.isProtocolInstanceStatusAtOrAfter(successStatus)
           && state.getInstance().getStatus() != failureStatus;
  }
}
