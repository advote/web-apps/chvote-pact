/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol.events;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import java.util.Objects;

/**
 * Event raised when the status of a {@link ProtocolInstance} changes.
 * <p>
 *   Both statuses before / after the change are directly registered, because this event can be intercepted
 *   around transaction phases by a {@link org.springframework.transaction.event.TransactionalEventListener}
 *   and we want to indicate the status transition in a transaction-agnostic way.
 * </p>
 *
 * @see org.springframework.context.ApplicationEventPublisher Spring's ApplicationEventPublisher
 * @see ch.ge.ve.chvote.pactback.service.protocol.ProtocolInstanceService ProtocolInstanceService
 */
public final class ProtocolStatusChangeEvent {

  private final ProtocolInstance protocolInstance;
  private final ProtocolInstanceStatus statusBefore;
  private final ProtocolInstanceStatus statusAfter;

  public ProtocolStatusChangeEvent(ProtocolInstance protocolInstance,
                                   ProtocolInstanceStatus statusBefore,
                                   ProtocolInstanceStatus statusAfter) {
    this.protocolInstance = protocolInstance;
    this.statusBefore = statusBefore;
    this.statusAfter = statusAfter;
  }

  public ProtocolInstance getProtocolInstance() {
    return protocolInstance;
  }

  public ProtocolInstanceStatus getStatusBefore() {
    return statusBefore;
  }

  public ProtocolInstanceStatus getStatusAfter() {
    return statusAfter;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProtocolStatusChangeEvent that = (ProtocolStatusChangeEvent) o;
    return Objects.equals(protocolInstance, that.protocolInstance) &&
           statusBefore == that.statusBefore &&
           statusAfter == that.statusAfter;
  }

  @Override
  public int hashCode() {
    return Objects.hash(protocolInstance, statusBefore, statusAfter);
  }
}
