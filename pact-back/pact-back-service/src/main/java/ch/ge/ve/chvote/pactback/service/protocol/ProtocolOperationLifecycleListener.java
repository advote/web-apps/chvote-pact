/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol;


import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.service.operation.PactSchedulerApi;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent;
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * Listens to events indicating a move in the lifecycle of an operation via the protocol instance's status.
 */
@Component
public class ProtocolOperationLifecycleListener {

  private final int              technicalDelay;
  private final PactSchedulerApi pactSchedulerApi;

  @Autowired
  public ProtocolOperationLifecycleListener(@Value("${chvote.pact.scheduler.technical-delay:0}") int technicalDelay,
                                            PactSchedulerApi pactSchedulerApi) {
    this.technicalDelay = technicalDelay;
    this.pactSchedulerApi = pactSchedulerApi;
  }

  /**
   * Registers the operation to the PACT-Scheduler on deployment.
   * <p>
   *   If the {@code ProtocolInstance} is deployed to PRODUCTION and READY_TO_RECEIVE_VOTES, a scheduled task
   *   must be registered to generate the results (the tally-archive) after the voting period ends.
   * </p>
   * <p>
   *   The task is scheduled on <code>siteClosingDate + gracePeriod (mn) + technicalDelay (mn)</code>.
   * </p>
   * <p>
   *   The event is handled before the transaction commits, so that if the registration fails the status
   *   will not be committed.
   * </p>
   *
   * @param event the event published when the {@code ProtocolInstance}'s status changes
   */
  @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
  public void registerOperationForScheduledEnding(ProtocolStatusChangeEvent event) {
    final ProtocolInstance protocolInstance = event.getProtocolInstance();
    if (protocolInstance.getEnvironment() == ProtocolEnvironment.PRODUCTION
        && event.getStatusAfter() == ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES) {

      final VotingPeriodConfiguration votingPeriod = protocolInstance.getOperation().fetchVotingPeriodConfiguration();
      final LocalDateTime scheduledTime = votingPeriod.getSiteClosingDate()
                                                      .plusMinutes(votingPeriod.getGracePeriod())
                                                      .plusMinutes(technicalDelay);

      pactSchedulerApi.register(protocolInstance.getOperation().getClientId(), scheduledTime);
    }
  }

  /**
   * Unregisters the operation from the PACT-Scheduler when it is undeployed.
   *
   * @param event the event publish when the {@code ProtocolInstance} is deleted
   */
  @TransactionalEventListener
  public void unregisterUndeployedOperation(ProtocolDeletionEvent event) {
    final ProtocolInstance protocolInstance = event.getProtocolInstance();
    if (protocolInstance.getEnvironment() == ProtocolEnvironment.PRODUCTION) {
      pactSchedulerApi.unregister(protocolInstance.getOperation().getClientId());
    }
  }

}
