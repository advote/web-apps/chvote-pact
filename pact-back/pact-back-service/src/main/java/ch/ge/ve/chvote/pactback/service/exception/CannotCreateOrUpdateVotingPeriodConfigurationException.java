/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.exception;

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;

/**
 * Signals that a voting period configuration cannot be created or updated.
 */
public class CannotCreateOrUpdateVotingPeriodConfigurationException extends RuntimeException {

  private static final String CANNOT_CREATE_OR_UPDATE_VOTING_PERIOD_CONFIGURATION_FOR_OPERATION_S =
      "Cannot create or update voting period configuration for Operation [%s] ";

  private CannotCreateOrUpdateVotingPeriodConfigurationException(String message) {
    super(message);
  }

  public static CannotCreateOrUpdateVotingPeriodConfigurationException noVotingMaterial(String clientId) {
    return new CannotCreateOrUpdateVotingPeriodConfigurationException(
        String.format(CANNOT_CREATE_OR_UPDATE_VOTING_PERIOD_CONFIGURATION_FOR_OPERATION_S +
                      "because voting material hasn't been sent", clientId));
  }

  public static CannotCreateOrUpdateVotingPeriodConfigurationException noConfigurationDeployed(String clientId) {
    return new CannotCreateOrUpdateVotingPeriodConfigurationException(
        String.format(CANNOT_CREATE_OR_UPDATE_VOTING_PERIOD_CONFIGURATION_FOR_OPERATION_S +
                      "because configuration hasn't been deployed", clientId));
  }

  public static CannotCreateOrUpdateVotingPeriodConfigurationException wrongDateForRealOperation(String clientId) {
    return new CannotCreateOrUpdateVotingPeriodConfigurationException(
        String.format(CANNOT_CREATE_OR_UPDATE_VOTING_PERIOD_CONFIGURATION_FOR_OPERATION_S +
                      "because site opening and/or closing date has change since operation creation", clientId));
  }

  public static CannotCreateOrUpdateVotingPeriodConfigurationException wrongProtocolState(
      String clientId, ProtocolInstanceStatus status) {
    return new CannotCreateOrUpdateVotingPeriodConfigurationException(
        String.format(CANNOT_CREATE_OR_UPDATE_VOTING_PERIOD_CONFIGURATION_FOR_OPERATION_S +
                      "because protocol is in incompatible state [%s]", clientId, status));
  }

  public static CannotCreateOrUpdateVotingPeriodConfigurationException notRejected(String clientId) {
    return new CannotCreateOrUpdateVotingPeriodConfigurationException(
        String.format(
            "Cannot update voting period configuration for Operation [%s] because request has not been rejected before",
            clientId));
  }

  public static CannotCreateOrUpdateVotingPeriodConfigurationException votingMaterialNotValidated(String clientId) {
    return new CannotCreateOrUpdateVotingPeriodConfigurationException(String.format(
        "Cannot update voting period configuration for Operation [%s] because voting material has not benn validated",
        clientId));
  }
}
