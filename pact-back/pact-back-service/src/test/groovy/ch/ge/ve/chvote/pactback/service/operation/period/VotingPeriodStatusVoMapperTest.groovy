/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.period

import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.period.entity.VotingPeriodConfiguration
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.user.entity.User
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import java.time.LocalDateTime
import spock.lang.Specification
import spock.lang.Unroll

class VotingPeriodStatusVoMapperTest extends Specification {
  def static final SUBMISSION_DATE = LocalDateTime.of(2018, 1, 1, 10, 00)
  def static final STATUS_DATE = LocalDateTime.of(2018, 1, 2, 10, 00)

  VotingPeriodStatusVoMapper mapper

  void setup() {
    ChVoteConfigurationProperties configurationProperties = new ChVoteConfigurationProperties()

    configurationProperties.pactContextUrl = "/pact"
    configurationProperties.vrIdentificationUrl = "/vrIdentificationUrl"
    configurationProperties.protocolCore.votingPeriodTimeoutMinutes = 10
    mapper = new VotingPeriodStatusVoMapper(configurationProperties)
  }

  def "Map should fail if there is no voting period configuration"() {
    given:
    def operation = new Operation()
    operation.setClientId("clientId")


    when:
    mapper.map(operation)

    then:
    def exception = thrown(EntityNotFoundException)
    exception.message == "No voting period configuration found for clientId [clientId]"
  }


  def "Status should be in AVAILABLE_FOR_INITIALIZATION if configuration has just been sent"() {
    given:
    def operation = new Operation()

    operation.setClientId("clientId")
    def votingPeriodConfiguration = new VotingPeriodConfiguration(id: 1234)
    votingPeriodConfiguration.author = "configAuthor"
    votingPeriodConfiguration.submissionDate = SUBMISSION_DATE
    operation.setVotingPeriodConfiguration(votingPeriodConfiguration)

    when:
    def status = mapper.map(operation)

    then:
    status.state == VotingPeriodStatusVo.State.AVAILABLE_FOR_INITIALIZATION
    status.lastChangeDate == SUBMISSION_DATE
    status.lastChangeUser == "configAuthor"
    status.configurationPageUrl == "/pact/voting-period-initialization/1234"
  }

  def "Status should be in INITIALIZATION_REQUESTED if the request for initialization is pending"() {
    given:
    def operation = new Operation()
    operation.setClientId("clientId")
    def votingPeriodConfiguration = new VotingPeriodConfiguration()
    votingPeriodConfiguration.author = "configAuthor"
    votingPeriodConfiguration.submissionDate = SUBMISSION_DATE

    def votingPeriodInitialization = new VotingPeriodInitialization()
    votingPeriodInitialization.status = PrivilegedAction.Status.PENDING
    votingPeriodInitialization.requester = new User(username: "requester")
    votingPeriodInitialization.statusDate = STATUS_DATE
    votingPeriodConfiguration.action = votingPeriodInitialization

    operation.setVotingPeriodConfiguration(votingPeriodConfiguration)

    when:
    def status = mapper.map(operation)

    then:
    status.state == VotingPeriodStatusVo.State.INITIALIZATION_REQUESTED
    status.lastChangeDate == STATUS_DATE
    status.lastChangeUser == "requester"
  }


  def "Status should be in INITIALIZATION_REJECTED if the request for initialization is rejected"() {
    given:
    def operation = new Operation()
    operation.setClientId("clientId")
    def votingPeriodConfiguration = new VotingPeriodConfiguration()
    votingPeriodConfiguration.author = "configAuthor"
    votingPeriodConfiguration.submissionDate = SUBMISSION_DATE

    def votingPeriodInitialization = new VotingPeriodInitialization()
    votingPeriodInitialization.status = PrivilegedAction.Status.REJECTED
    votingPeriodInitialization.requester = new User(username:  "requester")
    votingPeriodInitialization.validator = new User(username:  "validator")
    votingPeriodInitialization.statusDate = STATUS_DATE
    votingPeriodConfiguration.action = votingPeriodInitialization

    operation.setVotingPeriodConfiguration(votingPeriodConfiguration)

    when:
    def status = mapper.map(operation)

    then:
    status.state == VotingPeriodStatusVo.State.INITIALIZATION_REJECTED
    status.lastChangeDate == STATUS_DATE
    status.lastChangeUser == "validator"
  }


  @Unroll
  def "Status should be in #expectedStatus if the request for initialization is approved and protocol status is #protocolStatus"() {
    given:
    def operation = prepareDeployedOperation(protocolStatus)
    def protocolInstance = operation.fetchDeployedConfiguration().fetchProtocolInstance()

    when:
    def status = mapper.map(operation)

    then:
    status.state == expectedStatus
    status.lastChangeDate == protocolInstance.statusDate
    status.lastChangeUser == "validator"

    where:
    expectedStatus                                        | protocolStatus
    VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS | ProtocolInstanceStatus.CREDENTIALS_GENERATED
    VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS | ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZING
    VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS | ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZED
    VotingPeriodStatusVo.State.INITIALIZATION_FAILED      | ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE
    VotingPeriodStatusVo.State.INITIALIZED                | ProtocolInstanceStatus.READY_TO_RECEIVE_VOTES
    VotingPeriodStatusVo.State.CLOSED                     | ProtocolInstanceStatus.SHUFFLING
    VotingPeriodStatusVo.State.CLOSED                     | ProtocolInstanceStatus.DECRYPTING
    VotingPeriodStatusVo.State.CLOSED                     | ProtocolInstanceStatus.RESULTS_AVAILABLE
    VotingPeriodStatusVo.State.CLOSED                     | ProtocolInstanceStatus.NOT_ENOUGH_VOTES_TO_INITIATE_TALLY
    VotingPeriodStatusVo.State.CLOSED                     | ProtocolInstanceStatus.TALLY_ARCHIVE_GENERATION_FAILURE
  }

  def "Status should be in INITIALIZATION_FAILED if the request for INITIALIZATION_IN_PROGRESS hasn't been updated for a long period of time"() {
    given:
    def operation = prepareDeployedOperation(ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZING)
    def protocolInstance = operation.fetchDeployedConfiguration().fetchProtocolInstance()
    protocolInstance.statusDate = LocalDateTime.now().minusDays(1)

    when:
    def status = mapper.map(operation)

    then:
    status.state == VotingPeriodStatusVo.State.INITIALIZATION_FAILED
    status.lastChangeDate == protocolInstance.statusDate
    status.lastChangeUser == "validator"
  }

  def "All ProtocolInstanceStatus values should be mapped to a value"(ProtocolInstanceStatus status) {
    given:
    def operation = prepareDeployedOperation(status)

    when:
    mapper.map(operation)

    then:
    noExceptionThrown()

    where:
    status << ProtocolInstanceStatus.values().toList()
  }

  def prepareDeployedOperation(ProtocolInstanceStatus status) {
    def operation = new Operation(clientId: "clientId")

    def votingPeriodInitialization = new VotingPeriodInitialization(
            status: PrivilegedAction.Status.APPROVED,
            requester: new User(username:  "requester"),
            validator: new User(username:  "validator"),
            statusDate: STATUS_DATE
    )
    def votingPeriodConfiguration = new VotingPeriodConfiguration(
            author: "configAuthor", submissionDate: SUBMISSION_DATE, action: votingPeriodInitialization)

    def deployedConfiguration = new OperationConfiguration(
            protocolInstance: new ProtocolInstance(status: status, statusDate: LocalDateTime.now())
    )
    operation.setDeployedConfiguration(deployedConfiguration)
    operation.setVotingPeriodConfiguration(votingPeriodConfiguration)

    return operation
  }
}
