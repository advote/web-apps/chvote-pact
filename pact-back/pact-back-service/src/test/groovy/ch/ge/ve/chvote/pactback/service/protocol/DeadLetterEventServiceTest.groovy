/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.repository.protocol.DeadLetterEventRepository
import ch.ge.ve.chvote.pactback.repository.protocol.entity.DeadLetterEvent
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.protocol.vo.DeadLetterEventVo
import ch.ge.ve.event.Event
import ch.ge.ve.service.EventBus
import com.fasterxml.jackson.databind.ObjectMapper
import java.time.LocalDateTime
import spock.lang.Specification

class DeadLetterEventServiceTest extends Specification {

  def repository = Mock(DeadLetterEventRepository)
  def jackson = Mock(ObjectMapper)
  def eventBus = Mock(EventBus)
  def service = new DeadLetterEventService(repository, jackson, eventBus)

  def "findAll should return all available dead letter events"() {
    given:
    repository.findAll() >> [
            createDeadLetterEvent(1, "1", "channel", "Event", "Hi"),
            createDeadLetterEvent(2, "1", "channel", "Event", "Lo")
    ]

    when:
    def events = service.findAll()

    then:
    events.size() == 2
    events*.id.sort() == [1L, 2L]
    events*.content.sort() == ["Hi", "Lo"]
  }

  def "findById should call the repository to retrieve the requested event"() {
    given:
    repository.findById(1L) >> Optional.of(createDeadLetterEvent(1, "1", "channel", "Event", "Hi"))

    when:
    def event = service.findById(1L)

    then:
    event.id == 1L
  }

  def "findById should throw an EntityNotFoundException exception when the requested event doesn't exist"() {
    given:
    repository.findById(1L) >> Optional.empty()

    when:
    service.findById(1L)

    then:
    thrown(EntityNotFoundException)
  }

  def "save should persist the event only if it is not already stored"() {
    given:
    def event = createDeadLetterEvent("1", "channel", "Event", "Hi")

    when:
    service.save(DeadLetterEventVo.fromEntity(event))

    then:
    1 * repository.save(event) >> event
  }

  def "save should not persist the event if it is already stored"() {
    given:
    def event = createDeadLetterEvent("1", "channel", "Event", "Hi")

    when:
    service.save(DeadLetterEventVo.fromEntity(event))

    then:
    1 * repository.existsByProtocolIdAndTypeAndCreationDateAndContent(
            event.protocolId, event.type, event.creationDate, event.content) >> true
    0 * repository.save(event)
  }

  def "republish should delete the event from the repository after having republished it"() {
    given:
    repository.findById(1L) >> Optional.of(createDeadLetterEvent(1, "1", "channel", "ch.ge.ve.event.Event", "Hi"))
    jackson.readValue("Hi", Event) >> new Event(){}

    when:
    service.republish(1L)

    then:
    1 * eventBus.publish("channel", "1", null, _ as Event, null)
    1 * repository.deleteById(1L)
  }

  private static DeadLetterEvent createDeadLetterEvent(String protocolId, String channel, String type, String content) {
    return createDeadLetterEvent(null, protocolId, channel, type, content);
  }

  private static DeadLetterEvent createDeadLetterEvent(Long id, String protocolId,
                                                       String channel, String type, String content) {
    DeadLetterEvent event = id == null ? new DeadLetterEvent() : new DeadLetterEvent(id)
    event.setProtocolId(protocolId)
    event.setOriginalChannel(channel)
    event.setType(type)
    event.setContent(content)
    event.setCreationDate(LocalDateTime.now())
    event.setPickupDate(LocalDateTime.now())
    return event;
  }
}
