/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.operation.period

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.PublicKeyFile
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.operation.TimeService
import java.time.LocalDateTime
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

class VotingPeriodAttachmentServiceTest extends Specification {

  public static final String TIMEZONE = "Europe/Zurich"
  private VotingPeriodAttachmentService service

  private TimeService timeService
  private LocalDateTime dateNow

  void setup() {
    dateNow = LocalDateTime.now()

    timeService = Mock(TimeService)
    timeService.now() >> dateNow

    def chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    chVoteConfigurationProperties.timezone >> TIMEZONE

    service = new VotingPeriodAttachmentService(timeService, chVoteConfigurationProperties)
  }

  def "should parse the file and populate the VotingPeriodConfiguration entity"() {
    given:
    AttachmentFileEntryVo attachmentEntryVo = new AttachmentFileEntryVo()
    attachmentEntryVo.setImportDateTime(dateNow)

    def is = this.class.getClassLoader().getResourceAsStream("attachments/publicKey.txt")
    MultipartFile file = new MockMultipartFile("publicKey", "publicKey.txt", null, is)

    when:
    PublicKeyFile publicKeyFile = service.createPublicKeyFile(file, attachmentEntryVo)

    then:
    publicKeyFile.name == "publicKey"
    publicKeyFile.type == AttachmentType.ELECTION_OFFICER_KEY
    publicKeyFile.importDate == dateNow
  }


}
