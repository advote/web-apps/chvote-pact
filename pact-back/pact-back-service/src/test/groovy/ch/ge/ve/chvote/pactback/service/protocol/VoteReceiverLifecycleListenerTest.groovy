/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZED
import static ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus.ELECTION_OFFICER_KEY_INITIALIZING

import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolStatusChangeEvent
import ch.ge.ve.chvote.pactback.service.receiver.VoteReceiverService
import spock.lang.Specification

class VoteReceiverLifecycleListenerTest extends Specification {

  private VoteReceiverService service = Mock(VoteReceiverService)

  private VoteReceiverLifecycleListener listener = new VoteReceiverLifecycleListener(service)

  private String protocolId = "sample_protocol_id_1234"

  def "#notifyProtocolDeployed should trigger the service if the status transitions to 'ELECTION_OFFICER_KEY_INITIALIZED'"() {
    given:
    def instance = new ProtocolInstance(protocolId: protocolId)
    def event = new ProtocolStatusChangeEvent(instance, ELECTION_OFFICER_KEY_INITIALIZING, ELECTION_OFFICER_KEY_INITIALIZED)

    when:
    listener.notifyProtocolDeployed(event)

    then:
    1 * service.deployInstance(protocolId)
  }

  def "#notifyProtocolDeployed should not trigger the service if the status transitions to any value other than 'ELECTION_OFFICER_KEY_INITIALIZED'"(
          ProtocolInstanceStatus status) {
    given:
    def instance = new ProtocolInstance(protocolId: protocolId)
    def event = new ProtocolStatusChangeEvent(instance, ELECTION_OFFICER_KEY_INITIALIZING, status)

    when:
    listener.notifyProtocolDeployed(event)

    then:
    0 * service._

    where:
    status << ProtocolInstanceStatus.values().findAll { it != ELECTION_OFFICER_KEY_INITIALIZED }
  }

  def "#notifyProtocolDeleted should trigger the service"() {
    given:
    def instance = new ProtocolInstance(protocolId: protocolId)
    def event = new ProtocolDeletionEvent(instance)

    when:
    listener.notifyProtocolDeleted(event)

    then:
    1 * service.destroyInstance(protocolId)
  }
}
