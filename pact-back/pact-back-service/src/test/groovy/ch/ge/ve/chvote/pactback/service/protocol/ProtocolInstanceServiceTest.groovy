/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.service.protocol

import ch.ge.ve.chvote.pactback.repository.exception.OperationStateException
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterRepository
import ch.ge.ve.chvote.pactback.repository.protocol.DeadLetterEventRepository
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolEnvironment
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceRepository
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance
import ch.ge.ve.chvote.pactback.repository.protocol.entity.VotersCreationStats
import ch.ge.ve.chvote.pactback.service.config.ChVoteConfigurationProperties
import ch.ge.ve.chvote.pactback.service.exception.EntityNotFoundException
import ch.ge.ve.chvote.pactback.service.exception.InvalidDeploymentException
import ch.ge.ve.chvote.pactback.service.exception.ProtocolInstanceAlreadyExistsException
import ch.ge.ve.chvote.pactback.service.protocol.events.ProtocolDeletionEvent
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.function.Function
import org.springframework.context.ApplicationEventPublisher
import spock.lang.Specification

class ProtocolInstanceServiceTest extends Specification {

  private ProtocolInstanceService service
  private ProtocolInstanceRepository instanceRepository
  private DeadLetterEventRepository deadLetterEventRepository
  private VoterRepository voterRepository
  private ProtocolCleanupService protocolCleanupService
  private ApplicationEventPublisher applicationEventPublisher
  private ObjectMapper objectMapper
  private ChVoteConfigurationProperties chVoteConfigurationProperties

  void setup() {
    instanceRepository = Mock(ProtocolInstanceRepository)
    deadLetterEventRepository = Mock(DeadLetterEventRepository)
    voterRepository = Mock(VoterRepository)
    protocolCleanupService = Mock(ProtocolCleanupService)
    applicationEventPublisher = Mock(ApplicationEventPublisher)
    objectMapper = Mock()
    chVoteConfigurationProperties = Mock(ChVoteConfigurationProperties)
    def core = Mock(ChVoteConfigurationProperties.ProtocolCore)
    chVoteConfigurationProperties.getProtocolCore() >> core
    core.controlComponentsCount >> 2

    service = new ProtocolInstanceService(instanceRepository, deadLetterEventRepository, protocolCleanupService,
             voterRepository, applicationEventPublisher, objectMapper, "Test")
  }

  def "deleteProtocolInstance should delete the given protocol instance (DB protocol + kernel protocol) and raise an event to delete the protocols's files"() {
    given:
    def instance = new ProtocolInstance(id: 2, protocolId: 'P-01')

    when:
    service.deleteProtocolInstance(instance)

    then: "the protocol should have been called for cleanup"
    1 * protocolCleanupService.cleanUpProtocol(instance)

    and: "all associated entities should have been deleted"
    1 * deadLetterEventRepository.deleteByProtocolId("P-01")
    1 * voterRepository.deleteByProtocolInstance_Id(instance.id)
    1 * instanceRepository.delete(instance)

    and: "a ProtocolDeletionEvent should have been published"
    1 * applicationEventPublisher.publishEvent(*_) >> { arguments ->
      def event = arguments[0] as ProtocolDeletionEvent
      assert event.protocolInstance.id == 2
    }
  }

  def "createProtocolInstance should create a new instance given that no other instance exists"() {
    given:
    def operation = buildOperationWithOperationConfiguration(ValidationStatus.VALIDATED)

    when:
    service.createProtocolInstance(operation, { -> operation.fetchDeployedConfiguration() }, ProtocolEnvironment.TEST)

    then:
    1 * instanceRepository.existsByEnvironmentAndOperation_ClientId(*_) >> false
    1 * instanceRepository.save(*_) >> { arguments ->
      assert (arguments[0] as ProtocolInstance).operation.id == 1
      assert (arguments[0] as ProtocolInstance).configuration.id == 2
      assert (arguments[0] as ProtocolInstance).protocolId != null
      assert (arguments[0] as ProtocolInstance).environment == ProtocolEnvironment.TEST
      assert (arguments[0] as ProtocolInstance).status == ProtocolInstanceStatus.INITIALIZING
    }
  }

  def "createProtocolInstance should throw an exception if an instance already exists for the given configuration"() {
    given:
    def configuration = buildOperationConfiguration(ValidationStatus.VALIDATED);
    def operation = buildOperation(configuration)

    def instance = new ProtocolInstance()
    configuration.protocolInstance = instance
    instance.operation = operation
    instance.configuration = configuration
    instance.status = ProtocolInstanceStatus.CREDENTIALS_GENERATED
    instance.environment = ProtocolEnvironment.TEST

    when:
    service.createProtocolInstance(operation, { -> operation.fetchDeployedConfiguration() }, ProtocolEnvironment.TEST)

    then:
    0 * instanceRepository.existsByEnvironmentAndOperation_ClientId(*_)
    0 * instanceRepository.save(*_)
    thrown(ProtocolInstanceAlreadyExistsException)
  }

  def "createProtocolInstance should throw an exception if the operation has no deployed configuration"() {
    given:
    def operation = new Operation()
    operation.clientId = "some operation"

    when:
    service.createProtocolInstance(operation, { -> operation.fetchDeployedConfiguration() }, ProtocolEnvironment.TEST)

    then:
    OperationStateException ex = thrown()
    ex.message == "Operation [some operation] has no deployed configuration"
  }

  def "createProtocolInstance should throw an exception if an instance already exists for the given operation and environment"() {
    given:
    def configuration = buildOperationConfiguration(ValidationStatus.VALIDATED)
    def operation = buildOperation(configuration)

    when:
    service.createProtocolInstance(operation, { -> operation.fetchDeployedConfiguration() }, ProtocolEnvironment.TEST)

    then:
    1 * instanceRepository.existsByEnvironmentAndOperation_ClientId(*_) >> true
    0 * instanceRepository.save(*_)
    thrown(ProtocolInstanceAlreadyExistsException)
  }

  def "createProtocolInstance should throw an exception if the configuration is deprecated"() {
    given:
    def operation = buildOperationWithOperationConfiguration(ValidationStatus.INVALIDATED)

    when:
    service.createProtocolInstance(operation, { -> operation.fetchDeployedConfiguration() }, ProtocolEnvironment.TEST)

    then:
    0 * instanceRepository.existsByEnvironmentAndOperation_ClientId(*_)
    0 * instanceRepository.save(*_)
    thrown(InvalidDeploymentException)
  }

  def buildOperationConfiguration(ValidationStatus status) {
    def configuration = new OperationConfiguration()
    configuration.id = 2
    configuration.validationStatus = status
    return configuration
  }

  def buildOperation(OperationConfiguration configuration) {
    def operation = new Operation()
    operation.id = 1
    operation.clientId = '2'
    operation.deployedConfiguration = configuration
    return operation
  }

  def buildOperationWithOperationConfiguration(ValidationStatus status) {
    return buildOperation(buildOperationConfiguration(status))
  }

  def "saveCreationStatistics should associate the given statistics to the entity"() {
    given:
    instanceRepository.getOne(_) >> new ProtocolInstance()
    instanceRepository.save(_) >> { return it[0] }
    def newStats = [new VotersCreationStats(id: 1L), new VotersCreationStats(id: 2L)]

    when:
    def instance = service.saveCreationStatistics(1L, newStats)

    then:
    instance.statistics == newStats
  }

  def "saveCreationStatistics should replace existing statistics"() {
    given:
    instanceRepository.getOne(_) >> new ProtocolInstance().with {
      it.statistics = [new VotersCreationStats(id: 1L), new VotersCreationStats(id: 2L)]; return it
    }
    instanceRepository.save(_) >> { return it[0] }
    def newStats = [new VotersCreationStats(id: 303L), new VotersCreationStats(id: 505L)]

    when:
    def instance = service.saveCreationStatistics(1L, newStats)

    then:
    instance.statistics*.id == [303L, 505L]
  }

  def "withProtocolInstance should apply function to fresh entity"() {
    given:
    def protocolInstanceId = 111L
    def freshEntity = new ProtocolInstance()
    def function = Mock(Function)
    def functionResult = new Object()

    when:
    def result = service.withProtocolInstance(protocolInstanceId, function)

    then:
    1 * instanceRepository.getOne(protocolInstanceId) >> freshEntity
    1 * function.apply(freshEntity) >> functionResult
    result.is functionResult
  }
}
