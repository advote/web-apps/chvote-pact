/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment
import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction
import javax.annotation.Resource

class PrivilegedActionsRepositoryTest extends PactRepositoryTest {

  @Resource
  PrivilegedActionRepository repository

  def "countByRequesterIdNotAndStatus should retrieve the expected count"() {
    given:
    // The data in import.sql

    when:
    def countActionsToApproveByUser1 = repository.countByRequesterIdNotAndStatus(1L, PrivilegedAction.Status.PENDING)

    then:
    countActionsToApproveByUser1 == 2
  }

  def "findByIdAndStatus should retrieve the expected privileged action, including the target"() {
    given:
    // the data in import.sql

    when:
    def actionOpt = repository.findByIdAndStatus(id, status)

    then:
    actionOpt == expectedActionOpt

    where:
    id    | status                           || expectedActionOpt
    3501L | PrivilegedAction.Status.PENDING  || Optional.of(buildPrivilegedAction(3501L))
    4501L | PrivilegedAction.Status.PENDING  || Optional.of(buildPrivilegedAction(4501L))
    1501L | PrivilegedAction.Status.APPROVED || Optional.empty()
  }

  PrivilegedAction buildPrivilegedAction(Long id) {
    def action = new ConfigurationDeployment()
    action.id = id
    return action
  }

}
