/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.action.deploy

import ch.ge.ve.chvote.pactback.repository.PactRepositoryTest
import javax.annotation.Resource

class ConfigurationDeploymentRepositoryTest extends PactRepositoryTest {

  @Resource
  ConfigurationDeploymentRepository repository

  def "existsByOperationSettings_Id should find existence by id"() {
    given:
    // the data in import.sql

    when:
    def exists = repository.existsByOperationConfiguration_Id(id)

    then:
    exists == expectedResult

    where:
    id    || expectedResult
    1201L || false
    3201L || true
    4201L || true
    27L   || false
  }
}
