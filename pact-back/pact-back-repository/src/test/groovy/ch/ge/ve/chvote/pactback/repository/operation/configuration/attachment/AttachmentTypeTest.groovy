/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment

import spock.lang.Specification

class AttachmentTypeTest extends Specification {

  def "should find AttachmentType by its shortLabel"() {
    when:
    def type = AttachmentType.findTypeByShortLabel(shortLabel)

    then:
    type.orElse(null) == expectedType

    where:
    shortLabel             || expectedType
    "REPOSITORY_VOTATION"  || AttachmentType.OPERATION_REPOSITORY_VOTATION
    "REPOSITORY_ELECTION"  || AttachmentType.OPERATION_REPOSITORY_ELECTION
    "DOMAIN_OF_INFLUENCE"  || AttachmentType.DOMAIN_OF_INFLUENCE
    "DOCUMENT_FAQ"         || AttachmentType.OPERATION_FAQ
    "DOCUMENT_TERMS"       || AttachmentType.OPERATION_TERMS_OF_USAGE
    "DOCUMENT_CERTIFICATE" || AttachmentType.OPERATION_CERTIFICATE_VERIFICATION_FILE
    "ELECTION_OFFICER_KEY" || AttachmentType.ELECTION_OFFICER_KEY
    "HIGHLIGHTED_QUESTION" || AttachmentType.HIGHLIGHTED_QUESTION
    "BALLOT_DOCUMENTATION" || AttachmentType.BALLOT_DOCUMENTATION
    "NOT_A_TYPE"           || null
  }
}
