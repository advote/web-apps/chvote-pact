/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.voter.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VoterCountingCircle {

  @Column(name = "VOTER_N_COUNTING_CIRCLE_INDEX", nullable = false)
  private int protocolCountingCircleIndex;

  @Column(name = "VOTER_V_COUNTING_CIRCLE_BUSINESS_ID", nullable = false)
  private String businessId;

  @Column(name = "VOTER_V_COUNTING_CIRCLE_NAME", nullable = false)
  private String name;

  public VoterCountingCircle() {
    /* Empty constructor required by JPA */
  }

  public VoterCountingCircle(int protocolCountingCircleIndex, String businessId, String name) {
    this.protocolCountingCircleIndex = protocolCountingCircleIndex;
    this.businessId = businessId;
    this.name = name;
  }

  public int getProtocolCountingCircleIndex() {
    return protocolCountingCircleIndex;
  }

  public void setProtocolCountingCircleIndex(int protocolCountingCircleIndex) {
    this.protocolCountingCircleIndex = protocolCountingCircleIndex;
  }

  public String getBusinessId() {
    return businessId;
  }

  public void setBusinessId(String businessId) {
    this.businessId = businessId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
