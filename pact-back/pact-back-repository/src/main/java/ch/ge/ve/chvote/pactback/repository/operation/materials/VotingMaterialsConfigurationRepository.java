/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.materials;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * The {@link VotingMaterialsConfiguration} repository.
 */
public interface VotingMaterialsConfigurationRepository extends JpaRepository<VotingMaterialsConfiguration, Long> {

  /**
   * Find all {@link VotingMaterialsConfiguration} entities that do not have an associated
   * action or that their action is in the given {@link PrivilegedAction.Status}.
   *
   * @param status the status to look for
   *
   * @return A {@link Stream} containing the requested {@link VotingMaterialsConfiguration}
   */
  @Query("select c from VotingMaterialsConfiguration c left outer join c.creationAction as a where a is null or a.status = :status")
  Stream<VotingMaterialsConfiguration> findAllByCreationActionIsNullOrCreationActionStatus(@Param(
      "status") PrivilegedAction.Status status);

  /**
   * Find all {@link VotingMaterialsConfiguration} entities that their invalidation action is in the given
   * {@link PrivilegedAction.Status}.
   *
   * @param status the status to look for
   *
   * @return A {@link Stream} containing the requested {@link VotingMaterialsConfiguration}
   */
  @Query(
      "select c from VotingMaterialsConfiguration c left outer join c.creationAction as a left outer join c" +
      ".invalidationAction as b where a.status = 'APPROVED' and (b is null or b.status = :status)")
  Stream<VotingMaterialsConfiguration> findAllByInvalidationActionIsNullOrInvalidationActionStatus(@Param(
      "status") PrivilegedAction.Status status);
}