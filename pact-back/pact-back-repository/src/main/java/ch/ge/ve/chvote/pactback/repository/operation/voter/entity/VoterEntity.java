/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.voter.entity;

import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import java.util.List;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PACT_T_VOTER")
public class VoterEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "VOTER_N_ID")
  private Long id;

  @Column(name = "VOTER_N_PROTOCOL_VOTER_INDEX", nullable = false)
  private int protocolVoterIndex;

  @Column(name = "VOTER_V_LOCAL_PERSON_ID", nullable = false)
  private String localPersonId;

  @Embedded
  private DateOfBirth dateOfBirth;

  @Embedded
  private VoterCountingCircle voterCountingCircle;

  @Column(name = "VOTER_V_PRINTING_AUTHORITY", nullable = false)
  private String printingAuthorityName;

  @ElementCollection
  @CollectionTable(name = "PACT_T_VOTER_DOIS", joinColumns = @JoinColumn(name = "VOTER_ID"))
  @Column(name = "DOI_ID", nullable = false)
  private List<String> allowedDoiIds;

  @ManyToOne
  @JoinColumn(name = "VOTER_N_PROTOCOL_INSTANCE_ID")
  private ProtocolInstance protocolInstance;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public int getProtocolVoterIndex() {
    return protocolVoterIndex;
  }

  public void setProtocolVoterIndex(int protocolVoterIndex) {
    this.protocolVoterIndex = protocolVoterIndex;
  }

  public String getLocalPersonId() {
    return localPersonId;
  }

  public void setLocalPersonId(String localPersonId) {
    this.localPersonId = localPersonId;
  }

  public DateOfBirth getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(DateOfBirth dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public VoterCountingCircle getVoterCountingCircle() {
    return voterCountingCircle;
  }

  public void setVoterCountingCircle(VoterCountingCircle voterCountingCircle) {
    this.voterCountingCircle = voterCountingCircle;
  }

  public String getPrintingAuthorityName() {
    return printingAuthorityName;
  }

  public void setPrintingAuthorityName(String printingAuthorityName) {
    this.printingAuthorityName = printingAuthorityName;
  }

  public List<String> getAllowedDoiIds() {
    return allowedDoiIds;
  }

  public void setAllowedDoiIds(List<String> allowedDoiIds) {
    this.allowedDoiIds = allowedDoiIds;
  }

  public ProtocolInstance getProtocolInstance() {
    return protocolInstance;
  }

  public void setProtocolInstance(ProtocolInstance protocolInstance) {
    this.protocolInstance = protocolInstance;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    return Objects.equals(id, ((VoterEntity) o).id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
