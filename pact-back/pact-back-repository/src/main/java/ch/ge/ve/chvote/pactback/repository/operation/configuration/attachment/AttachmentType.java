/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.Attachment;
import java.util.Arrays;
import java.util.Optional;

/**
 * The possible types of an {@link Attachment}.
 */
public enum AttachmentType {
  /**
   * An attachment that describes an operation repository for a votation (eCH-0159).
   */
  OPERATION_REPOSITORY_VOTATION("REPOSITORY_VOTATION", true),
  /**
   * An attachment that describes an operation repository for an election (eCH-0157).
   */
  OPERATION_REPOSITORY_ELECTION("REPOSITORY_ELECTION", true),
  /**
   * An attachment that describes an area of influence.
   */
  DOMAIN_OF_INFLUENCE("DOMAIN_OF_INFLUENCE", true),
  /**
   * A PDF containing the frequently ask questions documentation.
   */
  OPERATION_FAQ("DOCUMENT_FAQ", false),
  /**
   * An attachment with the translations for the texts appearing in the voting site.
   */
  OPERATION_TRANSLATION_FILE("TRANSLATIONS", false),
  /**
   * A PDF containing the terms and conditions documentation.
   */
  OPERATION_TERMS_OF_USAGE("DOCUMENT_TERMS", false),
  /**
   * Operation certificate file.
   */
  OPERATION_CERTIFICATE_VERIFICATION_FILE("DOCUMENT_CERTIFICATE", false),
  /**
   * Operation voters register (eCH-0045).
   */
  OPERATION_REGISTER("REGISTER", true),
  /**
   * Election officer's public key.
   */
  ELECTION_OFFICER_KEY("ELECTION_OFFICER_KEY", false),
  /**
   * A PDF containing the documentation of a specific ballot.
   */
  BALLOT_DOCUMENTATION("BALLOT_DOCUMENTATION", false),
  /**
   * A PDF containing the documentation of a highlighted question.
   */
  HIGHLIGHTED_QUESTION("HIGHLIGHTED_QUESTION", false);

  private final boolean isOperationReferenceFile;
  private final String  shortLabel;

  AttachmentType(String shortLabel, boolean isOperationReferenceFile) {
    this.shortLabel = shortLabel;
    this.isOperationReferenceFile = isOperationReferenceFile;
  }

  /**
   * Get the short label for this type of attachment.
   *
   * @return the short label for this type of attachment.
   */
  private String getShortLabel() {
    return shortLabel;
  }

  /**
   * Whether this attachment is an operation reference file.
   *
   * @return true if its an operation reference file, false otherwise.
   */
  public boolean isOperationReferenceFile() {
    return isOperationReferenceFile;
  }

  /**
   * Retrieve an attachment type by its short label.
   *
   * @param shortLabel the short label to search for.
   *
   * @return the matching attachment type or null if the label does not match any type.
   *
   * @see #getShortLabel()
   */
  public static Optional<AttachmentType> findTypeByShortLabel(String shortLabel) {
    return Arrays.stream(AttachmentType.values())
                 .filter(type -> type.getShortLabel().equals(shortLabel))
                 .findFirst();
  }
}
