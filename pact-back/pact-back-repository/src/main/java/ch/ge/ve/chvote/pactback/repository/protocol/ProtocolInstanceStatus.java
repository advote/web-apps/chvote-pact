/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol;

/**
 * The protocol instance status.
 */
public enum ProtocolInstanceStatus {
  /**
   * The protocol instance is initializing.
   */
  INITIALIZING,
  /**
   * The public parameters have been initialized.
   */
  PUBLIC_PARAMETERS_INITIALIZED,
  /**
   * The protocol instance is generating keys.
   */
  GENERATING_KEYS,
  /**
   * Keys have been generated.
   */
  KEYS_GENERATED,
  /**
   * The protocol instance is generating credentials.
   */
  GENERATING_CREDENTIALS,
  /**
   * Credentials have been generated.
   */
  CREDENTIALS_GENERATED,
  /**
   * Credentials failed to be generated.
   */
  CREDENTIALS_GENERATION_FAILURE,
  /**
   * The protocol instance is initializing the election officer key.
   */
  ELECTION_OFFICER_KEY_INITIALIZING,
  /**
   * The election officer key initialization has failed.
   */
  ELECTION_OFFICER_KEY_INITIALIZATION_FAILURE,
  /**
   * The election office key has been initialized.
   */
  ELECTION_OFFICER_KEY_INITIALIZED,
  /**
   * The protocol instance is ready to receive votes.
   */
  READY_TO_RECEIVE_VOTES,
  /**
   * The protocol instance is shuffling the result.
   */
  SHUFFLING,
  /**
   * The protocol instance is decrypting the results.
   */
  DECRYPTING,
  /**
   * The protocol instance is collecting the results and generates the archive.
   */
  GENERATING_RESULTS,
  /**
   * The protocol instance tally generation has failed.
   */
  TALLY_ARCHIVE_GENERATION_FAILURE,
  /**
   * There are not enough confirmed votes to initiate the tally process.
   */
  NOT_ENOUGH_VOTES_TO_INITIATE_TALLY,
  /**
   * The results are available
   */
  RESULTS_AVAILABLE
}
