/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.progress.entity;

import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * An entity that represents progress of a single task of a protocol instance.
 */
@Entity
@Table(name = "PACT_T_PROTOCOL_STAGE_PROGRESS")
public class ProtocolStageProgress {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "PSP_N_ID")
  private Long id;

  @Column(name = "PSP_N_CC_INDEX")
  private Integer ccIndex;

  @Column(name = "PSP_V_STAGE")
  @Enumerated(EnumType.STRING)
  private ProtocolStage stage;

  @Column(name = "PSP_N_CC_DONE")
  private Long done;

  @Column(name = "PSP_N_CC_TOTAL")
  private Long total;

  @Column(name = "PSP_D_START_DATE")
  private LocalDateTime startDate;

  @Column(name = "PSP_D_LAST_MODIFICATION_DATE")
  private LocalDateTime lastModificationDate;

  public Long getId() {
    return id;
  }

  public Integer getCcIndex() {
    return ccIndex;
  }

  public void setCcIndex(Integer ccIndex) {
    this.ccIndex = ccIndex;
  }

  public ProtocolStage getStage() {
    return stage;
  }

  public void setStage(ProtocolStage stage) {
    this.stage = stage;
  }

  public Long getDone() {
    return done;
  }

  public void setDone(Long done) {
    this.done = done;
  }

  public Long getTotal() {
    return total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public LocalDateTime getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDateTime startDate) {
    this.startDate = startDate;
  }

  public LocalDateTime getLastModificationDate() {
    return lastModificationDate;
  }

  public void setLastModificationDate(LocalDateTime lastModificationDate) {
    this.lastModificationDate = lastModificationDate;
  }
}
