/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.chvote.pactback.repository.action.operation.voting.period;

import ch.ge.ve.chvote.pactback.repository.action.BasePrivilegedActionRepository;
import ch.ge.ve.chvote.pactback.repository.action.operation.voting.period.entity.VotingPeriodInitialization;

/**
 * A specialised {@link BasePrivilegedActionRepository} for interacting with {@link VotingPeriodInitialization} entities.
 */
public interface VotingPeriodInitializationActionRepository extends BasePrivilegedActionRepository<VotingPeriodInitialization> {

  /**
   * Whether a {@link VotingPeriodInitialization} exists for the given voting period configuration ID.
   *
   * @param votingPeriodConfigurationId the voting period configuration ID
   *
   * @return true if it exists, false otherwise
   */
  boolean existsByVotingPeriodConfiguration_Id(Long votingPeriodConfigurationId);
}
