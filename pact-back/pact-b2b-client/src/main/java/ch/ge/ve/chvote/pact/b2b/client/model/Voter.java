/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pact.b2b.client.model;

import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Object to identify a voter
 */
public class Voter {

  @NotNull
  @Size(min = 1)
  @Valid
  private final List<DomainOfInfluence> domainOfInfluenceIds;

  @NotNull
  @Size(min = 1)
  private final String localPersonId;

  @NotNull
  private final Integer voterIndex;

  @NotNull
  private final Integer voterBirthYear;

  @Min(1)
  @Max(12)
  private final Integer voterBirthMonth;

  @Min(1)
  @Max(31)
  private final Integer voterBirthDay;

  @NotNull
  private final String identificationCodeHash;

  @NotNull
  private final CountingCircle countingCircle;


  @JsonCreator
  public Voter(@JsonProperty("domainOfInfluenceIds") List<DomainOfInfluence> domainOfInfluenceIds,
               @JsonProperty("localPersonId") String localPersonId,
               @JsonProperty("voterIndex") Integer voterIndex,
               @JsonProperty("voterBirthYear") Integer voterBirthYear,
               @JsonProperty("voterBirthMonth") Integer voterBirthMonth,
               @JsonProperty("voterBirthDay") Integer voterBirthDay,
               @JsonProperty("identificationCodeHash") String identificationCodeHash,
               @JsonProperty("countingCircle") CountingCircle countingCircle) {
    this.domainOfInfluenceIds = domainOfInfluenceIds;
    this.localPersonId = localPersonId;
    this.voterIndex = voterIndex;
    this.voterBirthYear = voterBirthYear;
    this.voterBirthMonth = voterBirthMonth;
    this.voterBirthDay = voterBirthDay;
    this.identificationCodeHash = identificationCodeHash;
    this.countingCircle = countingCircle;
  }

  public List<DomainOfInfluence> getDomainOfInfluenceIds() {
    return domainOfInfluenceIds;
  }

  public String getLocalPersonId() {
    return localPersonId;
  }

  public Integer getVoterIndex() {
    return voterIndex;
  }

  public Integer getVoterBirthYear() {
    return voterBirthYear;
  }

  public Integer getVoterBirthMonth() {
    return voterBirthMonth;
  }

  public Integer getVoterBirthDay() {
    return voterBirthDay;
  }

  public String getIdentificationCodeHash() {
    return identificationCodeHash;
  }

  public CountingCircle getCountingCircle() {
    return countingCircle;
  }
}
