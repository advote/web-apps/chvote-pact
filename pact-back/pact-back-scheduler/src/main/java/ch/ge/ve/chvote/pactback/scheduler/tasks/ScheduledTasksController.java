/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tasks;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST endpoints to manage the scheduled tasks for deployed operations.
 */
@RestController
@RequestMapping(value = "tasks/operations", produces = MediaType.APPLICATION_JSON_VALUE)
public class ScheduledTasksController {

  private static final Logger logger = LoggerFactory.getLogger(ScheduledTasksController.class);

  private final ScheduledTasksService service;

  @Autowired
  public ScheduledTasksController(ScheduledTasksService service) {
    this.service = service;
  }

  /**
   * @return a list of all configured tasks
   */
  @ApiOperation(value = "List tasks", notes = "Lists all existing scheduled tasks, with the corresponding triggered " +
                                              "time if applicable, for monitoring purposes.")
  @GetMapping
  public List<ScheduledTask> listTasks() {
    return service.findAll();
  }

  /**
   * Creates a task to trigger the closure of an operation.
   * <p>
   *   A task will be scheduled at the voting period's end time to demand the PACT to generate the tally archive
   *   for this operation.
   * </p>
   * <p>
   *   This endpoint is idempotent : it will respond with "201 - CREATED" when the task is first created, then
   *   with "200 - OK" if called with the same parameters. <br>
   *   An error status of "409 - CONFLICT" will be sent back if the date parameter differs from the already
   *   configured one.
   * </p>
   *
   * @param clientId operation identifier in the client (caller) referential
   * @param votingPeriodEndTime datetime when the voting period is considered closed
   */
  @ApiOperation(value = "Registers an operation",
                notes = "Registers a task to the scheduler in order to trigger the " +
                        "tally archive generation for an operation on a specific date.")
  @ApiResponses(
      {
          @ApiResponse(code = 200, message = "OK - scheduled task exists (idempotent call)"),
          @ApiResponse(code = 201, message = "Created - scheduled task successfully registered"),
          @ApiResponse(code = 409,
                       message = "Conflict - an operation has already been registered with a different end date")
      })
  @PutMapping("{clientId}")
  @ResponseStatus(HttpStatus.CREATED)
  public void registerOperation(
      @PathVariable("clientId") @ApiParam("operation identifier in the client's referential") String clientId,
      @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
      @ApiParam(value = "datetime when the voting period ends", example = "2018-10-31T18:30:00")
          LocalDateTime votingPeriodEndTime) {
    final ScheduledTask task = service.createFor(clientId, votingPeriodEndTime);
    logger.info("New task #{} scheduled for operation \"{}\" on {}", task.getId(), clientId, votingPeriodEndTime);
  }

  /**
   * Removes a task configured for an operation.
   * <p>
   *   Can be used either to delete obsolete configuration (already triggered operation) or to cancel
   *   a scheduled task (not-yet-triggered operation).
   * </p>
   *
   * @param clientId operation identifier in the client (caller) referential
   */
  @ApiOperation(value = "Unregisters an operation", notes = "Unregisters a scheduled task for an operation")
  @ApiResponses(
      {
          @ApiResponse(code = 200, message = "OK - scheduled task successfully deleted"),
          @ApiResponse(code = 404, message = "Not found - no scheduled task exists for this operation")
      })
  @DeleteMapping("{clientId}")
  public void unregisterOperation(
      @PathVariable("clientId") @ApiParam("operation identifier in the client's referential") String clientId) {
    final long taskId = service.removeFor(clientId);
    logger.info("Task #{} removed for operation \"{}\"", taskId, clientId);
  }

  /**
   * Handles exception when the queried entity has not been found - associated to HTTP status "404 - NOT FOUND".
   * @param exception the intercepted exception
   */
  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public void notFound(Exception exception) {
    logger.warn("Try to access a non-existing entity", exception);
  }

  /**
   * Handles exception when a new request is being submitted for an already configured operation.
   * <p>
   *   Choose between status OK or CONFLICT depending on the submitted dates - cf. register endpoint.
   * </p>
   *
   * @param exc the intercepted exception
   * @param response the HTTP response - to set the appropriate status
   *
   * @see #registerOperation(String, LocalDateTime)
   */
  @ExceptionHandler(OperationAlreadyConfiguredException.class)
  public void handleOperationExists(OperationAlreadyConfiguredException exc, HttpServletResponse response) {
    if (exc.getSubmittedEndTime().isEqual(exc.getConfiguredEndTime())) {
      logger.info("Operation \"{}\" is already configured - ignoring request", exc.getOperationId());
      response.setStatus(HttpStatus.OK.value());
    } else {
      logger.warn("Operation \"{}\" is already configured with date = {} - cannot change the date to {}",
                  exc.getOperationId(), exc.getConfiguredEndTime(), exc.getSubmittedEndTime());
      response.setStatus(HttpStatus.CONFLICT.value());
    }
  }

}
