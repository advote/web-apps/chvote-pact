/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.scheduler.tally

import ch.ge.ve.chvote.pactback.scheduler.tasks.ScheduledTask
import ch.ge.ve.chvote.pactback.scheduler.tasks.ScheduledTasksRepository
import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.transaction.support.SimpleTransactionStatus
import org.springframework.transaction.support.TransactionCallback
import org.springframework.transaction.support.TransactionTemplate
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import spock.lang.Specification

class ScheduledGenerationCommandTest extends Specification {

  // -- We disable the logger for the tests because it is quite verbose - many error cases are tested
  private static final Logger logbackLogger = LoggerFactory.getLogger(ScheduledGenerationCommand)
  private static final Level originalLevel = logbackLogger.level

  void setupSpec() {
    logbackLogger.setLevel(Level.OFF)
  }

  void cleanupSpec() {
    logbackLogger.setLevel(originalLevel)
  }
  // --

  private ScheduledTasksRepository repository = Mock(ScheduledTasksRepository)

  private TallyArchiveGenerationRestClient generationClient = Mock(TallyArchiveGenerationRestClient)

  private TransactionTemplate transactionTemplate = Mock(TransactionTemplate) {
    execute(_) >> { TransactionCallback tc -> tc.doInTransaction(new SimpleTransactionStatus()) }
  }

  private ScheduledGenerationCommand command = new ScheduledGenerationCommand(repository, transactionTemplate, generationClient)


  def "should trigger the api and persist the resulting status"() {
    given:
    def one = new ScheduledTask(operationId: "one")
    def two = new ScheduledTask(operationId: "two")
    repository.findAvailableTasksAt(_) >> [one, two]

    generationClient.demandArchiveGeneration("one") >> HttpStatus.OK
    generationClient.demandArchiveGeneration("two") >> HttpStatus.CREATED

    when:
    command.triggerTallyArchivesGeneration()

    then:
    one.triggeredTime != null
    one.httpStatusReceived == 200

    two.triggeredTime != null
    two.httpStatusReceived == 201
  }

  def "should also persist the demand in case of client-side error"(HttpStatus httpStatus) {
    given:
    def task = new ScheduledTask(operationId: "sample")
    repository.findAvailableTasksAt(_) >> [task]

    generationClient.demandArchiveGeneration(task.operationId) >> {
      throw new HttpClientErrorException(httpStatus, "Exception for tests")
    }

    when:
    command.triggerTallyArchivesGeneration()

    then:
    task.triggeredTime != null
    task.httpStatusReceived == httpStatus.value()

    where:
    httpStatus << HttpStatus.values().findAll { it.is4xxClientError() }
  }

  def "should not persist the demand in case of server-side error"(HttpStatus httpStatus) {
    given:
    def task = new ScheduledTask(operationId: "sample")
    repository.findAvailableTasksAt(_) >> [task]

    generationClient.demandArchiveGeneration(task.operationId) >> {
      throw new HttpServerErrorException(httpStatus, "Exception for tests")
    }

    when:
    command.triggerTallyArchivesGeneration()

    then:
    [task.triggeredTime, task.httpStatusReceived].every { it == null }

    where:
    httpStatus << HttpStatus.values().findAll { it.is5xxServerError() }
  }

  def "should not persist the demand in case of any other exception, but should try the next task"() {
    given:
    def one = new ScheduledTask(operationId: "one")
    def two = new ScheduledTask(operationId: "two")
    repository.findAvailableTasksAt(_) >> [one, two]

    when:
    command.triggerTallyArchivesGeneration()

    then:
    2 * generationClient.demandArchiveGeneration(_) >> {
      throw new RuntimeException("Exception thrown for test purposes")
    }

    and:
    [one.triggeredTime, one.httpStatusReceived, two.triggeredTime, two.httpStatusReceived].every { it == null }
  }
}
