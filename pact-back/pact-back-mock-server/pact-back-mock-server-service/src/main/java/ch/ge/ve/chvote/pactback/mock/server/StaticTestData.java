/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import ch.ge.ve.interfaces.ech.eCH0007.v6.CantonAbbreviationType;
import java.time.LocalDateTime;

public class StaticTestData {
  public static final LocalDateTime CREATION_DATE                            = LocalDateTime.of(2040, 10, 10, 0, 0);
  public static final LocalDateTime IN_TEST_CONFIGURATION_LAST_CHANGE_DATE   = CREATION_DATE.plusDays(1);
  public static final LocalDateTime DEPLOYMENT_REQUEST_DATE                  =
      IN_TEST_CONFIGURATION_LAST_CHANGE_DATE.plusDays(1);
  public static final LocalDateTime DEPLOYMENT_VALIDATION_DATE               = DEPLOYMENT_REQUEST_DATE.plusDays(1);
  public static final LocalDateTime DEPLOYED_CONFIGURATION_LAST_CHANGE_DATE  = DEPLOYMENT_VALIDATION_DATE.plusDays(1);
  public static final LocalDateTime VOTING_MATERIAL_SENT_DATE                =
      DEPLOYED_CONFIGURATION_LAST_CHANGE_DATE.plusDays(1);
  public static final LocalDateTime VOTING_MATERIAL_CREATION_REQUEST_DATE    = VOTING_MATERIAL_SENT_DATE.plusDays(1);
  public static final LocalDateTime VOTING_MATERIAL_CREATION_VALIDATION_DATE =
      VOTING_MATERIAL_CREATION_REQUEST_DATE.plusDays(1);
  public static final LocalDateTime VOTING_MATERIAL_VALIDATION_DATE          =
      VOTING_MATERIAL_CREATION_VALIDATION_DATE.plusDays(1);

  public static final LocalDateTime OPERATION_DATE           = CREATION_DATE.plusYears(1);
  public static final LocalDateTime SITE_CLOSING_DATE        = OPERATION_DATE.minusDays(1);
  public static final LocalDateTime SITE_OPENING_DATE        = OPERATION_DATE.minusDays(10);
public static final LocalDateTime SIMULATION_SITE_CLOSING_DATE = OPERATION_DATE.minusDays(2);
  public static final LocalDateTime SIMULATION_SITE_OPENING_DATE = OPERATION_DATE.minusDays(3);  public static final String        CANTON                   = CantonAbbreviationType.GE.value();
  public static final int           GRACE_PERIOD             = 10;
  public static final String        BO_USER                  = "BoUser";
  public static final String        SIMULATION_NAME          = "SIMULATION_NAME";
  public static final int           NUMBER_OF_VOTERS_ON_CC_1 = 10;
  public static final int           NUMBER_OF_VOTERS_ON_CC_2 = 20;
  public static final String        PRINTER_1                = "printer 1";
  public static final String        PRINTER_2                = "printer 2";
  public static final String        REJECTION_REASON         = "rejected";
  public static final String        VOTING_CARD_LABEL        = "VOTING_CARD_LABEL";
  public static final int           COUNTING_CIRCLE_ID_1     = 1;
  public static final int           COUNTING_CIRCLE_ID_2     = 2;
  public static final String        OPERATION_NAME           = "Operation Name";
  public static final String        OPERATION_LABEL          = "201809VP";
  public static final String        SITE_TEXT_HASH           = "Site text hash";
  public static final String        VIRTUAL_MUNICIPALITY     = "Virtual Municipality";
  public static final String        REAL_MUNICIPALITY        = "Real Municipality";

  public static final String PINSTANCE_NODE_KEY = "TestNode";

  public static final LocalDateTime VOTING_MATERIAL_INVALIDATION_REQUEST_DATE     =
      VOTING_MATERIAL_CREATION_VALIDATION_DATE.plusDays(1);
  public static final LocalDateTime VOTING_MATERIAL_INVALIDATION_APPROVATION_DATE =
      VOTING_MATERIAL_INVALIDATION_REQUEST_DATE.plusDays(1);

  public static final int NB_VOTERS_ON_CC_1                         = 10;
  public static final int NB_VOTERS_ON_CC_2_FOR_PRINTER_1           = 20;
  public static final int NB_VOTERS_ON_CC_2_FOR_PRINTER_2           = 15;
  public static final int NB_VOTERS_FOR_CONTROLLER_TESTING_CARDS    = 5;
  public static final int NB_VOTERS_FOR_PRINTABLE_TESTING_CARDS     = 6;
  public static final int NB_VOTERS_FOR_NOT_PRINTABLE_TESTING_CARDS = 7;
  public static final int NB_VOTERS_FOR_PRINTER_TESTING_CARDS       = 8;
}
