/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import ch.ge.ve.chvote.pactback.repository.operation.DeploymentTarget;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.protocol.ProtocolInstanceStatus;

public class TestCaseBuilderWorkflow {
  interface WithNoConfiguration extends CanCreateDeployConfiguration, OperationHolder {

    WithConfigurationInTest withConfigurationInTest(ValidationStatus validationStatus);
  }

  public interface WithConfigurationInTest extends OperationHolder {
    CanCreateDeployConfiguration withPendingRequestToDeployConfiguration();

    CanCreateDeployConfiguration withRefusedRequestToDeployConfiguration();
  }

  public interface CanCreateDeployConfiguration extends OperationHolder {
    WithDeployedConfiguration withConfigurationDeployed();
  }

  public interface WithDeployedConfiguration extends OperationHolder {
    WithVotingMaterialConfiguration withVotingMaterialConfiguration(DeploymentTarget target);
  }

  public interface WithVotingMaterialConfiguration extends OperationHolder {
    NoMoreChangePossible requestVotingMaterialCreation();

    NoMoreChangePossible rejectVotingMaterialCreation();

    WithAcceptedVotingMaterialCreated acceptVotingMaterialCreation(ProtocolInstanceStatus status);
  }

  public interface NoMoreChangePossible extends OperationHolder {
  }

  public interface WithAcceptedVotingMaterialCreated extends OperationHolder {
    WithVotingMaterialValidated validateVotingMaterial();

    NoMoreChangePossible withPendingInvalidation();

    NoMoreChangePossible withApprovedInvalidation();

    WithVotingPeriodCreated withVotingPeriodCreated();
  }

  public interface OperationHolder {
    Operation getOperation();
  }

  public interface WithVotingMaterialValidated extends OperationHolder {
  }

  public interface WithVotingPeriodCreated extends OperationHolder {
  }
}
