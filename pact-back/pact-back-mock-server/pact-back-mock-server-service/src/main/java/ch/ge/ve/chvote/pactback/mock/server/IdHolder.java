/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.mock.server;

import ch.ge.ve.chvote.pactback.repository.action.entity.PrivilegedAction;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.entity.Operation;
import ch.ge.ve.chvote.pactback.repository.operation.materials.entity.VotingMaterialsConfiguration;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;

/**
 * This class export all ids that has been generated for a given test case
 */
@SuppressWarnings("unused")
public class IdHolder {
  private final Operation operation;

  public String getClientId() {
    return operation.getClientId();
  }

  public Long getOperationId() {
    return operation.getId();
  }

  public Long getDeployedConfigurationId() {
    return operation.getDeployedConfiguration().map(OperationConfiguration::getId).orElse(null);
  }

  public Long getTestConfigurationId() {
    return operation.getConfigurationInTest().map(OperationConfiguration::getId).orElse(null);
  }

  public String getProtocolId() {
    return operation.getDeployedConfiguration()
                    .flatMap(OperationConfiguration::getProtocolInstance)
                    .map(ProtocolInstance::getProtocolId).orElse(null);
  }

  public Long getVotingMaterialConfigurationId() {
    return operation.getVotingMaterialsConfiguration().map(VotingMaterialsConfiguration::getId).orElse(null);
  }


  public Long getVotingMaterialInvalidationId() {
    return operation.getVotingMaterialsConfiguration()
                    .flatMap(VotingMaterialsConfiguration::getInvalidationAction)
                    .map(PrivilegedAction::getId)
                    .orElse(null);
  }

  public IdHolder(Operation operation) {
    this.operation = operation;
  }
}
