/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation

import java.time.LocalDateTime
import javax.validation.Validation
import javax.validation.Validator
import spock.lang.Specification

class HighlightedQuestionVoTest extends Specification {
  private Validator validator

  def setup() {
    def factory = Validation.buildDefaultValidatorFactory()
    validator = factory.getValidator()
  }

  def "should not signal any violation for a valid HighlightedQuestionVo"() {
    given:
    def highlightedQuestionEntry = new AttachmentFileEntryVo()
    highlightedQuestionEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION
    highlightedQuestionEntry.importDateTime = LocalDateTime.now()
    highlightedQuestionEntry.zipFileName = "highlightedQuestion.zip"
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.answerFile = highlightedQuestionEntry
    highlightedQuestion.question = "Question ?"

    when:
    def violations = validator.validate(highlightedQuestion)

    then:
    violations.empty
  }

  def "should not validate a HighlightedQuestionVo with an invalid question"() {
    given:
    def highlightedQuestionEntry = new AttachmentFileEntryVo()
    highlightedQuestionEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION
    highlightedQuestionEntry.importDateTime = LocalDateTime.now()
    highlightedQuestionEntry.zipFileName = "highlightedQuestion.zip"
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.answerFile = highlightedQuestionEntry
    highlightedQuestion.question = "<?>"

    when:
    def violations = validator.validate(highlightedQuestion)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "question"
  }

  def "should not validate a HighlightedQuestionVo that's missing the attachment"() {
    given:
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.question = "Question ?"

    when:
    def violations = validator.validate(highlightedQuestion)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "answerFile"
  }

  def "should not validate a HighlightedQuestionsVo with the wrong attachment type"() {
    given:
    def highlightedQuestionEntry = new AttachmentFileEntryVo()
    highlightedQuestionEntry.attachmentType = AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION
    highlightedQuestionEntry.importDateTime = LocalDateTime.now()
    highlightedQuestionEntry.zipFileName = "highlightedQuestion.zip"
    def highlightedQuestion = new HighlightedQuestionsVo()
    highlightedQuestion.answerFile = highlightedQuestionEntry
    highlightedQuestion.question = "Question ?"

    when:
    def violations = validator.validate(highlightedQuestion)

    then:
    violations.size() == 1
    violations[0].propertyPath.toString() == "answerFile"
  }
}