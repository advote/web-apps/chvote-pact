/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;

/**
 * A DTO for incoming voting period configuration requests.
 */
public class VotingPeriodConfigurationSubmissionVo {
  @NotNull
  private String user;

  @NotNull
  private AttachmentFileEntryVo electoralAuthorityKey;

  @NotNull
  private DeploymentTarget target;

  private String simulationName;

  private LocalDateTime siteOpeningDate;

  private LocalDateTime siteClosingDate;

  private Integer gracePeriod;

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public AttachmentFileEntryVo getElectoralAuthorityKey() {
    return electoralAuthorityKey;
  }

  public void setElectoralAuthorityKey(AttachmentFileEntryVo electoralAuthorityKey) {
    this.electoralAuthorityKey = electoralAuthorityKey;
  }

  public LocalDateTime getSiteOpeningDate() {
    return siteOpeningDate;
  }

  public void setSiteOpeningDate(LocalDateTime siteOpeningDate) {
    this.siteOpeningDate = siteOpeningDate;
  }

  public LocalDateTime getSiteClosingDate() {
    return siteClosingDate;
  }

  public void setSiteClosingDate(LocalDateTime siteClosingDate) {
    this.siteClosingDate = siteClosingDate;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }

  public DeploymentTarget getTarget() {
    return target;
  }

  public void setTarget(DeploymentTarget target) {
    this.target = target;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public void setSimulationName(String simulationName) {
    this.simulationName = simulationName;
  }
}
