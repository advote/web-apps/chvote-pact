/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.validation;

import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A validator implementation for {@link RequiredMilestones} constraint. Checks that the given {@link Map} with
 * {@link OperationConfigurationSubmissionVo.Milestone} keys contains at least one element of each type set by
 * {@link RequiredMilestones#value()}}.
 */
public class RequiredMilestonesValidator implements ConstraintValidator<RequiredMilestones,
    Map<OperationConfigurationSubmissionVo.Milestone, LocalDateTime>> {
  private Set<OperationConfigurationSubmissionVo.Milestone> mustHaveMilestones;

  @Override
  public void initialize(RequiredMilestones constraintAnnotation) {
    mustHaveMilestones = EnumSet.copyOf(Arrays.asList(constraintAnnotation.value()));
  }

  @Override
  public boolean isValid(Map<OperationConfigurationSubmissionVo.Milestone, LocalDateTime> milestones,
                         ConstraintValidatorContext constraintValidatorContext) {
    //null values are valid
    if (milestones == null || milestones.size() == 0) {
      return true;
    }

    Set<OperationConfigurationSubmissionVo.Milestone> milestonesKeys = EnumSet.copyOf(milestones.keySet());
    return mustHaveMilestones.stream()
                             .filter(milestonesKeys::contains)
                             .count() == mustHaveMilestones.size();
  }
}
