/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.printerarchive;

import java.util.Objects;

/**
 * A DTO holding election configuration parameters.
 */
public final class ElectionConfigurationVo {

  private final String  ballotIdentifier;
  private final boolean ballotVoidOnElectoralRollWithNoCandidate;

  /**
   * Create a new election configuration.
   *
   * @param ballotIdentifier                         identifies the targeted ballot of this configuration.
   * @param ballotVoidOnElectoralRollWithNoCandidate whether it should be possible to choose an empty list with no
   *                                                 candidates.
   */
  public ElectionConfigurationVo(String ballotIdentifier, boolean ballotVoidOnElectoralRollWithNoCandidate) {
    this.ballotIdentifier = ballotIdentifier;
    this.ballotVoidOnElectoralRollWithNoCandidate = ballotVoidOnElectoralRollWithNoCandidate;
  }

  public String getBallotIdentifier() {
    return ballotIdentifier;
  }

  public boolean isBallotVoidOnElectoralRollWithNoCandidate() {
    return ballotVoidOnElectoralRollWithNoCandidate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ElectionConfigurationVo that = (ElectionConfigurationVo) o;
    return Objects.equals(ballotIdentifier, that.ballotIdentifier)
           && ballotVoidOnElectoralRollWithNoCandidate == that.ballotVoidOnElectoralRollWithNoCandidate;
  }

  @Override
  public int hashCode() {
    return Objects.hash(ballotIdentifier);
  }
}
