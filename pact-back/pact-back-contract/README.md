# Data constraints

## OperationConfigurationSubmissionVo

| Property | Description |  Required | Constraints |
| -------- | ----------- | -------- | ----------- |
| user | User (username) that uploads test site | Yes | Pattern: `\w+`<br>Min size: 1<br>Max size: 50 |
| clientId | unique operation identifier in external system | Yes | Pattern: `\d+`<br>Min size: 1<br>Max size: 20 |
| operationLabel | short name of the operation | Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 50 |
| operationName | full name of the operation | Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 200 |
| operationDate | | Yes | Must be in the future |
| gracePeriod | Grace period in minutes | Yes | Min: 0<br>Max: 50 |
| canton |  |  Yes | Pattern: `[A-Z]{2}`<br>Min size: 2<br>Max size: 2 |
| groupVotation | specify if the votations should be grouped. *Hard coded parameter in back-office by management entity*  | Yes | Allowed values: `true` or `false` |
| testPrinter | |Yes | See [TestSitePrinter](#TestSitePrinter) |
| attachments | Catalog of files uploaded in backend application |Yes | Min size 3<br>Must contain: `DOMAIN_OF_INFLUENCE`, `REGISTER` and  `TRANSLATIONS`<br>At least one of: `REPOSITORY_ELECTION` or `REPOSITORY_VOTATION` See [TestSitePrinter](#AttachmentFileEntryVo)|
| electionSiteConfigurations | Voting site configuration relative to election | Yes | See [ElectionSiteConfigurationVo](#ElectionSiteConfigurationVo) |
| highlightedQuestions | Highlighted Questions document | Yes | See [HighlightedQuestionsVo](#HighlightedQuestionsVo) |
| ballotDocumentations | Ballot documentation document | Yes | See [BallotDocumentationVo](#BallotDocumentationVo) |
| milestones | milestones configured for this operation | Yes | Min size 2<br>Must contain: `SITE_OPEN` and `SITE_CLOSE` |

## TestSitePrinter

| Property | Description | Required | Constraints |
| -------- | ----------- | -------- | ----------- |
| municipality | Hardcoded municipality for test voting card | Yes | See [MunicipalityVo](#MunicipalityVo) |
| printer | test voting card's printer relative information  | Yes | See [PrinterConfigurationVo](#PrinterConfigurationVo) |

## MunicipalityVo

| Property | Description |Required | Constraints |
| -------- | -------- | -------- | ----------- |
| ofsId | Municipality Identifier given by OFS | Yes | Min: 1<br>Max: 9999 |
| name | Name of the municipality  |Yes | Pattern: `[\p{L} \-',()/.]+`<br>Min size: 1<br>Max size: 40 |

## PrinterConfigurationVo

| Property | Description | Required | Constraints |
| -------- | ----------  | -------- | ----------- |
| id | printer unique identifier | Yes | Pattern: `[a-zA-Z0-9\-]`<br>Min size: 1<br>Max size: 20 |
| name | printer's name | Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 50 |
| publicKey | public encryption key to use for this printer | Yes | Bit length min: 511<br>Bit length max: 3072 |

## ElectionSiteConfigurationVo

| Property | Description | Required | Constraints |
| -------- | ----------- | -------- | ----------- |
| ballot |Ballot identifier (electionIdentification in ECH-157) | Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 50 |
| displayCandidateSearchForm | | Yes | Allowed values: `true` or `false` |
| allowChangeOfElectoralList | | Yes | Allowed values: `true` or `false` |
| displayEmptyPosition | | Yes | Allowed values: `true` or `false` |
| displayCandidatePositionOnACompactBallotPaper | | Yes | Allowed values: `true` or `false` |
| displayCandidatePositionOnAModifiedBallotPaper | | Yes | Allowed values: `true` or `false` |
| displaySuffrageCount | | Yes | Allowed values: `true` or `false` |
| displayListVerificationCode | | Yes | Allowed values: `true` or `false` |
| allowOpenCandidature | | Yes | Allowed values: `true` or `false` |
| allowMultipleMandates | | Yes | Allowed values: `true` or `false` |
| displayVoidOnEmptyBallotPaper | | Yes | Allowed values: `true` or `false` |
| candidateInformationDisplayModel | | Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 200 |
| displayedColumnsOnVerificationTable | | Yes | Unknown |
| columnsOrderOnVerificationTable | | Yes | Unknown |

## HighlightedQuestionsVo

| Property | Description | Required | Constraints |
| -------- | -----------  | -------- | ----------- |
| answerFile | pdf file uploaded on backend application | Yes | See [AttachmentFileEntryVo](#AttachmentFileEntryVo)<br>attachmentType must be: HIGHLIGHTED_QUESTION |
| question | Localized question label | Yes | Pattern: `[\p{L}\d \-',()/._?]+`<br>Min size: 1<br>Max size: 500 |

## BallotDocumentationVo

| Property | Description | Required | Constraints |
| -------- | ----------- | -------- | ----------- |
| documentation | documentation | Yes | See [AttachmentFileEntryVo](#AttachmentFileEntryVo)<br>attachmentType must be: BALLOT_DOCUMENTATION |
| label | localized label |  Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 200 |
| ballot | ballot identifier (voteIdentification in ECH-159) |  Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 50 |

## AttachmentFileEntryVo

| Property | Description | Required | Constraints |
| -------- | ----------- | -------- | ----------- |
| externalIdentifier | file identifier in external system. For replaced repository files they should keep the same identifier | No | Must be a number |
| zipFileName | File name from HTTP request attachment | Yes | Pattern: `[\p{L}\d \-',()/._]+`<br>Min size: 1<br>Max size: 100 |
| attachmentType | Identify type of attachment | Yes | Allowed values: `REPOSITORY_VOTATION`, `REPOSITORY_ELECTION`, `DOMAIN_OF_INFLUENCE`, `DOCUMENT_FAQ`, `DOCUMENT_TERMS`, `DOCUMENT_CERTIFICATE`, `REGISTER`, `BALLOT_DOCUMENTATION`, `TRANSLATIONS`, `HIGHLIGHTED_QUESTION` and `ELECTION_OFFICER_KEY`  |
| importDateTime | | Yes |  Must be in the past |