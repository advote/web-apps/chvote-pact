/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.google.common.hash.Hashing;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * A builder for {@link InformationAttachment} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class InformationAttachmentBuilder {
  private String         language;
  private String         name;
  private AttachmentType type;
  private LocalDateTime  importDate;
  private String         fileHash;
  private byte[]         file;

  private InformationAttachmentBuilder() {
  }

  private InformationAttachmentBuilder(String name) {
    this.name = name;
  }

  public static InformationAttachmentBuilder anOperationFaq(String name) {
    return new InformationAttachmentBuilder(name).withType(AttachmentType.OPERATION_FAQ);
  }

  public static InformationAttachmentBuilder anOperationTranslationFile(String name) {
    return new InformationAttachmentBuilder(name).withType(AttachmentType.OPERATION_TRANSLATION_FILE);
  }

  public static InformationAttachmentBuilder anOperationTermsOfUsage(String name) {
    return new InformationAttachmentBuilder(name).withType(AttachmentType.OPERATION_TERMS_OF_USAGE);
  }

  public static InformationAttachmentBuilder anOperationCertificateVerificationFile(String name) {
    return new InformationAttachmentBuilder(name).withType(AttachmentType.OPERATION_CERTIFICATE_VERIFICATION_FILE);
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static InformationAttachmentBuilder copyOf(InformationAttachment prototype) {
    return new InformationAttachmentBuilder()
        .withLanguage(prototype.getLanguage())
        .withName(prototype.getName())
        .withType(prototype.getType())
        .withImportDate(prototype.getImportDate())
        .withFileHash(prototype.getFileHash())
        .withFile(prototype.getFile());
  }

  public InformationAttachmentBuilder withLanguage(String language) {
    this.language = language;
    return this;
  }

  public InformationAttachmentBuilder withName(String name) {
    this.name = name;
    return this;
  }

  // private because set at construction time
  private InformationAttachmentBuilder withType(AttachmentType type) {
    Verify.verify( ! type.isOperationReferenceFile(), "%s should be an OperationReferenceFile", type);
    this.type = type;
    return this;
  }

  public InformationAttachmentBuilder withImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
    return this;
  }

  /**
   * Sets the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public InformationAttachmentBuilder withFile(byte[] content) {
    this.file = Arrays.copyOf(content, content.length);
    return this;
  }

  /**
   * Sets the file hash - independent of the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public InformationAttachmentBuilder withFileHash(String fileHash) {
    this.fileHash = fileHash;
    return this;
  }

  /**
   * Sets the hash for the file's content - content MUST have been set before calling this method !
   */
  public InformationAttachmentBuilder withCalculatedFileHash() {
    Preconditions.checkNotNull(file, "File's content must have been set prior to hash calculation");
    return withFileHash(Hashing.sha256().hashBytes(file).toString() );
  }

  public InformationAttachmentBuilder withRealFile(Path path) {
    try {
      byte[] bytes = Files.readAllBytes(path);
      return withFile(bytes).withCalculatedFileHash();
    } catch (Exception e) {
      throw new ResourceLoadingException("Failed reading file " + path, e);
    }
  }

  public InformationAttachment build() {
    InformationAttachment informationAttachment = new InformationAttachment();
    informationAttachment.setLanguage(language);
    informationAttachment.setName(name);
    informationAttachment.setType(type);
    informationAttachment.setImportDate(importDate);
    informationAttachment.setFileHash(fileHash);
    informationAttachment.setFile(file);
    return informationAttachment;
  }
}
