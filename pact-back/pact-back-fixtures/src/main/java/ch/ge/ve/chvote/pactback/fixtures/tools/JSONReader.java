/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.tools;

import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.JSDates;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.InputStream;
import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * Static utility to read (parse) JSON resources with a singleton Jackson mapper.
 */
public class JSONReader {

  private static final JSONReader INSTANCE = new JSONReader();

  private final ObjectMapper objectMapper;

  private JSONReader() {
    this.objectMapper = initializeJacksonMapper();
  }

  private static ObjectMapper initializeJacksonMapper() {
    SimpleModule module = new SimpleModule();
    // We only register deserializers as this class is only meant for JSON parsing
    module.addDeserializer(LocalDateTime.class, JSDates.DESERIALIZER);
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer());

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(module);
    return mapper;
  }

  /**
   * Load a JSON file as a classpath resource and deserialize it using our standard CHVote-configured Jackson mapper.
   *
   * @param resourcePath path to the JSON resource
   * @param valueType    the class the JSON should be deserialized as
   * @param <T>          the actual type.
   *
   * @return a new instance obtained via JSON deserialization
   */
  public static <T> T readFromClasspath(String resourcePath, Class<T> valueType) {
    try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath)) {
      return INSTANCE.objectMapper.readValue(is, valueType);
    } catch (Exception e) {
      throw new ResourceLoadingException("Unable to read fixture file : " + resourcePath, e);
    }
  }
}
