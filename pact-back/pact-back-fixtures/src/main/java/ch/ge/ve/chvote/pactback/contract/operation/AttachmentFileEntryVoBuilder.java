/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.contract.operation;

import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo.AttachmentType;
import java.time.LocalDateTime;

/**
 * A builder for {@link AttachmentFileEntryVo} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class AttachmentFileEntryVoBuilder {
  private String         zipFileName;
  private AttachmentType attachmentType;
  private LocalDateTime  importDateTime;
  private String         language;
  private long externalIdentifier;

  private AttachmentFileEntryVoBuilder() { /* hidden constructor */ }

  private AttachmentFileEntryVoBuilder(String zipFileName) {
    this.zipFileName = zipFileName;
  }

  public static AttachmentFileEntryVoBuilder aVotationRepository(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.REPOSITORY_VOTATION);
  }

  public static AttachmentFileEntryVoBuilder anElectionRepository(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.REPOSITORY_ELECTION);
  }

  public static AttachmentFileEntryVoBuilder aDomainOfInfluence(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.DOMAIN_OF_INFLUENCE);
  }

  public static AttachmentFileEntryVoBuilder aDocumentFaq(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.DOCUMENT_FAQ);
  }

  public static AttachmentFileEntryVoBuilder aDocumentTerms(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.DOCUMENT_TERMS);
  }

  public static AttachmentFileEntryVoBuilder aDocumentCertificate(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.DOCUMENT_CERTIFICATE);
  }

  public static AttachmentFileEntryVoBuilder aRegister(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.REGISTER);
  }

  public static AttachmentFileEntryVoBuilder aBallotDocumentation(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.BALLOT_DOCUMENTATION);
  }

  public static AttachmentFileEntryVoBuilder aHighlightedQuestion(String fileName) {
    return new AttachmentFileEntryVoBuilder(fileName).withAttachmentType(AttachmentType.HIGHLIGHTED_QUESTION);
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static AttachmentFileEntryVoBuilder copyOf(AttachmentFileEntryVo prototype) {
    return new AttachmentFileEntryVoBuilder()
        .withZipFileName(prototype.getZipFileName())
        .withAttachmentType(prototype.getAttachmentType())
        .withImportDateTime(prototype.getImportDateTime())
        .withExternalIdentifier(prototype.getExternalIdentifier())
        .withLanguage(prototype.getLanguage());


  }

  public AttachmentFileEntryVoBuilder withZipFileName(String zipFileName) {
    this.zipFileName = zipFileName;
    return this;
  }

  // private as set by static factory method
  private AttachmentFileEntryVoBuilder withAttachmentType(AttachmentType attachmentType) {
    this.attachmentType = attachmentType;
    return this;
  }

  public AttachmentFileEntryVoBuilder withImportDateTime(LocalDateTime importDateTime) {
    this.importDateTime = importDateTime;
    return this;
  }

  public AttachmentFileEntryVoBuilder withLanguage(String language) {
    this.language = language;
    return this;
  }

  public AttachmentFileEntryVoBuilder withExternalIdentifier(long externalIdentifier) {
    this.externalIdentifier = externalIdentifier;
    return this;
  }

  public AttachmentFileEntryVo build() {
    AttachmentFileEntryVo attachmentFileEntryVo = new AttachmentFileEntryVo();
    attachmentFileEntryVo.setZipFileName(zipFileName);
    attachmentFileEntryVo.setAttachmentType(attachmentType);
    attachmentFileEntryVo.setImportDateTime(importDateTime);
    attachmentFileEntryVo.setLanguage(language);
    attachmentFileEntryVo.setExternalIdentifier(externalIdentifier);
    return attachmentFileEntryVo;
  }

}
