/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.fixtures.repository;

import ch.ge.ve.chvote.pactback.repository.operation.configuration.entity.OperationConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterEntityBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.DateOfBirth;
import ch.ge.ve.chvote.pactback.repository.operation.voter.entity.VoterEntity;
import java.util.Collections;

/**
 * A static factory to obtain (entities) {@link VoterEntity} objects.
 */
public class VoterEntities {

  private VoterEntities() {
    throw new AssertionError("Not instantiable");
  }

  public static VoterEntity voterEntity() {
    return VoterEntityBuilder.aVoterEntity()
                             .withId(1L)
                             .withDateOfBirth(new DateOfBirth(1998, 7, 15))
                             .withLocalPersonId("fd34gb")
                             .withPrintingAuthorityName("Test Printer")
                             .withProtocolVoterIndex(1)
                             .withVoterCountingCircle(VoterCountingCircles.geneve())
                             .withAllowedDoiIds(Collections.singletonList("GE"))
                             .withProtocolInstance(
                                 Operations.failedOnResultsGeneration()
                                           .getConfigurationInTest()
                                           .map(OperationConfiguration::fetchProtocolInstance).orElse(null)
                             ).build();
  }
}
