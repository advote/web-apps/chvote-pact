/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.progress.entity;

import ch.ge.ve.chvote.pactback.repository.protocol.progress.ProtocolStage;
import java.time.LocalDateTime;

/**
 * A builder for {@link ProtocolStageProgress} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class ProtocolStageProgressBuilder {
  private Integer       ccIndex;
  private ProtocolStage stage;
  private Long          done;
  private Long          total;
  private LocalDateTime startDate;
  private LocalDateTime lastModificationDate;

  private ProtocolStageProgressBuilder() {
  }

  public static ProtocolStageProgressBuilder aProtocolStageProgress() {
    return new ProtocolStageProgressBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static ProtocolStageProgressBuilder copyOf(ProtocolStageProgress prototype) {
    return new ProtocolStageProgressBuilder()
        .withCcIndex(prototype.getCcIndex())
        .withStage(prototype.getStage())
        .withDone(prototype.getDone())
        .withTotal(prototype.getTotal())
        .withStartDate(prototype.getStartDate())
        .withLastModificationDate(prototype.getLastModificationDate());
  }

  public ProtocolStageProgressBuilder withCcIndex(Integer ccIndex) {
    this.ccIndex = ccIndex;
    return this;
  }

  public ProtocolStageProgressBuilder withStage(ProtocolStage stage) {
    this.stage = stage;
    return this;
  }

  public ProtocolStageProgressBuilder withDone(Long done) {
    this.done = done;
    return this;
  }

  public ProtocolStageProgressBuilder withTotal(Long total) {
    this.total = total;
    return this;
  }

  public ProtocolStageProgressBuilder withStartDate(LocalDateTime startDate) {
    this.startDate = startDate;
    return this;
  }

  public ProtocolStageProgressBuilder withLastModificationDate(LocalDateTime lastModificationDate) {
    this.lastModificationDate = lastModificationDate;
    return this;
  }

  public ProtocolStageProgress build() {
    ProtocolStageProgress protocolStageProgress = new ProtocolStageProgress();
    protocolStageProgress.setCcIndex(ccIndex);
    protocolStageProgress.setStage(stage);
    protocolStageProgress.setDone(done);
    protocolStageProgress.setTotal(total);
    protocolStageProgress.setStartDate(startDate);
    protocolStageProgress.setLastModificationDate(lastModificationDate);
    return protocolStageProgress;
  }
}
