/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import com.google.common.base.Preconditions;
import com.google.common.hash.Hashing;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * A builder for {@link PublicKeyFile} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class PublicKeyFileBuilder {
  private String         name;
  private Long           externalIdentifier;
  private LocalDateTime  importDate;
  private String         fileHash;
  private byte[]         file;

  private PublicKeyFileBuilder() {
  }

  public static PublicKeyFileBuilder aPublicKeyFile() {
    return new PublicKeyFileBuilder();
  }

  public PublicKeyFileBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public PublicKeyFileBuilder withExternalIdentifier(Long externalIdentifier) {
    this.externalIdentifier = externalIdentifier;
    return this;
  }

  public PublicKeyFileBuilder withImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
    return this;
  }

  /**
   * Sets the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public PublicKeyFileBuilder withFile(byte[] content) {
    this.file = Arrays.copyOf(content, content.length);
    return this;
  }

  /**
   * Sets the file hash - independent of the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public PublicKeyFileBuilder withFileHash(String fileHash) {
    this.fileHash = fileHash;
    return this;
  }

  /**
   * Sets the hash for the file's content - content MUST have been set before calling this method !
   */
  public PublicKeyFileBuilder withCalculatedFileHash() {
    Preconditions.checkNotNull(file, "File's content must have been set prior to hash calculation");
    return withFileHash(Hashing.sha256().hashBytes(file).toString() );
  }

  public PublicKeyFileBuilder withRealFile(Path path) {
    try {
      byte[] bytes = Files.readAllBytes(path);
      return withFile(bytes).withCalculatedFileHash();
    } catch (Exception e) {
      throw new ResourceLoadingException("Failed reading file " + path, e);
    }
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static PublicKeyFileBuilder copyOf(PublicKeyFile prototype) {
    return new PublicKeyFileBuilder()
        .withName(prototype.getName())
        .withExternalIdentifier(prototype.getExternalIdentifier())
        .withImportDate(prototype.getImportDate())
        .withFileHash(prototype.getFileHash())
        .withFile(Arrays.copyOf(prototype.getFile(), prototype.getFile().length));
  }

  public PublicKeyFile build() {
    PublicKeyFile publicKeyFile = new PublicKeyFile();
    publicKeyFile.setName(name);
    publicKeyFile.setExternalIdentifier(externalIdentifier);
    publicKeyFile.setType(AttachmentType.ELECTION_OFFICER_KEY);
    publicKeyFile.setImportDate(importDate);
    publicKeyFile.setFileHash(fileHash);
    publicKeyFile.setFile(file);
    return publicKeyFile;
  }
}
