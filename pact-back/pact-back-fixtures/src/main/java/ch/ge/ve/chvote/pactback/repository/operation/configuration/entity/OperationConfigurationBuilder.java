/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.entity;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeployment;
import ch.ge.ve.chvote.pactback.repository.action.deploy.entity.ConfigurationDeploymentBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.ValidationStatus;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachment;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.InformationAttachmentBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFile;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity.OperationReferenceFileBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfiguration;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.printer.entity.PrinterConfigurationBuilder;
import ch.ge.ve.chvote.pactback.repository.operation.entity.OperationBuilder;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstance;
import ch.ge.ve.chvote.pactback.repository.protocol.entity.ProtocolInstanceBuilder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A builder for {@link OperationConfiguration} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class OperationConfigurationBuilder {
  private String                          operationName;
  private String                          operationLabel;
  private LocalDateTime                   operationDate;
  private ValidationStatus                validationStatus;
  private LocalDateTime                   lastChangeDate;
  private String                          lastChangeUser;
  private String                          siteTextHash;
  private LocalDateTime                   siteOpeningDate;
  private LocalDateTime                   siteClosingDate;
  private Integer                         gracePeriod;
  private String                          canton;
  private boolean                         groupVotation;
  private PrinterConfiguration            testPrinter;
  private List<OperationReferenceFile>    operationReferenceFiles    = new ArrayList<>();
  private List<InformationAttachment>     informationAttachments     = new ArrayList<>();
  private List<ElectionSiteConfiguration> electionSiteConfigurations = new ArrayList<>();

  private ProtocolInstance        protocolInstance;
  private ConfigurationDeployment action;

  private OperationConfigurationBuilder() {
  }

  public static OperationConfigurationBuilder anOperationConfiguration() {
    return new OperationConfigurationBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   * <p>
   * <strong>This method won't copy bidirectional bindings</strong>. To avoid cyclic references copy problems,
   * this method does not copy the fields {@code protocolInstance} and {@code action}. <br>
   * If you want to copy a full graph, please consider copying from a "higher" entity. Or you can copy these two
   * fields using their own builders.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   *
   * @see OperationBuilder
   * @see ProtocolInstanceBuilder
   * @see ConfigurationDeploymentBuilder
   */
  public static OperationConfigurationBuilder copyWithoutBidirectionnal(OperationConfiguration prototype) {
    return new OperationConfigurationBuilder()
        .withOperationName(prototype.getOperationName())
        .withOperationLabel(prototype.getOperationLabel())
        .withOperationDate(prototype.getOperationDate())
        .withValidationStatus(prototype.getValidationStatus())
        .withLastChangeDate(prototype.getLastChangeDate())
        .withLastChangeUser(prototype.getLastChangeUser())
        .withSiteTextHash(prototype.getSiteTextHash())
        .withSiteOpeningDate(prototype.getSiteOpeningDate())
        .withSiteClosingDate(prototype.getSiteClosingDate())
        .withGracePeriod(prototype.getGracePeriod())
        .withCanton(prototype.getCanton())
        .withTestPrinter(PrinterConfigurationBuilder.copyOf(prototype.getTestPrinter()).build())
        .withOperationReferenceFiles(prototype.getOperationReferenceFiles().stream()
                                              .map(OperationReferenceFileBuilder::copyOf)
                                              .map(OperationReferenceFileBuilder::build)
                                              .collect(toList()))
        .withInformationAttachments(prototype.getInformationAttachments().stream()
                                             .map(InformationAttachmentBuilder::copyOf)
                                             .map(InformationAttachmentBuilder::build)
                                             .collect(toList()))
        .withElectionSiteConfigurations(prototype.getElectionSiteConfigurations().stream()
                                                 .map(ElectionSiteConfigurationBuilder::copyOf)
                                                 .map(ElectionSiteConfigurationBuilder::build)
                                                 .collect(toList()));
  }

  public OperationConfigurationBuilder withOperationName(String operationName) {
    this.operationName = operationName;
    return this;
  }

  public OperationConfigurationBuilder withOperationLabel(String operationLabel) {
    this.operationLabel = operationLabel;
    return this;
  }

  public OperationConfigurationBuilder withOperationDate(LocalDateTime operationDate) {
    this.operationDate = operationDate;
    return this;
  }

  public OperationConfigurationBuilder withValidationStatus(ValidationStatus status) {
    this.validationStatus = status;
    return this;
  }

  public OperationConfigurationBuilder withLastChangeDate(LocalDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
    return this;
  }

  public OperationConfigurationBuilder withLastChangeUser(String lastChangeUser) {
    this.lastChangeUser = lastChangeUser;
    return this;
  }

  public OperationConfigurationBuilder withLastChange(String user, LocalDateTime date) {
    return withLastChangeUser(user).withLastChangeDate(date);
  }

  public OperationConfigurationBuilder withSiteTextHash(String siteTextHash) {
    this.siteTextHash = siteTextHash;
    return this;
  }

  public OperationConfigurationBuilder withSiteOpeningDate(LocalDateTime siteOpeningDate) {
    this.siteOpeningDate = siteOpeningDate;
    return this;
  }

  public OperationConfigurationBuilder withSiteClosingDate(LocalDateTime siteClosingDate) {
    this.siteClosingDate = siteClosingDate;
    return this;
  }

  public OperationConfigurationBuilder withGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
    return this;
  }

  public OperationConfigurationBuilder withCanton(String canton) {
    this.canton = canton;
    return this;
  }

  public OperationConfigurationBuilder withGroupVotation(boolean groupVotation) {
    this.groupVotation = groupVotation;
    return this;
  }

  public OperationConfigurationBuilder withTestPrinter(PrinterConfiguration testPrinter) {
    this.testPrinter = testPrinter;
    return this;
  }

  public OperationConfigurationBuilder withOperationReferenceFiles(Collection<OperationReferenceFile> values) {
    this.operationReferenceFiles = new ArrayList<>(values);
    return this;
  }

  public OperationConfigurationBuilder withInformationAttachments(Collection<InformationAttachment> values) {
    this.informationAttachments = new ArrayList<>(values);
    return this;
  }

  public OperationConfigurationBuilder withElectionSiteConfigurations(Collection<ElectionSiteConfiguration> values) {
    this.electionSiteConfigurations = new ArrayList<>(values);
    return this;
  }

  public OperationConfigurationBuilder withProtocolInstance(ProtocolInstance protocolInstance) {
    this.protocolInstance = protocolInstance;
    return this;
  }

  public OperationConfigurationBuilder withAction(ConfigurationDeployment action) {
    this.action = action;
    return this;
  }

  public OperationConfiguration build() {
    OperationConfiguration operationConfiguration = new OperationConfiguration();
    operationConfiguration.setOperationName(operationName);
    operationConfiguration.setOperationLabel(operationLabel);
    operationConfiguration.setOperationDate(operationDate);
    operationConfiguration.setValidationStatus(validationStatus);
    operationConfiguration.setLastChangeDate(lastChangeDate);
    operationConfiguration.setLastChangeUser(lastChangeUser);
    operationConfiguration.setSiteTextHash(siteTextHash);
    operationConfiguration.setSiteOpeningDate(siteOpeningDate);
    operationConfiguration.setSiteClosingDate(siteClosingDate);
    operationConfiguration.setGracePeriod(gracePeriod);
    operationConfiguration.setCanton(canton);
    operationConfiguration.setGroupVotation(groupVotation);
    operationConfiguration.setTestPrinter(testPrinter);
    operationConfiguration.setOperationReferenceFiles(operationReferenceFiles);
    operationConfiguration.setInformationAttachments(informationAttachments);
    operationConfiguration.setElectionSiteConfigurations(electionSiteConfigurations);
    bind(operationConfiguration, protocolInstance);
    bind(operationConfiguration, action);
    return operationConfiguration;
  }

  private static void bind(OperationConfiguration configuration, ProtocolInstance protocolInstance) {
    if (configuration.getProtocolInstance().orElse(null) != protocolInstance) {
      configuration.setProtocolInstance(protocolInstance);
    }
    if (protocolInstance != null && protocolInstance.getConfiguration() != configuration) {
      protocolInstance.setConfiguration(configuration);
    }
  }

  private static void bind(OperationConfiguration configuration, ConfigurationDeployment action) {
    if (configuration.getAction().orElse(null) != action) {
      configuration.setAction(action);
    }
    if (action != null && action.getOperationConfiguration() != configuration) {
      action.setOperationConfiguration(configuration);
    }
  }
}
