/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.entity;

import ch.ge.ve.chvote.pactback.fixtures.ResourceLoadingException;
import ch.ge.ve.chvote.pactback.repository.operation.configuration.attachment.AttachmentType;
import com.google.common.base.Preconditions;
import com.google.common.hash.Hashing;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * A builder for {@link VotersRegisterFile} objects.
 * <p>
 *   Get a new builder with one of its static factory method.
 * </p>
 */
public final class VotersRegisterFileBuilder {
  private Integer                                nbOfVoters;
  private String                                 messageId;
  private LocalDateTime                          generationDate;
  private String                                 sourceApplicationManufacturer;
  private String                                 sourceApplicationProduct;
  private String                                 name;
  private String                                 sourceApplicationProductVersion;
  private AttachmentType                         type;
  private String                                 signatureAuthor;
  private LocalDateTime                          importDate;
  private LocalDateTime                          signatureExpiryDate;
  private String                                 fileHash;
  private OperationReferenceFile.SignatureStatus signatureStatus;
  private byte[]                                 file;

  private VotersRegisterFileBuilder() {
  }

  private VotersRegisterFileBuilder(String name) {
    this.name = name;
  }

  public static VotersRegisterFileBuilder aVotationOperationRepository(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.OPERATION_REPOSITORY_VOTATION);
  }

  public static VotersRegisterFileBuilder anElectionOperationRepository(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.OPERATION_REPOSITORY_ELECTION);
  }

  public static VotersRegisterFileBuilder aDomainOfInfluence(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.DOMAIN_OF_INFLUENCE);
  }

  public static VotersRegisterFileBuilder anOperationFaq(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.OPERATION_FAQ);
  }

  public static VotersRegisterFileBuilder anOperationTermsOfUsage(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.OPERATION_TERMS_OF_USAGE);
  }

  public static VotersRegisterFileBuilder anOperationCertificateVerificationFile(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.OPERATION_CERTIFICATE_VERIFICATION_FILE);
  }

  public static VotersRegisterFileBuilder anOperationRegister(String name) {
    return new VotersRegisterFileBuilder(name).withType(AttachmentType.OPERATION_REGISTER);
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   * @return a new builder instance
   */
  public static VotersRegisterFileBuilder copyOf(VotersRegisterFile prototype) {
    return new VotersRegisterFileBuilder()
        .withNbOfVoters(prototype.getNbOfVoters())
        .withMessageId(prototype.getMessageId())
        .withGenerationDate(prototype.getGenerationDate())
        .withSourceApplicationManufacturer(prototype.getSourceApplicationManufacturer())
        .withSourceApplicationProduct(prototype.getSourceApplicationProduct())
        .withName(prototype.getName())
        .withSourceApplicationProductVersion(prototype.getSourceApplicationProductVersion())
        .withType(prototype.getType())
        .withSignatureAuthor(prototype.getSignatureAuthor())
        .withImportDate(prototype.getImportDate())
        .withSignatureExpiryDate(prototype.getSignatureExpiryDate())
        .withFileHash(prototype.getFileHash())
        .withSignatureStatus(prototype.getSignatureStatus())
        .withFile(prototype.getFile());
  }

  public VotersRegisterFileBuilder withNbOfVoters(Integer nbOfVoters) {
    this.nbOfVoters = nbOfVoters;
    return this;
  }

  public VotersRegisterFileBuilder withMessageId(String messageId) {
    this.messageId = messageId;
    return this;
  }

  public VotersRegisterFileBuilder withGenerationDate(LocalDateTime generationDate) {
    this.generationDate = generationDate;
    return this;
  }

  public VotersRegisterFileBuilder withSourceApplicationManufacturer(String sourceApplicationManufacturer) {
    this.sourceApplicationManufacturer = sourceApplicationManufacturer;
    return this;
  }

  public VotersRegisterFileBuilder withSourceApplicationProduct(String sourceApplicationProduct) {
    this.sourceApplicationProduct = sourceApplicationProduct;
    return this;
  }

  public VotersRegisterFileBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public VotersRegisterFileBuilder withSourceApplicationProductVersion(String sourceApplicationProductVersion) {
    this.sourceApplicationProductVersion = sourceApplicationProductVersion;
    return this;
  }

  // Private because chosen at construction time
  private VotersRegisterFileBuilder withType(AttachmentType type) {
    this.type = type;
    return this;
  }

  public VotersRegisterFileBuilder withSignatureAuthor(String signatureAuthor) {
    this.signatureAuthor = signatureAuthor;
    return this;
  }

  public VotersRegisterFileBuilder withImportDate(LocalDateTime importDate) {
    this.importDate = importDate;
    return this;
  }

  public VotersRegisterFileBuilder withSignatureExpiryDate(LocalDateTime signatureExpiryDate) {
    this.signatureExpiryDate = signatureExpiryDate;
    return this;
  }

  public VotersRegisterFileBuilder withSignatureStatus(OperationReferenceFile.SignatureStatus signatureStatus) {
    this.signatureStatus = signatureStatus;
    return this;
  }

  /**
   * Sets the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public VotersRegisterFileBuilder withFile(byte[] content) {
    this.file = Arrays.copyOf(content, content.length);
    return this;
  }

  /**
   * Sets the file hash - independent of the file's content. Methods exist to set both content and hash consistently.
   *
   * @see #withRealFile(Path)
   * @see #withCalculatedFileHash()
   */
  public VotersRegisterFileBuilder withFileHash(String fileHash) {
    this.fileHash = fileHash;
    return this;
  }

  /**
   * Sets the hash for the file's content - content MUST have been set before calling this method !
   */
  public VotersRegisterFileBuilder withCalculatedFileHash() {
    Preconditions.checkNotNull(file, "File's content must have been set prior to hash calculation");
    return withFileHash(Hashing.sha256().hashBytes(file).toString() );
  }

  public VotersRegisterFileBuilder withRealFile(Path path) {
    try {
      byte[] bytes = Files.readAllBytes(path);
      return withFile(bytes).withCalculatedFileHash();
    } catch (Exception e) {
      throw new ResourceLoadingException("Failed reading file " + path, e);
    }
  }

  public VotersRegisterFile build() {
    VotersRegisterFile votersRegisterFile = new VotersRegisterFile();
    votersRegisterFile.setNbOfVoters(nbOfVoters);
    votersRegisterFile.setMessageId(messageId);
    votersRegisterFile.setGenerationDate(generationDate);
    votersRegisterFile.setSourceApplicationManufacturer(sourceApplicationManufacturer);
    votersRegisterFile.setSourceApplicationProduct(sourceApplicationProduct);
    votersRegisterFile.setName(name);
    votersRegisterFile.setSourceApplicationProductVersion(sourceApplicationProductVersion);
    votersRegisterFile.setType(type);
    votersRegisterFile.setSignatureAuthor(signatureAuthor);
    votersRegisterFile.setImportDate(importDate);
    votersRegisterFile.setSignatureExpiryDate(signatureExpiryDate);
    votersRegisterFile.setFileHash(fileHash);
    votersRegisterFile.setSignatureStatus(signatureStatus);
    votersRegisterFile.setFile(file);
    return votersRegisterFile;
  }
}
