/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.repository.protocol.entity;

/**
 * A builder for {@link VotersCreationStats} objects.
 * <p>
 * Get a new builder with one of its static factory method.
 * </p>
 */
public final class VotersCreationStatsBuilder {
  private String printerAuthorityName;
  private int    countingCircleId;
  private long   numberOfVoters;
  private String countingCircleName;
  private String countingCircleBusinessId;

  private VotersCreationStatsBuilder() {
  }

  public static VotersCreationStatsBuilder aVotersCreationStats() {
    return new VotersCreationStatsBuilder();
  }

  /**
   * Copy an existing object and change some of its attributes, then build a new one.
   * <p>
   * All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
   * </p>
   *
   * @param prototype the object to copy
   *
   * @return a new builder instance
   */
  public static VotersCreationStatsBuilder copyOf(VotersCreationStats prototype) {
    return new VotersCreationStatsBuilder()
        .withPrinterAuthorityName(prototype.getPrinterAuthorityName())
        .withCountingCircleId(prototype.getCountingCircleId())
        .withNumberOfVoters(prototype.getNumberOfVoters())
        .withCountingCircleName(prototype.getCountingCircleName())
        .withCountingCircleBusinessId(prototype.getCountingCircleBusinessId());
  }

  public VotersCreationStatsBuilder withPrinterAuthorityName(String printerAuthorityName) {
    this.printerAuthorityName = printerAuthorityName;
    return this;
  }

  public VotersCreationStatsBuilder withCountingCircleId(int countingCircleId) {
    this.countingCircleId = countingCircleId;
    return this;
  }

  public VotersCreationStatsBuilder withNumberOfVoters(long numberOfVoters) {
    this.numberOfVoters = numberOfVoters;
    return this;
  }

  public VotersCreationStatsBuilder withCountingCircleName(String countingCircleName) {
    this.countingCircleName = countingCircleName;
    return this;
  }

  public VotersCreationStatsBuilder withCountingCircleBusinessId(String countingCircleBusinessId) {
    this.countingCircleBusinessId = countingCircleBusinessId;
    return this;
  }

  public VotersCreationStats build() {
    VotersCreationStats votersCreationStats = new VotersCreationStats();
    votersCreationStats.setPrinterAuthorityName(printerAuthorityName);
    votersCreationStats.setCountingCircleId(countingCircleId);
    votersCreationStats.setNumberOfVoters(numberOfVoters);
    votersCreationStats.setCountingCircleName(countingCircleName);
    votersCreationStats.setCountingCircleBusinessId(countingCircleBusinessId);
    return votersCreationStats;
  }
}
