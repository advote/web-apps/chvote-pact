/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

import ch.ge.ve.chvote.pactback.repository.operation.voter.VoterEntityBuilder
import java.lang.reflect.Field
import java.time.LocalDate
import java.time.LocalDateTime
import org.springframework.util.StringUtils

//
// -------------------------------------------
// Create the builder static method "copyOf"
// based on its private fields
// -------------------------------------------

def builderType = VoterEntityBuilder
def targetType = StringUtils.delete(builderType.simpleName, "Builder")

// ==========  FUNCTIONS  ==========
static getAllDeclaredFields(Class type) {
  def current = type
  List<Field> fields = []
  while (current != null && current != Object) {
    fields.addAll(current.declaredFields)
    current = current.superclass
  }

  return fields
}

static isImmutableType(Class type) {
  if (type.isPrimitive()) return true
  if (type.isEnum()) return true

  return type in [
          String, Short, Integer, Long, Float, Double, Boolean, Byte,
          Character, LocalDate, LocalDateTime, BigDecimal, BigInteger
  ]
}

static toBuilderSetter(Field field, value) {
  def raw = "with${field.name.capitalize()}($value)"
  if (!isImmutableType(field.type)) {
    raw += "  // TODO deep copy"
  }
  return raw
}

static toGetter(Field field) {
  def prefix = field.type in [boolean, Boolean] ? "is" : "get"
  return "${prefix}${field.name.capitalize()}()"
}
// =================================

// -------------- Script begins here --------------
def fieldsCopy = getAllDeclaredFields(builderType).collect { field ->
  def getter = "prototype.${toGetter(field)}"
  return ".${toBuilderSetter(field, getter)}"
}

println """
/**
  * Copy an existing object and change some of its attributes, then build a new one.
  * <p>
  *   All attributes are deeply copied so that you can safely use the prototype and the copy without side effects.
  * </p>
  * 
  * @param prototype the object to copy
  * @return a new builder instance
  */
public static ${builderType.simpleName} copyOf($targetType prototype) {
  return new ${builderType.simpleName}()
        ${fieldsCopy.join('\n        ')};
}
"""