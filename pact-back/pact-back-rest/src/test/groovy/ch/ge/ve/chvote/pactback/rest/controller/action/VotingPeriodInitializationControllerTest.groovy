/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action

import static ch.ge.ve.chvote.pactback.rest.TestUtils.XML_HTTP_REQUEST
import static ch.ge.ve.chvote.pactback.rest.TestUtils.X_REQUESTED_WITH
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import ch.ge.ve.chvote.pactback.FrontendEndPointConfiguration
import ch.ge.ve.chvote.pactback.mock.server.OperationsCreationService
import ch.ge.ve.chvote.pactback.rest.H2DatabaseCleaner
import ch.ge.ve.chvote.pactback.rest.MockedRabbitConfiguration
import ch.ge.ve.chvote.pactback.rest.RestEndpointTest
import ch.ge.ve.chvote.pactback.rest.spring.AfterLastTest
import ch.ge.ve.chvote.pactback.rest.spring.BeforeFirstTest
import ch.ge.ve.chvote.pactback.rest.spring.ContextConfigTestExecutionListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Stepwise

@RestEndpointTest(context = [MockedRabbitConfiguration, FrontendEndPointConfiguration])
@TestExecutionListeners(listeners = ContextConfigTestExecutionListener, mergeMode = MERGE_WITH_DEFAULTS)
@Stepwise
class VotingPeriodInitializationControllerTest extends Specification {

  @Autowired
  MockMvc mvc

  @BeforeFirstTest
  static void prepareOperations(OperationsCreationService service) {
    service.createAllOperations()
  }

  @AfterLastTest
  static void restoreDatabase(H2DatabaseCleaner cleaner) {
    cleaner.cleanupDatabase()
  }

  def "should retrieve all pending or new voting period configuration actions"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-period-initialization/new-or-pending/")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))
    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.length()').value('2'))
            .andExpect(jsonPath('$[0].id').value('9'))
            .andExpect(jsonPath('$[0].votingPeriodConfiguration.id').value('1'))
            .andExpect(jsonPath('$[0].votingPeriodConfiguration.configuration.id').value('11'))
            .andExpect(jsonPath('$[0].type').value('VOTING_PERIOD_INITIALIZATION'))
            .andExpect(jsonPath('$[1].votingPeriodConfiguration.id').value('2'))
            .andExpect(jsonPath('$[1].type').value('VOTING_PERIOD_INITIALIZATION'))
  }

  def "should retrieve a voting period initialization action by id"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-period-initialization/9")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.id').value('9'))
            .andExpect(jsonPath('$.type').value('VOTING_PERIOD_INITIALIZATION'))
            .andExpect(jsonPath('$.status').value('PENDING'))
            .andExpect(jsonPath('$.operationName').value('201907VP'))
            .andExpect(jsonPath('$.votingPeriodConfiguration.id').value('1'))
            .andExpect(jsonPath('$.votingPeriodConfiguration.electoralAuthorityKey').value('A7ECDB122A5CFFA2D483F874C72DB962CD7693B318AD5259B9852498EB3CD191'))
  }

  def "should retrieve pending voting period actions by voting period configuration ID"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-period-initialization?businessId=1")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isOk())
            .andExpect(jsonPath('$.id').value('9'))
            .andExpect(jsonPath('$.type').value('VOTING_PERIOD_INITIALIZATION'))
            .andExpect(jsonPath('$.votingPeriodConfiguration.id').value('1'))
  }

  def "should fail to retrieve a non pending voting period initialization action by ID"() {
    when:
    def resultActions = mvc.perform(get("/privileged-actions/voting-period-initialization/1")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions
            .andExpect(status().isNotFound())
  }

  def "should fail to create a new action if an action already exists for the given voting period configuration id"() {
    when:
    def response = mvc.perform(put("/privileged-actions/voting-period-initialization/")
            .param("businessId", "2")
            .with(httpBasic("user1", "password"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    response
            .andExpect(status().isNoContent())
  }

  def "bad status for approving a PENDING privileged action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-period-initialization/4")
            .param("status", "PENDING").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isBadRequest())
  }

  def "user doesn't have the right to approve request"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-period-initialization/11")
            .param("status", "APPROVED").with(user("user1"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isForbidden())
  }

  def "approve a PENDING privileged action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-period-initialization/11")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNoContent())
  }

  def "approve and already approved action"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-period-initialization/2")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNotFound())
  }

  def "action to approve doesn't exists"() {
    when:
    def resultActions = mvc.perform(put("/privileged-actions/voting-period-initialization/27")
            .param("status", "APPROVED").with(user("user3"))
            .header(X_REQUESTED_WITH, XML_HTTP_REQUEST))

    then:
    resultActions.andExpect(status().isNotFound())
  }
}
