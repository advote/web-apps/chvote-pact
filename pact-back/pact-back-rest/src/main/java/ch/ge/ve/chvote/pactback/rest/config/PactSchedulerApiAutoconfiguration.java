/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.config;

import ch.ge.ve.chvote.pactback.service.operation.PactSchedulerApi;
import ch.ge.ve.chvote.pactback.service.operation.PactSchedulerInMemoryImpl;
import ch.ge.ve.chvote.pactback.service.operation.PactSchedulerRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Auto-configures the implementation of {@link PactSchedulerApi} to use.
 * <p>
 *   If the property {@code "chvote.pact.scheduler.base-uri"} exists, or the environment provides the variable
 *   {@code "CHVOTE_PACT_SCHEDULER_BASE_URI"}, a real REST client is used that will target the given host.
 *   <br>
 *   The fallback is a fully-compliant in-memory implementation, that won't persist the operations and thus
 *   should only be used in non-production environments.
 * </p>
 * <p>
 *   <b>Note :</b> "int" and "rec" Spring profiles force the declaration of a {@code "SCHEDULER_HOST_URI"} env variable
 *   through their <i>application-xx.yaml</i> config file in order to enforce a real client.
 * </p>
 */
@Configuration
public class PactSchedulerApiAutoconfiguration {

  @ConditionalOnProperty("chvote.pact.scheduler.base-uri")
  @Bean
  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  public PactSchedulerApi pactSchedulerClient(RestTemplateBuilder builder,
                                              @Value("${chvote.pact.scheduler.base-uri}") String hostUri) {
    LoggerFactory.getLogger(getClass()).info("Instantiate a PactSchedulerRestClient targeting {}", hostUri);
    return new PactSchedulerRestClient(hostUri, builder);
  }

  @ConditionalOnMissingBean
  @Bean
  public PactSchedulerApi defaultImplementation() {
    final Logger logger = LoggerFactory.getLogger(getClass());
    logger.warn("--------------------------------------------------------------------------------");
    logger.warn("Using in-memory implementation of PactSchedulerApi - not suitable for PRODUCTION");
    logger.warn("--------------------------------------------------------------------------------");
    return new PactSchedulerInMemoryImpl();
  }
}
