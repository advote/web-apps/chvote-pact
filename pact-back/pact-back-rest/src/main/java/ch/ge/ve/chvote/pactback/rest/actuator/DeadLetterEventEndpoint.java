/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.actuator;

import ch.ge.ve.chvote.pactback.service.protocol.DeadLetterEventService;
import ch.ge.ve.chvote.pactback.service.protocol.vo.DeadLetterEventVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Actuator endpoint for listing and republishing the events that have fallen into the dead letter exchange.
 */
@Component
@RestControllerEndpoint(id = "dlx")
@Api(
    value = "Dead letter events",
    tags = "Dead letter events")
public class DeadLetterEventEndpoint {

  private final DeadLetterEventService service;

  @Autowired
  public DeadLetterEventEndpoint(DeadLetterEventService service) {
    this.service = service;
  }

  /**
   * Returns all persisted dead letter events.
   *
   * @return all persisted dead letter events.
   */
  @GetMapping("/events")
  @ApiOperation(
      value = "Get all the dead letter events",
      notes = "Retrieve the list of events that have fallen into the dead letter exchange")
  public List<DeadLetterEventVo> findAll() {
    return service.findAll();
  }

  /**
   * Finds an event given its id.
   *
   * @return the corresponding event.
   */
  @GetMapping("/events/{eventId}")
  @ApiOperation(
      value = "Get event by id",
      notes = "Retrieve a dead letter event by its identifier.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Event not found"),
  })
  public DeadLetterEventVo findById(@PathVariable("eventId") Long id) {
    return service.findById(id);
  }

  /**
   * Republishes the specified event then deletes it.
   *
   * @param id the id of the event to republish.
   */
  @PostMapping("/events/{eventId}/publications")
  @ApiOperation(
      value = "Republish an event",
      notes = "Republish an event that has fallen into the dead letter exchange and removes it from the database.")
  @ApiResponses({
      @ApiResponse(code = 404, message = "Event not found"),
  })
  public void republish(@PathVariable("eventId") Long id) {
    service.republish(id);
  }
}
