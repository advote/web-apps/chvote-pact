/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.rest.controller.action;

import ch.ge.ve.chvote.pactback.service.action.PrivilegedActionService;
import ch.ge.ve.chvote.pactback.service.action.vo.PrivilegedActionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A specialised {@link BaseActionController} for interacting with {@link PrivilegedActionVo}s regardless of their
 * implementation.
 */
@RestController
@RequestMapping("/privileged-actions")
@Api(
    value = "Privileged Action",
    tags = "Privileged Action")
public class PrivilegedActionController extends BaseActionController<PrivilegedActionVo> {
  private final PrivilegedActionService service;

  /**
   * Creates a new privileged action controller.
   *
   * @param service the {@link PrivilegedActionVo} service.
   */
  @Autowired
  public PrivilegedActionController(PrivilegedActionService service) {
    super(service);
    this.service = service;
  }

  /**
   * Retrieve the number of actions that the user of the current session can approve.
   *
   * @param authentication the {@link Authentication} object for the current session.
   *
   * @return The number of pending actions for the current user.
   */
  @GetMapping(value = "pending-count", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get pending actions count",
      notes = "Retrieve the total number of pending actions that the current user can approve.")
  public Integer getPendingCount(Authentication authentication) {
    return service.actionsPendingApprovalCount(authentication.getName());
  }

  /**
   * Retrieve all {@link PrivilegedActionVo} which status is PENDING.
   *
   * @return A list of {@link PrivilegedActionVo}.
   */
  @GetMapping(value = "pending-actions", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
      value = "Get pending actions",
      notes = "Retrieve all the actions that need approval.")
  public List<PrivilegedActionVo> getPendingActions() {
    return service.getPendingPrivilegedActions();
  }
}
