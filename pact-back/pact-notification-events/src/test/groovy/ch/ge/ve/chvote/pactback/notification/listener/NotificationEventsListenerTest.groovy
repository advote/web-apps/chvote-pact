/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.listener

import ch.ge.ve.chvote.pactback.notification.event.BallotBoxInitializedEvent
import ch.ge.ve.chvote.pactback.notification.event.VotingMaterialsGeneratedEvent
import java.util.function.Consumer
import spock.lang.Specification

class NotificationEventsListenerTest extends Specification {

  NotificationEventsConsumersRegistry registry
  NotificationEventsListener listener

  def setup() {
    registry = Mock()
    listener = new NotificationEventsListener(registry)
  }

  def "#processBallotBoxInitializedEvent(...) should invoke all registered ballot box initialized event consumers"() {
    given:
    def event = new BallotBoxInitializedEvent("1")
    def consumer1 = Mock(Consumer)
    def consumer2 = Mock(Consumer)
    registry.getBallotBoxInitializedEventConsumers() >> [consumer1, consumer2]

    when:
    listener.processBallotBoxInitializedEvent(event)

    then:
    1 * consumer1.accept(event)
    1 * consumer2.accept(event)
  }

  def "#processVotingMaterialsGeneratedEvent(...) should invoke all registered voting materials generated event consumers"() {
    given:
    def event = new VotingMaterialsGeneratedEvent("1")
    def consumer1 = Mock(Consumer)
    def consumer2 = Mock(Consumer)
    registry.getVotingMaterialsGeneratedEventConsumers() >> [consumer1, consumer2]

    when:
    listener.processVotingMaterialsGeneratedEvent(event)

    then:
    1 * consumer1.accept(event)
    1 * consumer2.accept(event)
  }
}
