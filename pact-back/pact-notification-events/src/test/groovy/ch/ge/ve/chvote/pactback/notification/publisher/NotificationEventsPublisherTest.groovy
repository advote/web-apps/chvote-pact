/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-pact                                                                                    -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.chvote.pactback.notification.publisher

import ch.ge.ve.chvote.pactback.notification.config.Exchanges
import ch.ge.ve.chvote.pactback.notification.event.BallotBoxInitializedEvent
import org.springframework.amqp.rabbit.core.RabbitTemplate
import spock.lang.Specification

class NotificationEventsPublisherTest extends Specification {

  def "#publish(...) should send the event to the expected exchange on RabbitMq"() {
    given:
    def rabbitTemplate = Mock(RabbitTemplate)
    def publisher = new NotificationEventsPublisher(rabbitTemplate)
    def event = new BallotBoxInitializedEvent("1")

    when:
    publisher.publish(event)

    then:
    1 * rabbitTemplate.convertAndSend(Exchanges.PACT_NOTIFICATION_EVENTS, "", event)
  }

  def "#publish(...) should throw a NullPointerException when given a null event"() {
    given:
    def publisher = new NotificationEventsPublisher(Mock(RabbitTemplate))

    when:
    publisher.publish(null)

    then:
    thrown(NullPointerException)
  }
}
